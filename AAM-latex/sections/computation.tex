\section{Computation of a stationary Nash equilibrium}
\label{sec:Computation}

Our stationary Nash equilibrium computation algorithm is motivated by the notion of \emph{evolutionary dynamics} in static population games. We write the equilibrium seeking problem as the problem of finding the rest points of the following continuous-time dynamical system:
\begin{align}
    \dot{d}_\tau &= d_\tau \: P_\tau(d,\pi) - d_\tau,  \; \forall \tau, \tag{EV.1} \label{eq:EvolutionState} \\
    \dot{\pi}_\tau[\cdot \mid u,k] &= \eta \: H^k(Q_\tau[u,k,\cdot](d,\pi),\pi_\tau[\cdot \mid u,k]), \; \forall \tau, u, k. \tag{EV.2} \label{eq:EvolutionPolicy}
\end{align}
See~\cite[Section~6]{elokda2021dynamic} for an interpretation. We use $\eta$ as a \emph{policy update rate} parameter, which controls the rate of policy changes~\eqref{eq:EvolutionPolicy} relative to the rate of state changes~\eqref{eq:EvolutionState}, and
\[
    H^k : \Real^{k+1} \times \Delta(\Bids^k) \rightarrow \Real^{k+1}
\]
is one of the familiar \emph{mean dynamics} in evolutionary game theory~\cite[Part~II]{sandholm2010population}. We choose the \emph{perturbed best response dynamic} as our mean dynamic, which allows modelling agents that are not perfectly rational.
The \emph{perturbed best response policy} is given by:
\begin{align}
    \label{eq:PerturbedBestResponse}
    \pbr_\tau[b \mid u,k](d,\pi) := \frac{\exp{(\lambda \: Q_\tau[u,k,b](d,\pi))}}{\sum_{b'} \exp{(\lambda \: Q_\tau[u,k,b'](d,\pi))}},
\end{align}
where the parameter $\lambda$ controls the degree of rationality of the agents.
For $\lambda = 0$, the perturbed best response is a uniform random distribution over all the bids.
At finite values of $\lambda$, it assigns higher probabilities to bids with higher single-stage deviation rewards in a smooth manner.
At the limit $\lambda \rightarrow \infty$, we recover the perfect best response with a uniform random distribution over all the bids maximizing the single-stage deviation rewards.
The policy update dynamics~\eqref{eq:EvolutionPolicy} are then simply
\begin{align}
    \dot{\pi}_\tau[\cdot \mid u,k] = \eta \: (\pbr_\tau[\cdot \mid u,k](d,\pi) - \pi_\tau[\cdot \mid u,k]), \; \forall u, k. \label{eq:PerturbedBestResponseEvolution}
\end{align}
Note that these dynamics lead to perturbed versions of the equilibrium policies $\epi_\tau$ at the rest points, rather than the exact policies~\cite[Section~6.2.4]{sandholm2010population}. However, for a sufficiently large $\lambda$, exact policies can be computed in practice, and we use $\lambda=1000$ in our computations\footnote{Numerical conditioning is required for high $\lambda$ to ensure that the exponential is stable. This is achieved with $\pbr_\tau[b \mid u,k](d,\pi) = \frac{\exp{(\lambda \: Q_\tau[u,k,b](d,\pi) - \max_{b^*} \lambda \: Q_\tau[u,k,b^*](d,\pi))}}{\sum_{b'} \exp{(\lambda \: Q_\tau[u,k,b'](d,\pi) - \max_{b^*} \lambda \: Q_\tau[u,k,b^*](d,\pi))}}$.}.

For computation purposes, we discretize the dynamics~\eqref{eq:EvolutionState}--\eqref{eq:EvolutionPolicy} using the discrete step size $dt$, yielding Algorithm~\ref{alg:StationaryEquilibrium}.

\begin{algorithm}[tb!]
\caption{Stationary Nash equilibrium computation}\label{alg:StationaryEquilibrium}
\textbf{Input:} Initial state distribution $d^0$ with average karma $\kbar$, initial policy $\pi^0$,  parameters $\eta$, $\lambda$, $dt$.

\textbf{Initialize:} $\sd \gets d^0$, $\epi \gets \pi^0$.

\textbf{While} $(\sd, \epi)$ not converged,
\begin{enumerate}
\item Compute:
\begin{enumerate}
\item $\bdist[b'](\sd,\epi)$, $\oprob[o \mid b](\sd,\epi)$, $\kappa[k^+ \mid k,b,o](\sd,\epi)$,
\item $\reward[u,b](\sd,\epi)$, $\transition_\tau[u^+,k^+ \mid u,k,b](\sd,\epi)$,
\item $R_\tau[u,k](\sd,\epi)$, $P_\tau[u^+,k^+ \mid u,k](\sd,\epi)$,
\item $V_\tau[u,k](\sd,\epi)$, $Q_\tau[u,k,b](\sd,\epi)$, and
\item $\pbr_\tau[b \mid u,k](\sd,\epi)$.
\end{enumerate}
\item Forward-propagate the discretized evolutionary dynamics:
\begin{align*}
    \sd_\tau &\gets (1 - dt) \: \sd_\tau + dt \: \sd_\tau \: P_\tau(\sd,\epi), \\
    \epi_\tau[\cdot \mid u,k] &\gets (1 - \eta \: dt) \: \epi_\tau[\cdot \mid u,k] + \eta \: dt \: \pbr_\tau[\cdot \mid u,k](\sd,\epi). \\
\end{align*}
\vspace*{-8mm}
\end{enumerate}
\end{algorithm}

% Figure~\ref{fig:lambda-sweep} shows example stationary Nash equilibrium policy computations for $\lambda \in \{10,100,1000\}$.
% The policies become increasingly deterministic with increasing $\lambda$. We choose $\lambda=1000$ in the subsequent analysis.

% \begin{figure}[bt!]
% 	\centering
% 	\includegraphics[height=.2\textwidth]{tikz/tikz-lambda-10.pdf}
% 	\hfil
% 	\includegraphics[height=.2\textwidth]{tikz/tikz-lambda-100.pdf}
% 	\hfil
% 	\includegraphics[height=.2\textwidth]{tikz/tikz-lambda-1000.pdf}
	
% 	\includegraphics[height=.2\textwidth]{tikz/tikz-lambda-10-karma-dist.pdf}
% 	\hfil
% 	\includegraphics[height=.2\textwidth]{tikz/tikz-lambda-100-karma-dist.pdf}
% 	\hfil
% 	\includegraphics[height=.2\textwidth]{tikz/tikz-lambda-1000-karma-dist.pdf}
	
% 	\caption{Stationary Nash equilibrium policy and karma distribution computations for different values of the rationality parameter $\lambda$. The plots are truncated at $k=20$ for display purposes; the rightmost value of the stationary karma distribution shows the proportion of agents with $k>=20$.}
% 	\label{fig:lambda-sweep}
% \end{figure}