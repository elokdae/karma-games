clc;
screenwidth = 1920;
screenheight = 1080;

% %% Specify file
% file = 'stationary_equilibrium/results/se_U_1_1_10_phi1_0.95_0.05_0.00_0.00_0.50_0.50_0.95_0.05_0.00_alpha1_0.90_alpha2_1.00_pay_0_tax_0.005/k_bar_10_z_0.90_0.10.mat';
% 
% %% Access fairness
% load(file);

% Overall probability of winning
w = dot(se_nu_up_bj, squeeze(gamma_down_b_up_o(:,1)));

% Probability of winning per type
w_down_mu_alpha = zeros(param.n_mu, param.n_alpha);
for i_mu = 1 : param.n_mu
    for i_alpha = 1 : param.n_alpha
        prob_up_b = 1 / param.g_up_mu_alpha(i_mu,i_alpha) * einsum('ijkl,ijklm->m', se_d_up_mu_alpha_u_k(i_mu,i_alpha,:,:), se_pi_down_mu_alpha_u_k_up_b(i_mu,i_alpha,:,:,:));
        w_down_mu_alpha(i_mu,i_alpha) = dot(prob_up_b, squeeze(gamma_down_b_up_o(:,1)));
    end
end

% Probability of winning per type for CENT-U
cent_u_w_down_mu_alpha = zeros(param.n_mu, param.n_alpha);
prob_up_uj = param.prob_down_mu_up_u.' * param.w_up_mu;
for i_mu = 1 : param.n_mu
    cent_u_w_down_mu_alpha(i_mu,:) = 0.5 * param.prob_down_mu_up_u(i_mu,1) * prob_up_uj(1);
    for i_u = 2 : param.n_u
        cent_u_w_down_mu_alpha(i_mu,:) = cent_u_w_down_mu_alpha(i_mu,:) + param.prob_down_mu_up_u(i_mu,i_u) * sum(prob_up_uj(1:i_u-1));
        cent_u_w_down_mu_alpha(i_mu,:) = cent_u_w_down_mu_alpha(i_mu,:) + param.prob_down_mu_up_u(i_mu,i_u) * 0.5 * prob_up_uj(i_u);
    end
end

w_array = w;
labels = "overall";
for i_mu = 1 : param.n_mu
    for i_alpha = 1 : param.n_alpha
        w_array = [w_array, w_down_mu_alpha(i_mu,i_alpha)];
        label_str = strcat("\mu=", int2str(i_mu), ", \alpha=", num2str(param.Alpha(i_alpha), '%.2f'));
        labels = [labels, label_str];
    end
end


%% Plot
figure(1);
fig = gcf;
fig.Position = [0, 0, screenheight, screenheight];
bar(categorical(labels), w_array);
text(categorical(labels), w_array, num2str(w_array.'), 'vert', 'bottom', 'horiz', 'center', 'FontSize', 20, 'FontWeight', 'bold');
axes = gca;
axes.XAxis.FontSize = 15;
axes.XAxis.FontName = 'ubuntu';
axes.YAxis.FontSize = 15;
axes.YAxis.FontName = 'ubuntu';
axes.XLabel.FontName = 'ubuntu';
axes.XLabel.FontWeight = 'bold';
axes.XLabel.FontSize = 20;
axes.YLabel.FontName = 'ubuntu';
axes.YLabel.FontWeight = 'bold';
axes.YLabel.String = 'Average access';
axes.YLabel.FontSize = 20;