\section{Karma mechanisms for dynamic resource allocation}
\label{sec:karma}

% \begin{figure}[bt]
% 	\centering
% 	\includegraphics[width=0.65\textwidth]{figures/interactions-Ct.pdf}
% 	\caption{Example dynamic resource allocation problem, where the resource is the right of way in intersections. Anonymous agents in a large population get repeatedly and randomly matched in resource competition instances $\{\Comp_{t_n}\}$. A karma mechanism can be used to select who to allocate the resource to.}
% 	\label{fig:IntersectionExample}
% \end{figure}


% In order to provide an interpretation for these definitions, we refer to the motivating example of \emph{intersection management in a road network} throughout the presentation.
We consider a population of agents $\N = \{1,\dots,N\}$, where the number of agents $N$ is typically large. For example, $\N$ is the set of ride-hailing platform riders in the metropolitan area of interest.

At discrete global time instants $t \in \Int$, two random agents from the population (denoted by $\Comp[t] \subset \N$) compete for a scarce, indivisible resource, such as the closest ride-hailing driver
%right of way in an intersection
.
%(see Figure~\ref{fig:IntersectionExample}).
We are concerned with designing a mechanism that, at each interaction time $t$, selects one of the two agents in $\Comp[t]$ to allocate the resource to (i.e., grant the trip request).
% Once the resource is allocated, a new instance of the resource competition is formed with a new pair of randomly matched agents from the population $\Comp_{t_{n+1}} \subset \N$.

A karma mechanism works as follows.
Each agent $l \in \N$ in the population is endowed with a non-negative integer counter $k^l[t] \in \Int$, called \emph{karma}, which is private to the agent.
Moreover, an additional \emph{surplus karma} counter $\ksur[t] \in \Int$ exists in the system.

At each interaction time $t$, each agent $i \in \Comp[t]$ involved in the resource competition submits a sealed non-negative integer bid $b^i[t] \in \{0,\dots,k^i[t]\}$, which is bounded by the agent's karma. The outcome of the interaction is determined by a \emph{resource allocation rule} and a \emph{payment} rule.
The \emph{resource allocation rule} decides which of the two competing agents is selected to receive the contended resource.
%(i.e., pass first in the intersection). 

\smallskip
\begin{center}
    \fbox{
        \begin{minipage}{0.92\textwidth}
            \begin{center}
                \textbf{Resource allocation rule}
            \end{center}
            
            \parbox{15mm}{\textbf{Input:}} 
            sealed bids $b^i[t]$ of the two competing agents $i \in \Comp[t]$.
            
            \medskip
            
            \parbox{15mm}{\textbf{Output:}} 
            selected agent $i^*[t] \in \Comp[t]$ to receive the resource,
            
            \[
                i^*[t] = \argmax_{i \in \Comp[t]} \: b^i[t].
            \]
        \end{minipage}
    }
\end{center}
\smallskip

It is natural to consider a resource allocation rule that allocates the resource to the agent with the highest bid.
A tie breaking rule is needed when both bids coincide, and we use a fair coin toss for this purpose.

The \emph{karma payment rule} determines the karma payments of the two competing agents.

%\def\paymentselected{\Deltaminus k_{t_n}^{i^*_{t_n}}} 
\def\paymentwinner{p^{i^*}[t]}
%\def\paymentloser{\Deltaminus k_{t_n}^{-i^*_{t_n}}}
\def\paymentloser{p^{-i^*}[t]}

\smallskip
\begin{center}
    \fbox{
        \begin{minipage}{0.92\textwidth}
            \begin{center}
                \textbf{Karma payment rule}
            \end{center}
            
            \parbox{15mm}{\textbf{Input:}}  \begin{minipage}[t]{\textwidth}
                sealed bids $b^i[t]$ of the two competing agents $i \in \Comp[t]$;\\
                selected agent $i^*[t]$ and yielding agent $-i^*[t] = \Comp[t] \setminus i^*[t]$.
            \end{minipage}
            
            \medskip
            
            \parbox{15mm}{\textbf{Output:}}  \begin{minipage}[t]{\textwidth}
                %\begin{itemize}[leftmargin=*]
                    %\item Karma payment from the selected agent $\Deltaminus k_{t_n}^{i^*_{t_n}} \in \{0,\dots,b^{i^*_{t_n}}_{t_n}\}$.
                    karma payment of the selected agent 
                    $0 \le \paymentwinner \le b^{i^*}[t]$;\\
                    %\item Karma payment to the non-selected agent $\Deltaplus k_{t_n}^{-i^*_{t_n}} \in \{0,\dots,\Deltaminus k_{t_n}^{i^*_{t_n}}\}$.
                    karma payment of the yielding agent 
                    $-\paymentwinner \le \paymentloser \le 0$.
                    %\item Karma payment to the central ledger $\Deltaplus K_{t_n} = \Deltaminus k_{t_n}^{i^*_{t_n}} - \Deltaplus k_{t_n}^{-i^*_{t_n}}$.
                    %\item surplus karma $\Deltaplus k_{t_n}^\text{s} = \Deltaminus k_{t_n}^{i^*_{t_n}} + \Deltaminus k_{t_n}^{-i^*_{t_n}}$.
                %\end{itemize}
            \end{minipage}
        \end{minipage}
    }
\end{center}
\smallskip

Note that the yielding agent makes a non-positive payment (i.e., it receives karma). As a consequence of this payment rule, at each interaction time $t$, the karma counters are updated as follows:
\begin{align*}
    k^{i^*}[t+1] & \leftarrow k^{i^*}[t] - \paymentwinner
    && \text{(selected agent $i^*[t]$)}, \\
    k^{-i^*}[t+1] & \leftarrow k^{-i^*}[t] - \paymentloser
    && \text{(yielding agent $-i^*[t]$)}, \\
    \ksur[t+1] & \leftarrow \ksur[t] + \paymentwinner + \paymentloser
    && \text{(surplus karma)}.
\end{align*}
Examples of karma payment rules are presented in Section~\ref{subsec:KarmaTransfer}. The surplus karma $\ksur[t]$ is meant to keep track of any excess karma payment in the interaction, in case $\paymentwinner + \paymentloser \neq 0$, such that this excess gets \emph{redistributed} to the population agents.
This redistribution occurs at time instants $\tredist$ in accordance with a \emph{karma redistribution rule}.

\def\redistribution{r^l[\tredist]}
%\Deltaplus k^i_{t^r_m}

\smallskip
\begin{center}
    \fbox{
        \begin{minipage}{0.92\textwidth}
            \begin{center}
                \textbf{Karma redistribution rule}
            \end{center}
            
            \parbox{15mm}{\textbf{Input:}}  \begin{minipage}[t]{0.7\textwidth}
                %\begin{itemize}[leftmargin=*]
                    %\item Discrete time instances $\{t^r_m\}_{m \in \Int}$ at which to perform the redistribution.
                    karma $k^l[\tredist]$ of all the agents $l \in \N$;\\
                    surplus karma $\ksur[\tredist]$.
                %\end{itemize}
            \end{minipage}
            
            \medskip
            
            \parbox{15mm}{\textbf{Output:}}  \begin{minipage}[t]{0.7\textwidth}
                %\begin{itemize}[leftmargin=*]
                    %\item Karma payment from the central ledger $\Deltaminus K_{t^r_m} \in \{0,\dots,K_{t^r_m}\}$.
                    %\item Karma payments to each agent $\Deltaplus k^l_{t^r_m}$ satisfying $\sum_l \Deltaplus k^l_{t^r_m} = \Deltaminus K_{t^r_m}$.
                    karma redistribution $\redistribution$
                    to each agent $l \in \N$.
%                    \item surplus karma $\Deltaminus k_{t^r_m}^\text{s} = \Deltaminus k_{t_n}^{i^*_{t_n}} + \Deltaminus k_{t_n}^{-i^*_{t_n}}$.
 %                                       $\Deltaminus K_{t^r_m} \in \{0,\dots,K_{t^r_m}\}$.
%                \end{itemize}
            \end{minipage}
        \end{minipage}
    }
\end{center}
\smallskip

As a consequence of this redistribution rule, at each redistribution time $\tredist$ the karma counters are updated as follows:
\begin{align*}
    k^{l}[\tredist+1] & \leftarrow k^{l}[\tredist] + \redistribution
    && \text{(every agent $l$)}, \\
    \ksur[\tredist+1] & \leftarrow \ksur[\tredist] - \sum_l \redistribution 
    && \text{(surplus karma)}.
\end{align*}

% \begin{center}
%     \fbox{
%         \parbox{0.9\textwidth}{
%             \begin{center}
%                 \textbf{Karma transfer rule}
%             \end{center}
            
%             \textbf{Input:\phantom{Output}} \begin{minipage}[t]{0.7\textwidth}
%                 \begin{itemize}[leftmargin=*]
%                     \item Sealed bids $b^i$ of the involved agents $i \in \Comp_{t_n}$.
%                     \item Selected agent $i^*_{t_n}$ and non-selected agent $-i^*_{t_n} = \Comp_{t_n} \setminus i^*_{t_n}$.
%                 \end{itemize}
%             \end{minipage}
            
%             \bigskip
            
%             \textbf{Output:\phantom{Input}} \begin{minipage}[t]{0.7\textwidth}
%                 \begin{itemize}[leftmargin=*]
%                     \item Change of karma of the selected agent $-b^{i^*_{t_n}}_t \leq \Delta k^{i_t^*}_t \leq 0$.
%                     \item Change of karma of the non-selected agent $0 \leq \Delta k^{-i_t^*}_t \leq - \Delta k^{i_t^*}_t$.
%                     \item Excess karma $\delta k_t = -\Delta k^{i_t^*}_t - \Delta k^{-i_t^*}_t$.
%                 \end{itemize}
%             \end{minipage}
            
%             \medskip
%         }
%     }
% \end{center}

A considerable freedom in the design of the karma payment and redistribution rules is possible.
Hereafter, we present some possible examples, and in Section~\ref{sec:NumericalAnalysis} we demonstrate how the choice of these rules affects the strategic behavior of the agents and allows the system designer to achieve different resource allocation objectives.

% In Section~\ref{sec:NumericalAnalysis}, we demonstrate how the choice of the karma transfer rule affects the strategic behavior of the agents and allows the system designer to strike trade-offs in the achieved fairness and efficiency of the resource allocation.
% The \emph{excess karma} $\delta k_t$ represents an amount of karma that does not get transferred directly from the agent that receives the resource to the agent that yields it, but rather \emph{goes to society}.
% We consider that at discrete time instances $T_m \in \T = \{T_1, T_2, \dots\}$, the total excess karma collected between $T_{m-1}$ and $T_m$, given by $\delta K_m = \sum_{t=T_{m-1}+1}^{T_m} \delta k_t$ (with $T_0 = -1$), is re-distributed to all the agents in the population.
% Consequently, the karma of the agents changes in time according to the following dynamics
% \begin{align}
%     (k')^l_{t+1} &= \begin{cases}
%         k^l_t + \Delta k^l_t, &\text{if } l \in \Comp_t, \\
%         k^l_t, &\text{if } l \notin \Comp_t,
%     \end{cases} \\
%     k^l_{t+1} &= \begin{cases}
%         (k')^l_{t+1}, &\text{if } t \notin \T, \\
%         (k')^l_{t+1} + \frac{\delta K_m}{n},  &\text{if } t = T_m\footnotemark.
%     \end{cases} \label{eq:KarmaDynamics}
% \end{align}

% \footnotetext{Without a major loss of generality, we assume that the times $T_m$ coincide with instances where $\delta K_m$ is an integer multiple of $n$. Alternatively, $\delta K_m$ can be distributed in a slightly uneven manner to ensure that $k^l_{t+1}$ remains integer for all the agents $l \in \N$.}

% Notice that these dynamics satisfy the following important principle.

% \begin{definition}[Karma preservation]
% \label{def:KarmaPreserve}
% For all time instances $T_m \in \T$, the average amount of karma in the population satisfies
% \begin{align}
%     \frac{1}{n} \sum_{l \in \N} k^l_{T_m+1} = \frac{1}{n} \sum_{l \in \N} k^l_{T_{m-1}+1}. \tag{KP} \label{eq:KarmaPreservation}
% \end{align}
% \end{definition}

% This means that the average amount of karma in the population is preserved at the time instances $T_m$, and hence the supply of karma (and consequently, its value) does not inflate or deflate on the long run. Therefore, one can choose the desired average amount of karma $\kbar \in \K$, and initialize the system at time $t=0$ with it such that $\frac{1}{n} \sum_{l \in \N} k^l_0 = \kbar$, and this average will be preserved. Note that we can preserve this average also when new users enter the system by giving them $\kbar$ upon entry.

\subsection{Examples of karma payment rules}
\label{subsec:KarmaTransfer}



\smallskip
\begin{center}
    \fbox{
        \begin{minipage}{0.92\textwidth}
            \begin{center}
                \textbf{Pay bid to peer} ($\texttt{PBP}$)
            \end{center}
            
            The selected agent pays its bid directly to the yielding agent, i.e.,
            \[
                \paymentwinner = -\paymentloser = b^{i^*}[t].
            \]
        \end{minipage}
    }
\end{center}
\smallskip

$\texttt{PBP}$ is an example of completely peer-to-peer karma payment rules. It has the advantage of not requiring the system-level surplus karma counter $\ksur [t]$ or any system-wide karma redistribution.
% In our motivating example, the two competing AVs can perform the transaction in a completely decentralized manner, e.g., through a cryptographic implementation.

% \smallskip
% \begin{center}
%     \fbox{
%         \parbox{0.9\textwidth}{
%             \begin{center}
%                 \textbf{Pay one to peer} ($\texttt{P1P}$)
%             \end{center}
            
%             The selected agent pays $1$ directly to the yielding agent (if it bid $1$ or more), i.e.,
%             \[
%                 \paymentwinner = - \paymentloser = \min\{1,b^{i^*_{t_n}}_{t_n}\}.
%             \]
%         }
%     }
% \end{center}

\smallskip
\begin{center}
    \fbox{
        \begin{minipage}{0.92\textwidth}
            \begin{center}
                \textbf{Pay bid to society} ($\texttt{PBS}$)
            \end{center}
            
            The selected agent pays its bid and therefore creates surplus karma (to be later redistributed), i.e., 
            \[
                \paymentwinner = b^{i^*}[t], \quad 
                \paymentloser = 0.
            \]
        \end{minipage}
    }
\end{center}
\smallskip

In contrast to $\texttt{PBP}$, $\texttt{PBS}$ is an example of a karma payment rule in which surplus karma is generated and needs to be redistributed.
We will demonstrate in the numerical analysis in Section~\ref{sec:NumericalAnalysis} that such a redistributive scheme can lead to higher levels of efficiency and fairness of the resource allocation.

% \smallskip
% \begin{center}
%     \fbox{
%         \parbox{0.9\textwidth}{
%             \begin{center}
%                 \textbf{Pay bid to peer with fee} ($\texttt{PBP+FEE}$)
%             \end{center}
            
%             Let $0 \leq v[b] \leq b$ be a non-decreasing fee based on the bid $b$.
%             The selected agent pays its bid minus the fee directly to the yielding agent, while the fee contributes to the surplus karma (to be later redistributed), i.e.,
%             \[
%                 \paymentwinner = b^{i^*_{t_n}}_{t_n}, \quad
%                 -\paymentloser = b^{i^*_{t_n}}_{t_n} - v[b^{i^*_{t_n}}_{t_n}].
%             \]
%         }
%     }
% \end{center}
% \smallskip

% Note that both $\texttt{PBP}$ and $\texttt{PBS}$ are special cases of $\texttt{PBP+FEE}$, the former with $v[b] = 0$ and the latter with $v[b]=b$.

\subsection{Examples of karma redistribution rules}

There are a plethora of methods to redistribute the surplus karma to the agents in the population, as the designer has the freedom to decide both the redistribution times $\tredist$ and the redistribution rule. 
Nevertheless, we assume that redistribution rules are intended to completely redistribute the surplus karma so that most of the karma in the system is held by the agents, i.e., the total karma held by the agents is a \emph{preserved quantity}. We will formalize this assumption in the analysis in Section~\ref{sec:KarmaTransferModel}, where we will effectively assume that the surplus is kept at zero (either by not generating surplus or by redistributing it immediately).

One possibility is for the time instants $\tredist$ to be periodic events in which the redistribution occurs (e.g., every day at midnight).
If $\ksur[\tredist]$ is not an integer multiple of the number of agents $N$, %either the whole amount can be non-uniformly redistributed such that each agent receives an integer amount, or 
then a remainder is left to be redistributed in the next period.
%
Another possibility is for the redistribution to occur asynchronously whenever $\ksur[t]$ exceeds $N$, so that a unit of karma per agent can be transferred. 
%
Finally, a non-uniform (possibly randomized) redistribution is possible.

% \smallskip
% \begin{center}
%     \fbox{
%         \parbox{0.9\textwidth}{
%             \begin{center}
%                 \textbf{Uniform karma redistribution} (for $\texttt{PBS}$, $\texttt{PBP+FEE}$, ...)
%             \end{center}
%             Surplus karma is transfered equally to all agents, i.e. 
%             \begin{align*}
%                 \redistribution = \left\lfloor \frac{\ksur_{\tredist_n}}{N}\right\rfloor.
%             \end{align*}
%         }
%     }
% \end{center}
% \smallskip

Beyond these examples, we will not detail here the specifics of the karma redistribution rule.
%
%In Section~\ref{subsec:KarmaTransferModel} we will consider a specific synchronous karma redistribution rule with further assumptions on the times at which the agents interact $\{t_n\}_{n \in \Int}$ for the purpose of a simpler analysis.
%
However, it is interesting to notice that redistributions need not to be limited to positive karma values. For example, it is possible to reduce the karma of each agent according to a non-decreasing ``tax'' function $0 \leq h[k] \leq k$, in order to create surplus (which will then be redistributed). 
We will briefly discuss the consequences of such a design in the numerical analysis in Section~\ref{sec:NumericalAnalysis}.


% \smallskip
% \begin{center}
%     \fbox{
%         \parbox{0.9\textwidth}{
%             \begin{center}
%                 \textbf{Karma capital tax} ($\texttt{TAX}$)
%             \end{center}
            
%             Each agent contributes to generate surplus karma according to a non-decreasing tax $0 \leq h[k] \leq k$ on its karma $k$, i.e.,
%             \begin{align*}
%                 \redistribution = -h[k^l_{\tredist_n}].
%             \end{align*}
%         }
%     }
% \end{center}
% \smallskip


% \begin{center}
%     \fbox{
%         \parbox{0.9\textwidth}{
%             \begin{center}
%                 \textbf{Karma capital tax} ($\texttt{TAX}$)
%             \end{center}
            
%             At the time instances $T_m \in \T$, apply a tax on the agents' next karma and re-distribute the total to all the agents in the population.
%             Let $(k_\textup{pre})^l_{T_m+1}$ be the agents' next karma before applying the tax, as given by~\eqref{eq:KarmaDynamics}.
%             Let $h[k_\textup{pre}]$ be a capital tax rule satisfying
%             \begin{enumerate}
%                 \item $h[k_\textup{pre}] \leq k_\textup{pre}$, and
%                 \item $0 \leq h[k_\textup{pre} + 1] - h[k_\textup{pre}] \leq 1$,
%             \end{enumerate}
%             i.e., the tax is never greater than the karma, is non-decreasing in the karma, and preserves the order of karma after applying it.
%             Then,
%             \begin{align*}
%                 H_{T_m} &= \sum_{l \in \N} h[(k_\textup{pre})^l_{T_m+1}], \\
%                 k^l_{T_m+1} &= (k_\textup{pre})^l_{T_m+1} - h[(k_\textup{pre})^l_{T_m+1}] + \frac{H_{T_m}}{n}.
%             \end{align*}
%         }
%     }
% \end{center}