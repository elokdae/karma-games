% Write efficiency of multiple alpha types to csv file
function write_efficiency_mult_alpha_csv(z1_eff_comp, se_access_eff, se_cost_eff, rand_E, u_E, param, fileprefix)
    % Pre-processing
    n_z1_eff_comp = length(z1_eff_comp);
    z1_eff_comp = reshape(z1_eff_comp, [], 1);
    i_ALPHA = [(1 : param.n_alpha).'; -1];
    ALPHA = [reshape(param.Alpha, [], 1); -1];
    RAND_E = rand_E * ones(param.n_alpha + 1, 1);
    U_E = u_E * ones(param.n_alpha + 1, 1);

    % Header
    header = ["z-1", "z-2", "i-alpha", "alpha", "w", "e", "e-rand", "e-u"];
    filename = [fileprefix, '-efficiency.csv'];
    fout = fopen(filename, 'w');
    for i = 1 : length(header) - 1
        fprintf(fout, '%s,', header(i));
    end
    fprintf(fout, '%s\n', header(end));
    fclose(fout);

    % Data
    for i_z1_eff_comp = 1 : n_z1_eff_comp
        Z1 = z1_eff_comp(i_z1_eff_comp) * ones(param.n_alpha + 1, 1);
        Z2 = 1 - Z1;
        data = [Z1, Z2, i_ALPHA, ALPHA, se_access_eff(:,i_z1_eff_comp), se_cost_eff(:,i_z1_eff_comp), RAND_E, U_E];
        dlmwrite(filename, data, '-append');
    end
end