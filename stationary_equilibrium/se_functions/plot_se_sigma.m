% Plot SE karma distribution
function plot_se_sigma(fg, position, se_sigma_down_mu_alpha_up_k, param, se_param, i_alpha_comp)
persistent se_sigma_plot
    if ~ishandle(fg)
        figure(fg);
        fig = gcf;
        fig.Position = position;
        se_sigma_plot = cell(param.n_mu, param.n_alpha);
        n_subplots = param.n_mu * param.n_alpha;
        n_cols = ceil(sqrt(n_subplots));
        n_rows = ceil(n_subplots / n_cols);
        for i_mu = 1 : param.n_mu
            for i_alpha = 1 : param.n_alpha
                i_subplot = (i_mu - 1) * param.n_alpha + i_alpha;
                subplot(n_rows, n_cols, i_subplot);
                se_sigma_plot{i_mu,i_alpha} = bar(se_param.K, squeeze(se_sigma_down_mu_alpha_up_k(i_mu,i_alpha,:)));
                axis tight;
                axes = gca;
                axes.Title.FontName = 'ubuntu';
                alpha = param.Alpha(max([i_alpha, i_alpha_comp]));
                if alpha > 0.99 && alpha < 1
                    alpha_str = num2str(alpha, '%.3f');
                else
                    alpha_str = num2str(alpha, '%.2f');
                end
                axes.Title.String = ['k_{bar} = ', num2str(param.k_bar, '%02d'), ' SE karma distribution for \mu = ', num2str(i_mu), ', \alpha = ', alpha_str];
                axes.Title.FontSize = 20;
                axes.XAxis.FontSize = 20;
                axes.XAxis.FontName = 'ubuntu';
                axes.YAxis.FontSize = 20;
                axes.YAxis.FontName = 'ubuntu';
                axes.XLabel.FontName = 'ubuntu';
                axes.XLabel.FontWeight = 'bold';
                axes.XLabel.String = 'Karma';
                axes.XLabel.FontSize = 20;
                axes.YLabel.FontName = 'ubuntu';
                axes.YLabel.FontWeight = 'bold';
                axes.YLabel.String = 'Distribution';
                axes.YLabel.FontSize = 20;
            end
        end
    else
        for i_mu = 1 : param.n_mu
            for i_alpha = 1 : param.n_alpha
                se_sigma_plot{i_mu,i_alpha}.YData = squeeze(se_sigma_down_mu_alpha_up_k(i_mu,i_alpha,:));
                alpha = param.Alpha(max([i_alpha, i_alpha_comp]));
                if alpha > 0.99 && alpha < 1
                    alpha_str = num2str(alpha, '%.3f');
                else
                    alpha_str = num2str(alpha, '%.2f');
                end
                se_sigma_plot{i_mu,i_alpha}.Parent.Title.String = ['k_{bar} = ', num2str(param.k_bar, '%02d'), ' SE karma distribution for \mu = ', num2str(i_mu), ', \alpha = ', alpha_str];
            end
        end
    end
end