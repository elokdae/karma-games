% Plot SE karma distribution
function plot_se_nu(fg, position, se_nu_up_bj, param, se_param, i_alpha_comp)
persistent se_nu_plot
    if ~ishandle(fg)
        figure(fg);
        fig = gcf;
        fig.Position = position;
        se_nu_plot = bar(se_param.K, se_nu_up_bj);
        axis tight;
        axes = gca;
        axes.Title.FontName = 'ubuntu';
        alpha = param.Alpha(i_alpha_comp);
        if alpha > 0.99 && alpha < 1
            alpha_str = num2str(alpha, '%.3f');
        else
            alpha_str = num2str(alpha, '%.2f');
        end
        axes.Title.String = ['k_{bar} = ', num2str(param.k_bar, '%02d'), ' SE bid distribution for \alpha = ', alpha_str];
        axes.Title.FontSize = 20;
        axes.XAxis.FontSize = 20;
        axes.XAxis.FontName = 'ubuntu';
        axes.YAxis.FontSize = 20;
        axes.YAxis.FontName = 'ubuntu';
        axes.XLabel.FontName = 'ubuntu';
        axes.XLabel.String = 'Bid';
        axes.XLabel.FontSize = 20;
        axes.YLabel.FontName = 'ubuntu';
        axes.YLabel.String = 'Distribution';
        axes.YLabel.FontSize = 20;
    else
        se_nu_plot.YData = se_nu_up_bj;
        alpha = param.Alpha(i_alpha_comp);
        if alpha > 0.99 && alpha < 1
            alpha_str = num2str(alpha, '%.3f');
        else
            alpha_str = num2str(alpha, '%.2f');
        end
        se_nu_plot.Parent.Title.String = ['k_{bar} = ', num2str(param.k_bar, '%02d'), ' SE bid distribution for \alpha = ', alpha_str];
    end
end