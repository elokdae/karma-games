clc;
screenwidth = 1920;
screenheight = 1080;

%% Some parameters
i_mu = 1;
i_alpha = 1;
alpha_fair_comp = [0.15 : 0.05 : 0.95, 0.96 : 0.01 : 1];
n_alpha_fair_comp = length(alpha_fair_comp);

file_start = 'stationary_equilibrium/results/se_U_1_phi1_1.00_pay_0/k_bar_10_alpha_';

T = 0 : 20;
n_T = length(T);

%% Corrective fairness
w_down_alpha_T = zeros(n_alpha_fair_comp, n_T);
sigma_up_u_k = cell(n_alpha_fair_comp, n_T);

for i_alpha_fair_comp = 1 : n_alpha_fair_comp
    alpha = alpha_fair_comp(i_alpha_fair_comp);
    file = [file_start, num2str(alpha, '%.2f'), '.mat'];
    load(file);
    
    % Massage policy to ensure win at (u_max,k_max)
    se_pi_down_mu_alpha_u_k_up_b(i_mu,i_alpha,end,end,:) = circshift(se_pi_down_mu_alpha_u_k_up_b(i_mu,i_alpha,end,end,:), 1);
    se_pi_down_mu_alpha_u_k_up_b(i_mu,i_alpha,end,end,end) = se_pi_down_mu_alpha_u_k_up_b(i_mu,i_alpha,end,end,end) + se_pi_down_mu_alpha_u_k_up_b(i_mu,i_alpha,end,end,1);
    se_pi_down_mu_alpha_u_k_up_b(i_mu,i_alpha,end,end,1) = 0;
%     se_pi_down_mu_alpha_u_k_up_b(i_mu,i_alpha,end,end,:) = 0;
%     se_pi_down_mu_alpha_u_k_up_b(i_mu,i_alpha,end,end,end) = 1;
    
    sigma_up_u_k{i_alpha_fair_comp,1} = reshape(se_d_up_mu_alpha_u_k(i_mu,i_alpha,:,:) / param.g_up_mu_alpha(i_mu,i_alpha), param.n_mu, se_param.n_k);
    % sigma_up_u_k{1} = zeros(param.n_u, se_param.n_k);
    % sigma_up_u_k{1}(1,1) = 1;
    prob_up_b = einsum('ijklm,kl->m', se_pi_down_mu_alpha_u_k_up_b(i_mu,i_alpha,:,:,:), sigma_up_u_k{i_alpha_fair_comp,1});
    w_down_alpha_T(i_alpha_fair_comp,1) = dot(prob_up_b, squeeze(gamma_down_b_up_o(:,1)));

    for t = 2 : n_T
        prob_up_u_k_b = einsum('ijklm,kl->klm', se_pi_down_mu_alpha_u_k_up_b(i_mu,i_alpha,:,:,:), sigma_up_u_k{i_alpha_fair_comp,t-1});
        prob_up_u_k_b_lose = einsum('ijk,kl->ijk', prob_up_u_k_b, gamma_down_b_up_o(:,2));
        prob_up_u_kn_lose = einsum('ijk,jklm->im', prob_up_u_k_b_lose, kappa_down_k_b_o_up_kn(:,:,2,:));
        if sum(prob_up_u_kn_lose(:)) > 0
            prob_up_u_kn_lose = prob_up_u_kn_lose / sum(prob_up_u_kn_lose(:));
        end
        sigma_up_u_k{i_alpha_fair_comp,t} = einsum('ijk,jl->kl', param.phi_down_mu_u_up_un(i_mu,:,:), prob_up_u_kn_lose);
        prob_up_b = einsum('ijklm,kl->m', se_pi_down_mu_alpha_u_k_up_b(i_mu,i_alpha,:,:,:), sigma_up_u_k{i_alpha_fair_comp,t});
        w_down_alpha_T(i_alpha_fair_comp,t) = dot(prob_up_b, squeeze(gamma_down_b_up_o(:,1)));
    end
end

%% Plot
figure(1);
fig = gcf;
fig.Position = [0, 0, screenheight, screenheight];
plot(T, 0.5 * ones(size(T)), 'LineWidth', 2);
hold on;
plot(T, 1.0 * ones(size(T)), ':k', 'LineWidth', 1);
plot(T, w_down_alpha_T, '--o', 'LineWidth', 2, 'MarkerSize', 10);
axes = gca;
axis_semi_tight(axes, 1.2);
axes.XAxis.FontSize = 15;
axes.XAxis.FontName = 'ubuntu';
axes.YAxis.FontSize = 15;
axes.YAxis.FontName = 'ubuntu';
axes.XLabel.FontName = 'ubuntu';
axes.XLabel.FontWeight = 'bold';
axes.XLabel.String = 'Previous denials of service';
axes.XLabel.FontSize = 20;
axes.YLabel.FontName = 'ubuntu';
axes.YLabel.FontWeight = 'bold';
axes.YLabel.String = 'Probability of getting service';
axes.YLabel.FontSize = 20;