\section{Introduction}
\label{sec:Intro}

The advent of autonomous systems that interact with each other \rev{in unstructured environments, e.g.,} outside of the factory floor\rev{,} presents an unprecedented challenge: the need for coordination when these systems require access to shared resources, with potentially conflicting interests.
While this challenge is not unique \rev{to} autonomous systems, their communication and computation capabilities enable coordinat\rev{ed solutions and desirable outcomes} that have not been possible thus far.

Consider, as a running example throughout the paper, a scenario where two autonomous vehicles (AVs) meet at an intersection.
Naturally, each AV is interested in getting its traveller to the destination fastest, and therefore wants to go first in the intersection. %But only one of the AVs can go first.
Rudimentary methods can be used to regulate access to the intersection, such as the traffic light, the stop sign, or the `right goes first' rule.
These methods are ought to become obsolete, since they disrupt the traffic flow, yield an essentially random allocation of the right of way, and do not make use of the AV's capabilities to communicate and self-coordinate.

It is important to notice that AVs, and autonomous systems in general, are generally capable of regulating their actions so that critical events like collisions are averted, and some kind of unprincipled sharing of the common resources may emerge from this capability.
However, when the contended resource is valuable and public, some notion of fairness in the use of such a resource is generally sought. 
Humans often have a way to incorporate some simple notion of fairness in their coordination strategies. 
Using subtle driving maneuvers and possibly hand gestures, human drivers are able to signal their intent and are willing to yield occasionally when we feel that we have been treated generously a few times, even if we are likely intersecting with a different person now than before~\rev{\cite{nandavar2019understanding}}.
In some cases this `give and take' behavior is partly codified, e.g., in the all-way stop intersections and in the zipper lane merge.

This work is an attempt to systematize the `give and take' such that it can be applied unambiguously in autonomous systems, and builds upon the exploratory concept proposed in~\cite{censi2019today}. The main enabler is \emph{karma}, a counter that encodes the history of `giving and taking' of the users.
Inspired by the notion of karma in Indian tradition~\cite{doniger1980karma}, each user is endowed with karma points which increase when the user yields the resource, and decrease when the user accesses the resource.
A user with a high level of karma was likely yielding in the past, and gets an advantage in receiving the resource now. In turn, the disfavored user that yields now gets compensated in karma which will give it an advantage in the future.

In its simplest form, such a \emph{karma mechanism} is an effective means to facilitate \emph{turn-taking} on the large scale, when there is a large number of users who need not know each others\rev{'} identities.
But a karma mechanism can do more than simple turn-taking.
If users are given the option to choose how much karma to use now, e.g., through an auction-like \emph{karma bidding} scheme, the karma becomes also a means to express \emph{private temporal preferences}.
For example, a user may decide to yield (and gain karma) when its urgency is low, in anticipation to the situation where accessing the resource is time-critical.
A karma mechanism can hence facilitate the allocation of the resource to whoever needs it most, i.e., resource allocation \emph{efficiency}.

A classical device that can be used to express preferences is money, and indeed karma shares similarities with money in how it acts as a token of exchange.
\rev{In the context of our AVs example, a few works have proposed the use of monetary auctions for autonomous intersection management~\cite{carlino2013auction}.
A larger body of literature exists on the related problem of congestion management, where decades of research on the use of monetary road pricing policies has been faced with little public acceptance~\cite{yang2011managing}, since such policies essentially constitute `building roads for the rich'.
T}he distinguishing feature of karma is that it is acquired from \emph{fair exchanges} that are relevant to the resource allocation problem at hand.
One need not worry about matters of wealth inequality, or rely on the assumption that money is a universal, objective measure of value, since karma is only acquired from the process of yielding the resource to another user.
Karma hence facilitates the design of a purpose-built, self-contained economy for the resource allocation task.
Like in monetary economies, the karma economy can be \emph{tuned} to achieve different fairness and efficiency objectives in a manner that is targeted to the specific resource allocation task, through the design of karma payment rules, the redistribution of karma, and other techniques that we explore here.

\rev{Karma mechanisms promise to be efficient and fair, but these unconventional mechanisms require a novel analysis.
A difficulty in the analysis arises} due to the lack of reliance on an extrinsic measure of value. Karma does not have value in itself, and is never used directly in the \rev{cost} functions of the users.
The value of karma instead arises from how it facilitates access to the resource, and how users need to ration its use in order to cover their future resource access demands.
This makes the behavior of rational users under the karma mechanism, and the resulting social welfare, difficult to predict, and requires the formulation of non-trivial dynamic games played in large populations.
In this work, and in comparison to~\cite{censi2019today}, we develop a tractable and rigorous game-theoretic model to study karma mechanisms that is built on top of the class of \emph{dynamic population games}~\cite{elokda2021dynamic}, and prove the existence of a suitable notion of equilibrium, the \emph{stationary Nash equilibrium}.
% , and provide analytical tools to study social welfare measures of interest at the equilibrium.
We then utilize the tools developed to numerically investigate the strategic behaviors that emerge under the karma mechanisms, their consequences for the social welfare, as well as how the karma mechanism can be tuned to achieve different resource allocation objectives.
% different karma mechanism designs which are tailored to case studies involving different assumptions on the users' private utilities and degree of strategic competence.

\subsection{Related works}

\subsubsection{Governing common resources}

The class of resource allocation problems we consider here
% \rev{, such as our running example of AVs competing over common road space,}
shares similarities to the \emph{common-pool resources} (CPRs) extensively studied in~\cite{ostrom1990governing}.
% For example, the intersections disputed by the AVs are common-pool resources.
There, multiple aspects to the challenge of governing common-pool resources are identified\rev{.
These }include the \emph{problem of appropriation}, that is, under what rules should resource units be allocated to the users\rev{;} the \emph{problem of credible commitment}, that is, how to ensure that the users follow the appropriation rules\rev{;} and the \emph{problem of mutual monitoring}, which concerns with whether the users have an incentive to monitor and punish others in the case of infringements~\cite[Chapter~2]{ostrom1990governing}.
The karma mechanisms we propose target the problem of appropriation.
%, while the problem of ensuring that users of the karma system actually follow it remains open. In the context of intersection management for AVs, some degree of central enforcement is likely required by a consortium of transport officials, AV manufacturers, and other stakeholders.
%Nevertheless, 
As Ostrom points out, it is fundamental to the success of the self-government of a CPR that the appropriation method is deemed as ``fair, orderly, and efficient'' by the users of the resource system, such that they have a motivation to participate in and contribute to the system.

%In~\cite{ostrom1990governing}, a number of successful resource appropriation methods adopted in real CPR problems are presented.
%A representative example is the system devised by local fishermen to manage the inshore fishery in Alanya, Turkey~\cite{berkes1986local}, where the fishing grounds are divided in locations for single use, and the fishermen are assigned a schedule to the fishing locations which rotates each day, such that all fishermen get to fish in all locations.
Interestingly, a defining feature of many appropriation methods presented in~\cite{ostrom1990governing} (e.g., scheduling of inshore fishing in Alanya, Turkey~\cite{berkes1986local}) is that they facilitate turn-taking.
Karma mechanisms also facilitate turn-taking, but in a setting where there is a large number of users who need not know each other, and need not interact with the same users again.
Moreover, karma mechanisms enable the users to \emph{express preferences} through their bids, which is instrumental to the efficiency of the resource allocation whenever there is heterogeneity in the preferences.

\subsubsection{Mechanism design without money}

The famous Gibbard-Satterthwaite impossibility theorem~\cite{gibbard1973manipulation,satterthwaite1975strategy} poses a fundamental challenge in the design of resource allocation mechanisms.
It asserts that when there are three or more alternative allocations, it is impossible to design a strategy-proof mechanism that is non-dictatorial when the domain of preferences is unrestricted.
One avenue to escape this impossibility is in the use of money, which imposes structure on the preferences of the users by measuring them against the objective monetary yardstick.
The problem of designing monetary mechanisms is well studied, with positive results including the Vickrey-Clarke-Groves (VCG) mechanism~\cite{vickrey1961counterspeculation,clarke1971multipart,groves1973incentives}, a general mechanism that is well known to be strategy-proof and lead to efficient allocations.
However, the use of money is deemed as unfair or unethical in many application domains.
Examples include the allocation of donated kidneys to transplant patients~\cite{roth2004kidney} \rev{and} the allocation of college applicants to colleges~\cite{gale1962college}.
This motivates the design of \emph{mechanisms without money}~\cite{schummer2007mechanism}, which is in general more difficult than monetary mechanisms due to the lack of a general instrument that can be used to align incentives.
Some successes include the cases when preferences are single-peaked~\cite{moulin1980strategy}, when each user has one item to trade~\cite{shapley1974cores}, and when matching users pairwise in a bipartite graph~\cite{gale1962college}, which all leverage specific structures in the preferences of the agents that are difficult to generalize.
\rev{When users must express preferences over many alternatives, a general approach is the \emph{pseudo-market} pioneered by~\cite{hylland1979efficient} and famously adopted in the context of allocating course seats in business schools~\cite{sonmez2010course}. In a pseudo-market, users are given a finite budget of tokens to distribute over the alternatives, whose prices (in tokens) are set/discovered to clear the market (i.e., allocate the correct amount of resources to the correct amount of users).
However, pseudo-market only promise to be pareto-efficient and are also not strategy-proof (although strategizing becomes difficult when there are many users and alternatives)~\cite{hylland1979efficient}.
It is noteworthy to mention that in our running intersection allocation example, any allocation of the right of way is pareto-efficient (as long as both vehicles do not stand still and wait).}

The aforementioned difficulty stems \rev{from} the fact that the classical mechanism design problem is concerned with a \emph{static} or \emph{one-shot} allocation of goods.
On the other hand, when the allocation is \emph{dynamic} or \emph{repeating over time}, new opportunities for the design of strategy-proof and efficient mechanisms present themselves.
On a conceptual level, just as how money can be used to incentivize truthful behavior (or punish non-truthfulness), a similar incentive can be achieved through a promise of future service (or denial thereof).
Despite of this intuitive notion, the role that repetition could play in mechanism design has only been recently noticed, and the literature on mechanism design for dynamic resource allocation is sparse.
A few recent works \rev{build upon the notion of ``promised utilities'' pioneered by~\cite{spear1987repeated} in the context of contract design in repeated relationships.
This includes~\cite{guo2020dynamic}, which develops an incentive compatible mechanism for the case when a single principal repeatedly allocates a single good to a single user, and~\cite{balseiro2019multiagent} which extends this approach to the case when the single good is to be repeatedly allocated to one of the same multiple users.
The working principle of these works is to find a set of future utilities that the principal promises to the user(s) as a function of their reported immediate utility, in a manner that incentivizes truthful reporting, and while ensuring that the principal can recursively keep these future promises.
In~\cite{guo2020dynamic}, the promised utilities are given the intuitive interpretation of being the number of times the user can claim the good in a row ``with no questions asked'', while in~\cite{balseiro2019multiagent}, no intuitive meaning to how the principal can keep its promises is given.
Similar to these works, karma is a device to encode future promises to the users; the higher a user's karma the more favorable its future position will be.
But with karma, these promises need not be made explicit, and a single principle need not be held accountable for them.
Instead, the future value of karma arises in a decentralized and natural manner as the users strategically ration its use to cover their future needs.
}
% include~\cite{balseiro2019multiagent}, which proposes a mechanism based on ``promised utilities'' that are not intuitive to implement in practice\rev{.}

\rev{In other works, }\cite{sonmez2020incentivized} leverage the high likelihood of kidney transplant failures to incentivize participation in the kidney exchange by providing a priority for re-transplant to participants\rev{.}
\cite{kim2021organ} similarly incentivize participation in the kidney exchange by issuing participants a voucher for re-transplant that is also redeemable by their offspring.
We consider our karma mechanisms to be complimentary to these works, offering a simple and intuitive alternative that has the potential to scale to large systems and across multiple applications.

\subsubsection{Artificial currency mechanisms}

A special class of mechanisms without money, which are perhaps the most related to our karma mechanisms, are the so-called \emph{artificial currency} or \emph{scrip} mechanisms.
These mechanisms have been proposed in multiple isolated application instances since the early 2000's.
\cite{golle2001incentives} propose a ``point system'' to address the problem of \emph{free-riding} in peer-to-peer networks, where agents tend to download many more files than they upload.
%Uploads are incentivized by rewarding points which are needed to download files.
Similar works in the domain of peer-to-peer networks include~\cite{vishnumurthy2003karma}, who specifically call their point system ``karma'' and focus on its cryptographic implementation rather than the design of the mechanism itself\rev{; and}~\cite{friedman2006efficiency}, who do incorporate elements of mechanism design but focus solely on the choice of a single parameter, which is the total amount of karma in their specific model.
In the domain of transportation, \cite{salazar2021urgency} recently demonstrated how an artificial currency (also called ``karma'') can be utilized instead of monetary tolls to achieve optimal routing in a two arc road network.
In the domain of course allocation, the use of artificial currency has been employed in business schools as a means for students to bid over courses with limited seats and to allocate those seats~\cite{sonmez2010course,budish2011combinatorial,budish2012multi}. The course allocation problem is however fundamentally different \rev{from} the \rev{considered} class of problems we since it is essentially a one-shot allocation.
A more related work is the ``choice system'' for the allocation of food donations to food banks in the United States~\cite{prendergast2016allocation}. There, food banks are allocated ``shares'' which they use to bid on the food donations they need.
To the extent of our knowledge, this is the most concrete example of a real-life implementation of a karma-like mechanism\rev{.
I}t is considered to be a major success as evidenced by the active participation of food banks in the system as well as the unprecedented fluidity of food donations it resulted in.

All of the above works share in common the need for a non-monetary medium of exchange to coordinate the use of shared resources.
However, there is an apparent lack of unity in the approaches taken, with most works proposing a \rev{problem-tailored,} heuristic mechanism with little rigorous justification \rev{and scope for generalization}.
In~\cite{prendergast2016allocation}, a model is presented that makes many simplifying assumptions on the strategic behavior of the users\rev{.
The model does not truly capture} the dynamic nature of the optimization \rev{problem of} the users, wh\rev{o} must ration their use of shares now to secure their future needs.
In~\cite{salazar2021urgency}, a game-theoretic equilibrium is considered in which individual users solve a finite horizon dynamic optimization, but importantly, the amount of karma saved at the end of the horizon is treated as an exogenous parameter.
In contrast, our approach rigorously models the dynamic strategic problem faced by the users of the karma mechanism, and our stationary Nash equilibria provide unprecedented insights into the users' microscopic behavior\rev{. 
W}e believe \rev{that this will} be fundamental to our understanding of these mechanisms and \rev{will} serve as an important tool for the mechanism design.

% \subsubsection{Fair resource allocation}

% A plethora of definitions of what constitutes a fair resource allocation, as well as how to measure the fairness of an allocation, exists in the extensive literature on the subject.
% When the resource is divisible, a variety of fairness definitions and associated social welfare functions arise axiomatically, which primarily depend on the allowed degree of comparability of the utilities of different users~\cite{roberts1980interpersonal}.
% These include \emph{utilitarianism}~\cite{d1977equity}, \emph{proportional fairness} or \emph{Nash welfare}~\cite{nash1950bargaining,kaneko1979nash,kelly1997charging}, \emph{lexi-max-min fairness}~\cite{arrow1977extended}, and \emph{$\alpha$-fairness}~\cite{roberts1980interpersonal,mo2000fair}, where the latter forms a family of fairness definitions parametrized by a parameter $\alpha$ and includes the former three as special cases.
% Another notion of fairness that does not require any degree of interpersonal comparability is \emph{envy-freeness}~\cite{foley1966resource}, which defines a fair resource allocation as one where no user envies the resource share of another.
% When the resource is indivisible, a common approach to fairness is based on the notion of \emph{priority}~\cite{young1995equity}, where priority to access the resource is given to users based on a scoring scheme that is publicly deemed as fair.
% On the other hand, measures to quantify how fair a resource allocation is include the well-known Gini coefficient~\cite{gini1912variabilita}, Jain's index~\cite{jain1984quantitative}, Theil's index~\cite{theil1967economics}, the average utility of the least fortunate percentage of the population~\cite{chapman2018fair}, and many others (see~\cite[Chapter~2]{sen1997economic} for a survey).
% The defining feature of all of the above works is that they are, once again, concerned with static or one-shot allocations.
% While in principle a repeated resource allocation can be viewed as a static allocation of the resource duplicated over time (see, e.g., \cite{luss1999equitable}), this approach is impractical when the repetition is frequent, and when we must reason about the fairness of the resource allocation \emph{before} future instances of it occur.

% To this end, a tangential stream of literature studies the fairness of \emph{randomized resource allocations}, where the popular distinction between fairness \emph{ex-ante} and \emph{ex-post} is made~\cite{cappelen2013just}.
% A randomized resource allocation is ex-ante fair if the chances or odds of randomization are fair, and it is ex-post fair if in addition all possible realizations of the randomization are fair.
% In our context, a repeated coin toss is ex-ante fair, but could lead to vastly unfair allocations ex-post after multiple repetitions.
% It is clear from this example that a fairness definition is needed which incorporates memory from past allocations, a notion that, to the extent of our knowledge, has not been formalized before.
% We take a step in this direction with our definition of \emph{corrective fairness}, which quantifies the likelihood of receiving the resource given the number of consecutive previous denials.

% \subsubsection{Dynamic population games}

% \todo[inline]{The following copied from other paper, need to clean up (but has the intended references)}

% In a wide variety of fields, ranging from economics to biology and engineering, population games have become a standard model of strategic interplay in large societies of rational agents, see~\cite{sandholm2010population}. When the number of agents is large, the identity and behavior of the individual agent is not significant to the whole, and one can instead focus on groups of agents behaving in different ways. This enables the formulation of large-scale game-theoretic models that would otherwise be intractable.

% In the classical population game, this grouping is based on static \emph{types} of the agents which encode their incentives and behavior.
% It is natural, however, to expect that the agents alter their behavior based also on time-varying \emph{states}, which are in turn affected by their individual decisions and the decisions of others.
% For example, an agent behaves differently under the threat of an epidemic based on whether it is infected or not, and that very behavior affects its future infection state, see~\cite{elokda2021adynamic}.
% On the other hand, game-theoretic settings involving state dynamics have classically been formulated as the stochastic games of~\cite{shapley1953stochastic}, which are known to become intractable for more than a few agents and are not suitable for large populations.

% Extensions of stochastic games to large populations have been previously proposed under different taxonomies, including the \emph{mean field games} of~\cite{huang2006large} and~\cite{lasry2007mean}, the \emph{anonymous sequential games} of~\cite{jovanovic1988anonymous}, and variants thereof (see~\cite{elokda2021dynamic} for a comprehensive review).
% Most of these works give theoretical guarantees on the existence of an equilibrium notion, often referred to as the \emph{stationary equilibrium}, but give little clues on how they are applicable in practice.
% In sharp contrast to the literature, our treatment aims at offering the same kind of versatility that the static population games of~\cite{sandholm2010population} have seen.
% In our main contribution, Theorem~\ref{th:Reduction}, we show that dynamic population games can be reduced to a suitably defined static population game whose classical Nash equilibria coincide with the stationary equilibria in our dynamic setting.
% As a consequence, we are able to formulate a macroscopic model of the coupled evolution of both the agents' actions and states, by adapting classical evolutionary dynamics to dynamic population games.
% This evolutionary model can act both as an equilibrium seeking algorithm, as well as a model of the agents' behavior off the equilibrium.
% We demonstrate the utility of our approach on two application examples, the former being an intricate dynamic auction-like mechanism with finite budgets, called a \emph{karma mechanism}, and the latter being a model of strategic behavior under the threat of an epidemic.

\subsection{Organization of the paper}

Section~\ref{sec:karma} introduces the setting of dynamic resource allocation and the concept of karma mechanisms.
In Section~\ref{sec:Model} we model karma mechanisms as dynamic population games, and show that a stationary Nash equilibrium is guaranteed to exist.
The model is utilized in a numerical investigation of karma mechanisms in Section~\ref{sec:NumericalAnalysis}, where we provide insights on the emerging strategic behavior as well as the consequences of the karma mechanism design on the achieved efficiency and fairness of the resource allocation.