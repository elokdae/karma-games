% Wrtie stationary distribution to csv file
function n_k = write_d_csv(d_up_mu_alpha_u_k, k_max, param, se_param, i_alpha_comp, fileprefix)
    % Create normalized version of d which normalizes the karma distribution to 1
    % per (mu-alpha-u) tuple
    d_up_mu_alpha_u_k_norm = d_up_mu_alpha_u_k ./ sum(d_up_mu_alpha_u_k, 4);

    % Truncate the end of the distribution
    n_k = k_max + 1;
    d_up_mu_alpha_u_k(:,:,:,n_k) = sum(d_up_mu_alpha_u_k(:,:,:,n_k:end), 4);
    d_up_mu_alpha_u_k(:,:,n_k+1) = d_up_mu_alpha_u_k(:,:,n_k);
    d_up_mu_alpha_u_k_norm(:,:,:,n_k) = sum(d_up_mu_alpha_u_k_norm(:,:,:,n_k:end), 4);
    d_up_mu_alpha_u_k_norm(:,:,n_k+1) = d_up_mu_alpha_u_k_norm(:,:,n_k);
    
    % Header
    header = ["mu", "alpha", "i-u", "u", "k", "k2", "P(k)", "norm-P(k)"];
    filename = [fileprefix, '-dist.csv'];
    fout = fopen(filename, 'w');
    for i = 1 : length(header) - 1
        fprintf(fout, '%s,', header(i));
    end
    fprintf(fout, '%s\n', header(end));
    fclose(fout);

    % Data
    for i_mu = 1 : param.n_mu
        MU = i_mu * ones(n_k + 1, 1);
        for i_alpha = 1 : param.n_alpha
            if param.n_alpha == 1
                alpha = param.Alpha(i_alpha_comp);
            else
                alpha = param.Alpha(i_alpha);
            end
            for i_u = 1 : param.n_u
                data = [MU, alpha * ones(n_k + 1, 1), i_u * ones(n_k + 1, 1), param.U(i_u) * ones(n_k + 1, 1), se_param.K(1:n_k+1), se_param.K(1:n_k+1) - 0.5, squeeze(d_up_mu_alpha_u_k(i_mu,i_alpha,i_u,1:n_k+1)), squeeze(d_up_mu_alpha_u_k_norm(i_mu,i_alpha,i_u,1:n_k+1))];
                dlmwrite(filename, data, '-append');
            end
        end
    end
end