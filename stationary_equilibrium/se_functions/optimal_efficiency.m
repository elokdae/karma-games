function e_opt = optimal_efficiency(param)
%OPTIMAL_EFFICIENCY Computes the optimal efficiency

prob_up_u = einsum('ij,ik->j', param.prob_down_mu_up_u, param.w_up_mu);

e_opt = 0;
for i_u = 1 : param.n_u
    e_opt = e_opt - 0.5 * prob_up_u(i_u)^2 * param.U(i_u);
    for i_uj = i_u + 1 : param.n_u
        e_opt = e_opt - prob_up_u(i_u) * prob_up_u(i_uj) * param.U(i_u);
    end
end
end

