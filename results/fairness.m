clear;
close all;
clc;

%% Screen size used to place plots
screensize = get(groot, 'ScreenSize');
screenwidth = screensize(3);
screenheight = screensize(4);
default_width = screenwidth / 2;
default_height = screenheight / 2;
fg = 1;

%% Some parameters
k_bar_vec = 10;
alpha_comp_vec = [0.05 : 0.05 : 0.95, 0.96 : 0.01 : 1.00];

n_k_bar = length(k_bar_vec);
n_alpha_comp = length(alpha_comp_vec);

file_str = 'results/n_a_200_T_100000_U_1_10_phi1_0.50_0.50_0.50_pay_0/k_bar_';

fairness_horizon_computed = true;
n_fairness_horizon_to_plot = 3;

sw_computed = false;

compare = false;
if compare
    compare_file_str = 'results/n_a_200_T_100000_U_1_10_phi1_0.80_0.20_0.40_0.60_pay_0/k_bar_';
end

% Flag to save plots
save_plots = true;

%% Unfariness arrays
se_UF = nan(n_k_bar, n_alpha_comp);
if sw_computed
    sw_UF = zeros(n_k_bar, 1);
end
if compare
    compare_se_UF = nan(n_k_bar, n_alpha_comp);
end

for i_k_bar = 1 : n_k_bar
    % SE unfairness
    for i_alpha_comp = 1 : n_alpha_comp
        alpha = alpha_comp_vec(i_alpha_comp);
        if alpha > 0.99 && alpha < 1
            file = [file_str, num2str(k_bar_vec(i_k_bar), '%02d'), '_alpha_', num2str(alpha, '%.3f'), '.mat'];
        else
            file = [file_str, num2str(k_bar_vec(i_k_bar), '%02d'), '_alpha_', num2str(alpha, '%.2f'), '.mat'];
        end
        if ~exist(file, 'file')
            file = [file_str, num2str(k_bar_vec(i_k_bar), '%02d'), '.mat'];
        end
        load(file, 'UF_se', 'param');
        i_alpha_sim = find(abs(param.Alpha - alpha) <= eps);
        if ~isempty(i_alpha_sim)
            se_UF(i_k_bar,i_alpha_comp) = UF_se{i_alpha_sim}(end);
        end
    end
    % SW unfairness
    if sw_computed
        load(file, 'UF_sw');
        if exist('UF_sw', 'var')
            sw_UF(i_k_bar) = UF_sw(end);
            clear UF_sw
        else
            sw_UF(i_k_bar) = nan;
        end
    end
    % Compare inefficiency
    if compare
        for i_alpha_comp = 1 : n_alpha_comp
            alpha = alpha_comp_vec(i_alpha_comp);
            if alpha > 0.99 && alpha < 1
                compare_file = [compare_file_str, num2str(k_bar_vec(i_k_bar), '%02d'), '_alpha_', num2str(alpha, '%.3f'), '.mat'];
            else
                compare_file = [compare_file_str, num2str(k_bar_vec(i_k_bar), '%02d'), '_alpha_', num2str(alpha, '%.2f'), '.mat'];
            end
            if ~exist(compare_file, 'file')
                compare_file = [compare_file_str, num2str(k_bar_vec(i_k_bar), '%02d'), '.mat'];
            end
            load(compare_file, 'UF_se', 'param');
            i_alpha_sim = find(abs(param.Alpha - alpha) <= eps);
            if ~isempty(i_alpha_sim)
                compare_se_UF(i_k_bar,i_alpha_comp) = UF_se{i_alpha_sim}(end);
            end
        end
    end
    % Centralized urgency efficiency (get once for comparison)
    if i_k_bar == 1
        load(file, 'UF_rand', 'UF_u', 'UF_a');
        rand_UF = UF_rand(end);
        u_UF = UF_u(end);
        a_UF = UF_a(end);
    end
end

if fairness_horizon_computed
    fair_hor_file = [file_str, 'centralized_policies.mat'];
    load(fair_hor_file, 'UF_fair_hor_u_a', 'param');
    fair_hor_UF = zeros(param.n_fairness_horizon, 1);
    for i_fair_hor = 1 : param.n_fairness_horizon
        fair_hor_UF(i_fair_hor) = UF_fair_hor_u_a{i_fair_hor}(end);
    end
end

if save_plots
    if ~exist('results/fairness', 'dir')
        mkdir('results/fairness');
    end
end

%% Plot all
figure(fg);
fg = fg + 1;
fig = gcf;
fig.Position = [0, 0, screenwidth, screenheight];
plot(k_bar_vec, repmat(-u_UF, 1, n_k_bar), 'm-', 'LineWidth', 4);
lgd_text = "no-memory";
hold on;
plot(k_bar_vec, repmat(-a_UF, 1, n_k_bar), 'c-', 'LineWidth', 4);
lgd_text = [lgd_text; "full-memory"];
plot(k_bar_vec, repmat(-rand_UF, 1, n_k_bar), 'r-', 'LineWidth', 4);
lgd_text = [lgd_text; "baseline-random"];
if fairness_horizon_computed
    for i_fair_hor = 1 : min([n_fairness_horizon_to_plot, param.n_fairness_horizon])
        plot(k_bar_vec, repmat(-fair_hor_UF(i_fair_hor), 1, n_k_bar), '--', 'LineWidth', 1);
        lgd_text = [lgd_text; strcat(num2str(param.fairness_horizon(i_fair_hor)), "-memory")];
    end
end
if sw_computed
    plot(k_bar_vec, -sw_UF, 'g-', 'LineWidth', 3);
    lgd_text = [lgd_text; "social-welfare"];
end
for i_alpha_comp = 1 : n_alpha_comp
    plot(k_bar_vec, -se_UF(:,i_alpha_comp), '-x', 'LineWidth', 2);
    lgd_text = [lgd_text; strcat("$\alpha$ = ", num2str(alpha_comp_vec(i_alpha_comp), '%.2f'))];
end
if compare
    for i_alpha_comp = 1 : n_alpha_comp
        plot(k_bar_vec, -se_UF(:,i_alpha_comp), '-x', 'LineWidth', 2);
        lgd_text = [lgd_text; strcat("$\alpha$ = ", num2str(alpha_comp_vec(i_alpha_comp), '%.2f'), "-2")];
    end
end
axis tight;
axes = gca;
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Fairness of different policies';
axes.Title.FontSize = 16;
axes.XAxis.FontSize = 10;
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = 'Average karma';
axes.XLabel.FontSize = 14;
axes.YLabel.Interpreter = 'latex';
axes.YLabel.String = 'Fairness';
axes.YLabel.FontSize = 14;
lgd = legend(lgd_text);
lgd.FontSize = 12;
lgd.Interpreter = 'latex';
lgd.Location = 'bestoutside';
yl = ylim;

if save_plots
    saveas(gcf, 'results/fairness/all.png');
end

%% Plot slices through k_bar
figure(fg);
fg = fg + 1;
fig = gcf;
fig.Position = [0, 0, screenwidth, screenheight];
if n_k_bar == 1
    num_cols = 1;
    num_rows = 1;
else
    num_cols = round(sqrt(screenwidth / screenheight * (n_k_bar + 1)));
    num_rows = ceil((n_k_bar + 1) / num_cols);
end
for i_k_bar = 1 : n_k_bar
    subplot(num_rows,num_cols,i_k_bar);
    semilogx(alpha_comp_vec, repmat(-u_UF, 1, n_alpha_comp), 'm-', 'LineWidth', 4);
    hold on;
    semilogx(alpha_comp_vec, repmat(-a_UF, 1, n_alpha_comp), 'c-', 'LineWidth', 4);
    semilogx(alpha_comp_vec, repmat(-rand_UF, 1, n_alpha_comp), 'r-', 'LineWidth', 4);
    colorOrder = get(gca, 'ColorOrder');
    if fairness_horizon_computed
        for i_fair_hor = 1 : min([n_fairness_horizon_to_plot, param.n_fairness_horizon])
            semilogx(alpha_comp_vec, repmat(-fair_hor_UF(i_fair_hor), 1, n_alpha_comp), '--', 'LineWidth', 1, 'Color', colorOrder(i_fair_hor+1,:))
        end
    end
    if sw_computed
        semilogx(alpha_comp_vec, repmat(-sw_UF(i_k_bar), 1, n_alpha_comp), 'g-', 'LineWidth', 3);
    end
    semilogx(alpha_comp_vec, -se_UF(i_k_bar,:), '-x', 'LineWidth', 2, 'Color', colorOrder(1,:));
    if compare
        semilogx(alpha_comp_vec, -compare_se_UF(i_k_bar,:), '-o', 'LineWidth', 2, 'Color', colorOrder(2,:));
    end
    axis tight;
    ylim(yl);
    axes = gca;
    axes.Title.Interpreter = 'latex';
    axes.Title.String = ['$k_{bar}$ = ', num2str(k_bar_vec(i_k_bar), '%02d')];
    axes.Title.FontSize = 14;
    axes.XAxis.FontSize = 10;
    axes.YAxis.FontSize = 10;
    axes.XLabel.Interpreter = 'latex';
    axes.XLabel.String = 'Future discount factor';
    axes.XLabel.FontSize = 12;
    axes.YLabel.Interpreter = 'latex';
    axes.YLabel.String = 'Fairness';
    axes.YLabel.FontSize = 12;
end
if n_k_bar == 1
    lgd_text = "no-memory";
    lgd_text = [lgd_text; "full-memory"];
    lgd_text = [lgd_text; "baseline-random"];
    if fairness_horizon_computed
        for i_fair_hor = 1 : min([n_fairness_horizon_to_plot, param.n_fairness_horizon])
            lgd_text = [lgd_text; strcat(num2str(param.fairness_horizon(i_fair_hor)), "-memory")];
        end
    end
    if sw_computed
        lgd_text = [lgd_text; "social-welfare"];
    end
    lgd_text = [lgd_text; "stat-equilibrium"];
    if compare
        lgd_text = [lgd_text; "stat-equilibrium-2"];
    end
else
    subplot(num_rows,num_cols,n_k_bar+1);
    plot(nan, nan, 'm-', 'LineWidth', 4);
    lgd_text = "no-memory";
    hold on;
    plot(nan, nan, 'c-', 'LineWidth', 4);
    lgd_text = [lgd_text; "full-memory"];
    plot(nan, nan, 'r-', 'LineWidth', 4);
    lgd_text = [lgd_text; "baseline-random"];
    colorOrder = get(gca, 'ColorOrder');
    if fairness_horizon_computed
        for i_fair_hor = 1 : min([n_fairness_horizon_to_plot, param.n_fairness_horizon])
            plot(nan, nan, '--', 'LineWidth', 1, 'Color', colorOrder(i_fair_hor+1,:));
            lgd_text = [lgd_text; strcat(num2str(param.fairness_horizon(i_fair_hor)), "-memory")];
        end
    end
    if sw_computed
        plot(nan, nan, 'g-', 'LineWidth', 3);
        lgd_text = [lgd_text; "social-welfare"];
    end
    plot(nan, nan, '-x', 'LineWidth', 2, 'Color', colorOrder(1,:));
    lgd_text = [lgd_text; "stat-equilibrium"];
    if compare
        plot(nan, nan, '-o', 'LineWidth', 2, 'Color', colorOrder(2,:));
        lgd_text = [lgd_text; "stat-equilibrium-2"];
    end
    axis off;
end
lgd = legend(lgd_text);
lgd.FontSize = 12;
lgd.Interpreter = 'latex';
lgd.Location = 'bestoutside';
title = sgtitle('Fairness as a function of $\alpha$ for different average karmas');
title.Interpreter = 'latex';
title.FontSize = 16;

if save_plots
    saveas(gcf, 'results/fairness/k_bar_slices.png');
end

%% Plot slices through alpha
figure(fg);
fg = fg + 1;
fig = gcf;
fig.Position = [0, 0, screenwidth, screenheight];
if n_alpha_comp == 1
    num_cols = 1;
    num_rows = 1;
else
    num_cols = round(sqrt(screenwidth / screenheight * (n_alpha_comp + 1)));
    num_rows = ceil((n_alpha_comp + 1) / num_cols);
end
for i_alpha_comp = 1 : n_alpha_comp
    subplot(num_rows,num_cols,i_alpha_comp);
    plot(k_bar_vec, repmat(-u_UF, 1, n_k_bar), 'm-', 'LineWidth', 4);
    hold on;
    plot(k_bar_vec, repmat(-a_UF, 1, n_k_bar), 'c-', 'LineWidth', 4);
    plot(k_bar_vec, repmat(-rand_UF, 1, n_k_bar), 'r-', 'LineWidth', 4);
    colorOrder = get(gca, 'ColorOrder');
    if fairness_horizon_computed
        for i_fair_hor = 1 : min([n_fairness_horizon_to_plot, param.n_fairness_horizon])
            plot(k_bar_vec, repmat(-fair_hor_UF(i_fair_hor), 1, n_k_bar), '--', 'LineWidth', 1, 'Color', colorOrder(i_fair_hor+1,:));
        end
    end
    if sw_computed
        plot(k_bar_vec, -sw_UF, 'g-', 'LineWidth', 3);
    end
    plot(k_bar_vec, -se_UF(:,i_alpha_comp), '-x', 'LineWidth', 2, 'Color', colorOrder(1,:));
    axis tight;
    ylim(yl);
    axes = gca;
    axes.Title.Interpreter = 'latex';
    axes.Title.String = ['$\alpha$ = ', num2str(alpha_comp_vec(i_alpha_comp), '%0.2f')];
    axes.Title.FontSize = 14;
    axes.XAxis.FontSize = 10;
    axes.YAxis.FontSize = 10;
    axes.XLabel.Interpreter = 'latex';
    axes.XLabel.String = 'Average karma';
    axes.XLabel.FontSize = 12;
    axes.YLabel.Interpreter = 'latex';
    axes.YLabel.String = 'Fairness';
    axes.YLabel.FontSize = 12;
end
if n_alpha_comp == 1
    lgd_text = "no-memory";
    lgd_text = [lgd_text; "full-memory"];
    lgd_text = [lgd_text; "baseline-random"];
    if fairness_horizon_computed
        for i_fair_hor = 1 : min([n_fairness_horizon_to_plot, param.n_fairness_horizon])
            lgd_text = [lgd_text; strcat(num2str(param.fairness_horizon(i_fair_hor)), "-memory")];
        end
    end
    if sw_computed
        lgd_text = [lgd_text; "social-welfare"];
    end
    lgd_text = [lgd_text; "stat-equilibrium"];
else
    subplot(num_rows,num_cols,n_alpha_comp+1);
    plot(nan, nan, 'm-', 'LineWidth', 4);
    lgd_text = "no-memory";
    hold on;
    plot(nan, nan, 'c-', 'LineWidth', 4);
    lgd_text = [lgd_text; "full-memory"];
    plot(nan, nan, 'r-', 'LineWidth', 4);
    lgd_text = [lgd_text; "baseline-random"];
    colorOrder = get(gca, 'ColorOrder');
    if fairness_horizon_computed
        for i_fair_hor = 1 : min([n_fairness_horizon_to_plot, param.n_fairness_horizon])
            plot(nan, nan, '--', 'LineWidth', 1, 'Color', colorOrder(i_fair_hor+1,:));
            lgd_text = [lgd_text; strcat(num2str(param.fairness_horizon(i_fair_hor)), "-memory")];
        end
    end
    if sw_computed
        plot(nan, nan, 'g-', 'LineWidth', 3);
        lgd_text = [lgd_text; "social-welfare"];
    end
    plot(nan, nan, '-x', 'LineWidth', 2, 'Color', colorOrder(1,:));
    lgd_text = [lgd_text; "stat-equilibrium"];
    axis off;
end
lgd = legend(lgd_text);
lgd.FontSize = 12;
lgd.Interpreter = 'latex';
lgd.Location = 'bestoutside';
title = sgtitle('Fairness as a function of $k_{bar}$ for different future discount factors');
title.Interpreter = 'latex';
title.FontSize = 16;

if save_plots
    saveas(gcf, 'results/fairness/alpha_slices.png');
end