function param = load_parameters()

%% Setting parameters
% Average amount of karma
param.k_bar = 10;
% Set to row vector to perform multiple SW computations in one shot
% param.k_bar = 11 : 12;

% Number of urgency types
param.n_mu = 1;

% Future awareness types
param.Alpha = 0.98;
% Set to row vector to simulate multiple alpha values or perform multiple
% SE computations in one shot
% param.Alpha = [1 : -0.01 : 0.96, 0.95 : -0.05 : 0.15, 0.00];
% param.Alpha = [1 : -0.01 : 0.96, 0.95 : -0.05 : 0.00];
% param.Alpha = [0.1, 0.05];
% param.Alpha = [1, 0.99, 0.97, 0.95 : -0.05 : 0.10];
% param.Alpha = [0.10 : 0.05 : 0.95, 0.97, 0.99, 1];
% param.Alpha = 1.0;
% Set to column vector for multiple future awareness types
% param.Alpha = [0.7; 0.99];
valid_Alpha = (size(param.Alpha, 1) == 1 || size(param.Alpha, 2) == 1) ...
    && min(param.Alpha) >= 0 && max(param.Alpha) <= 1;
assert(valid_Alpha, 'Invalid future awareness types');

% Number of future awareness types
param.n_alpha = size(param.Alpha, 1);

% Number of future awareness values for simulation/computation
param.n_alpha_comp = size(param.Alpha, 2);

% Type distribution
% We only consider independant urgency and future awareness type
% distributions for now
% Urgency type distribution
param.w_up_mu = 1 / param.n_mu * ones(param.n_mu, 1);
% param.w_up_mu = [0.8; 0.2];
valid_w = length(param.w_up_mu) == param.n_mu ...
    && min(param.w_up_mu) >= 0 && sum(param.w_up_mu) == 1;
assert(valid_w, 'Invalid urgency type distribution');
% Future awareness type distribution
param.z_up_alpha = 1 / param.n_alpha * ones(param.n_alpha, 1);
valid_z = length(param.z_up_alpha) == param.n_alpha ...
    && min(param.z_up_alpha) >= 0 && sum(param.z_up_alpha) == 1;
assert(valid_z, 'Invalid future awareness type distribution');
% Joint type distribution
param.g_up_mu_alpha = param.w_up_mu * param.z_up_alpha.';

% Sorted set of urgency values
% param.U = [1; 2; 3; 6; 11];
% param.U = [1; 2; 10];
param.U = [1; 1; 10];
% param.U = [1; 10; 20];
% param.U = [1; 10];
% param.U = 1;
param.U = sort(param.U);

% Number of urgency values
param.n_u = length(param.U);

% Index set of urgency values
param.i_U = 1 : param.n_u;

% Urgency Markov chain
param.phi_down_mu_u_up_un = zeros(param.n_mu, param.n_u, param.n_u);
% param.phi_down_mu_u_up_un(1,:,:) = [0.9, 0.1;
%                                     0.9, 0.1];
% param.phi_down_mu_u_up_un(1,:,:) = [0.9, 0.0, 0.0, 0.0, 0.1;
%                                     0.9, 0.0, 0.0, 0.0, 0.1;
%                                     0.9, 0.0, 0.0, 0.0, 0.1;
%                                     0.9, 0.0, 0.0, 0.0, 0.1;
%                                     0.9, 0.0, 0.0, 0.0, 0.1];
param.phi_down_mu_u_up_un(1,:,:) = [0.95, 0.05, 0;
                                    0.0, 0.5, 0.5;
                                    0.95, 0.05, 0];
% param.phi_down_mu_u_up_un(1,:,:) = [0.5, 0.5, 0.0;
%                                     0.5, 0.5, 0.0;
%                                     0.5, 0.5, 0.0];
% param.phi_down_mu_u_up_un(1,:,:) = [0.5, 0.5;
%                                     0.5, 0.5];
% param.phi_down_mu_u_up_un(1,:,:) = [0.75, 0.25;
%                                     0.75, 0.25];
% param.phi_down_mu_u_up_un(1,:,:) = [0.8, 0.2;
%                                     0.4, 0.6];
% param.phi_down_mu_u_up_un(1,1,1) = 1;
if param.n_mu > 1
%     param.phi_down_mu_u_up_un(2,:,:) = [0.8, 0.0, 0.0, 0.2, 0.0;
%                                         0.8, 0.0, 0.0, 0.2, 0.0;
%                                         0.8, 0.0, 0.0, 0.2, 0.0;
%                                         0.8, 0.0, 0.0, 0.2, 0.0;
%                                         0.8, 0.0, 0.0, 0.2, 0.0];
%     param.phi_down_mu_u_up_un(3,:,:) = [0.5, 0.0, 0.5, 0.0, 0.0;
%                                         0.5, 0.0, 0.5, 0.0, 0.0;
%                                         0.5, 0.0, 0.5, 0.0, 0.0;
%                                         0.5, 0.0, 0.5, 0.0, 0.0;
%                                         0.5, 0.0, 0.5, 0.0, 0.0];
%     param.phi_down_mu_u_up_un(4,:,:) = [0.0, 1.0, 0.0, 0.0, 0.0;
%                                         0.0, 1.0, 0.0, 0.0, 0.0;
%                                         0.0, 1.0, 0.0, 0.0, 0.0;
%                                         0.0, 1.0, 0.0, 0.0, 0.0;
%                                         0.0, 1.0, 0.0, 0.0, 0.0];
%     param.phi_down_mu_u_up_un(2,:,:) = [0.1, 0.9, 0.0;
%                                         0.1, 0.9, 0.0;
%                                         0.1, 0.9, 0.0];
% param.phi_down_mu_u_up_un(2,:,:) = [0.5, 0.0, 0.5;
%                                     0.5, 0.0, 0.5;
%                                     0.5, 0.0, 0.5];
    param.phi_down_mu_u_up_un(2,:,:) = [1.0, 0.0, 0.0;
                                        1.0, 0.0, 0.0;
                                        1.0, 0.0, 0.0];
end
valid_phi = true;
for i_mu = 1 : param.n_mu
    for i_u = 1 : param.n_u
        valid_phi = valid_phi ...
            && min(param.phi_down_mu_u_up_un(i_mu,i_u,:)) >= 0 ...
            && sum(param.phi_down_mu_u_up_un(i_mu,i_u,:)) == 1;
        if ~valid_phi
            break;
        end
    end
    if ~valid_phi
        break;
    end
end
assert(valid_phi, 'Invalid urgency Markov chain');

% Stationary distribution of urgency Markov chain
% Also detect if urgency is i.i.d, in which case we can simply sample from
% stationary distribution. This is the case when all rows of the transition
% matrix are equal to the stationary distribution
param.prob_down_mu_up_u = zeros(param.n_mu, param.n_u);
param.u_iid = true(param.n_mu, 1);
for i_mu = 1 : param.n_mu
    param.prob_down_mu_up_u(i_mu,:) = stat_dist(squeeze(param.phi_down_mu_u_up_un(i_mu,:,:)));
    for i_u = 1 : param.n_u
        if norm(param.prob_down_mu_up_u(i_mu,:).' - squeeze(param.phi_down_mu_u_up_un(i_mu,i_u,:)), inf) >= 1e-10
            param.u_iid(i_mu) = false;
            break;
        end
    end
end

% Karma transition mechanism
% 0 => Pay bid to peer
% 1 => Pay bid to society
% 2 => Pay 2nd price to society
% 3 => Pay 1 to peer
% 4 => Pay other's bid to peer
% 5 => Pay bid to society always (winner & loser) - this tends to be
% unstable
% 10 => Loser pays bid to peer
% 11 => Loser pays bid to society
% 20 => binary bid, pay bid to peer, fixed bids as per parameter
% 21 => binary bid, pay bid to society, fixed bids as per parameter
% 22 => binary bid, pay bid to peer, 0 or bid all
% 23 => binary bid, pay bid to society, 0 or bid all
% 24 => binary bid, pay bid to peer, 0 or bid half
% 25 => binary bid, pay bid to society, 0 or bid half
% 30 => 3-way bid (gain-maintain-spend), pay bid to peer, fixed bids as per parameter
% 31 => 3-way bid (gain-maintain-spend), pay bid to society, fixed bids as per parameter
% 32 => 3-way bid (gain-maintain-spend), pay bid to society, maintain bid equals p_bar
param.transition_mechanism = 0;

% Karma taxation rate
param.tax = 0.0;

% Limited action schemes
switch param.transition_mechanism
    case {20, 21, 22, 23, 24, 25, 30, 31, 32}
        param.limited_actions = true;
        
        % Limited action fixed / initialization bids
        switch param.transition_mechanism
            case {20, 21, 22, 23, 24, 25}
                param.b_down_a = [0; 4];
            case 30
                param.b_down_a = [0; 1; 4];
            case {31, 32}
                param.b_down_a = [0; 2; 5];
        end
    
    otherwise
        param.limited_actions = false;
end


%% Simulation parameters
% Randomization seeds
% param.Seed = 0 : 9;
param.Seed = 0;

% Number of randomization seeds
param.n_seed = length(param.Seed);

% Number of agents
param.n_agents = 200;

% Total amount of karma
param.k_tot = param.k_bar * param.n_agents;

% Number of timesteps
param.T = 100000;

% Expected number of interactions per agent
% 2 agents engage in an interacion, which doubles their chances
param.exp_T_i = round(param.T / (param.n_agents / 2));

% Maximum allowed number of interactions per agent. Used in memory
% allocations
param.max_T_i = round(1.5 * param.exp_T_i);

% Matching method
% 0 => random with replacement
% 1 => random without replacement (all agents matched once before an agent
% matched again)
% 2 => random with replacement and non-uniform weights
param.matching = 1;

% Weights for non-uniform matching (param.matching = 2)
param.matching_weights = [2 * ones(param.n_agents / 2, 1); ones(param.n_agents / 2, 1)];

% Redistribution based on # of interactions?
param.interaction_weighted_redistribution = false;

% Fairness horizons
param.fairness_horizon = [1 : 10, 2.^(4 : ceil(log2(param.exp_T_i)))];

% Number of fairness horizons
param.n_fairness_horizon = length(param.fairness_horizon);

% Karma initial distribution in simulation
% 0 => All agents have average karma k_bar
% 1 => Uniform distribution over [0 : 2 * k_bar]
% 2 => Stationary distribution predicted by SE/SW computation
param.karma_initialization = 0;

% Tolerance to consider policy 'pure'
% If ratio between 2nd highest and highest probability is lower,
% policy is considered pure in highest probability bid
param.pure_policy_tol = 5e-2;

% Karma transition mechanism to use in simulation
param.sim_transition_mechanism = param.transition_mechanism;    % Same as SE computation by default
% param.sim_transition_mechanism = 1;

% Save results
param.save = true;

end