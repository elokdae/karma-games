% Write simulation results to csv file
function write_simulation_results_csv(alpha_comp, IE_se, UF_se, UF_pass_se, IE_rand, UF_rand, UF_pass_rand, IE_u, UF_u, UF_pass_u, IE_a, UF_a, UF_pass_a, IE_apass, UF_apass, UF_pass_apass, fileprefix)
    % Populate column vectors appropriately
    alpha_comp = reshape(alpha_comp, [], 1);
    n_alpha_comp = length(alpha_comp);

    % Make vectors out of rand and centralized urgency policies
    e_rand = -IE_rand * ones(n_alpha_comp, 1);
    cost_f_rand = -UF_rand * ones(n_alpha_comp, 1);
    access_f_rand = -UF_pass_rand * ones(n_alpha_comp, 1);
    e_u = -IE_u * ones(n_alpha_comp, 1);
    cost_f_u = -UF_u * ones(n_alpha_comp, 1);
    access_f_u = -UF_pass_u * ones(n_alpha_comp, 1);
    e_cf = -IE_a * ones(n_alpha_comp, 1);
    cost_f_cf = -UF_a * ones(n_alpha_comp, 1);
    access_f_cf = -UF_pass_a * ones(n_alpha_comp, 1);
    e_af = -IE_apass * ones(n_alpha_comp, 1);
    cost_f_af = -UF_apass * ones(n_alpha_comp, 1);
    access_f_af = -UF_pass_apass * ones(n_alpha_comp, 1);

    % Header
    header = ["alpha", "e", "cost-f", "access-f", "e-rand", "cost-f-rand", "access-f-rand", "e-u", "cost-f-u", "access-f-u", "e-cf", "cost-f-cf", "access-f-cf", "e-af", "cost-f-af", "access-f-af"];
    filename = [fileprefix, '-simulation-results.csv'];
    fout = fopen(filename, 'w');
    for i = 1 : length(header) - 1
        fprintf(fout, '%s,', header(i));
    end
    fprintf(fout, '%s\n', header(end));
    fclose(fout);

    % Data
    data = [alpha_comp, -IE_se, -UF_se, -UF_pass_se, e_rand, cost_f_rand, access_f_rand, e_u, cost_f_u, access_f_u, e_cf, cost_f_cf, access_f_cf, e_af, cost_f_af, access_f_af];
    dlmwrite(filename, data, '-append');
end