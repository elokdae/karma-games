% Plot SE state transitions
function plot_se_P(fg, position, colormap, se_P_down_mu_alpha_u_k_up_un_kn, param, se_param, i_alpha_comp)
    persistent se_P_plot
    if ~ishandle(fg)
        figure(fg);
        fig = gcf;
        fig.Position = position;
        se_P_plot = cell(param.n_mu, param.n_alpha);
        n_subplots = param.n_mu * param.n_alpha;
        n_cols = ceil(sqrt(n_subplots));
        n_rows = ceil(n_subplots / n_cols);
        label = cell(se_param.n_x, 1);
        for i_u = 1 : param.n_u
            base_i_u = (i_u - 1) * se_param.n_k;
%             u_str = num2str(param.U(i_u));
            u_str = ['u_', num2str(i_u)];
            for i_k = 1 : se_param.n_k
                label{base_i_u+i_k} = ['(', u_str, ',', num2str(se_param.K(i_k)), ')'];
            end
        end
        for i_mu = 1 : param.n_mu
            for i_alpha = 1 : param.n_alpha
                i_subplot = (i_mu - 1) * param.n_alpha + i_alpha;
                subplot(n_rows, n_cols, i_subplot);
                P_mat = zeros(se_param.n_x);
                for i_u = 1 : param.n_u
                    start_i_u = (i_u - 1) * se_param.n_k + 1;
                    end_i_u = i_u * se_param.n_k;
                    for i_un = 1 : param.n_u
                        start_i_un = (i_un - 1) * se_param.n_k + 1;
                        end_i_un = i_un * se_param.n_k;
                        P_mat(start_i_u:end_i_u,start_i_un:end_i_un) =...
                            squeeze(se_P_down_mu_alpha_u_k_up_un_kn(i_mu,i_alpha,i_u,:,i_un,:));
                    end
                end
                se_P_plot{i_mu,i_alpha} = heatmap(label, label, P_mat.', 'ColorbarVisible','off');
                se_P_plot{i_mu,i_alpha}.YDisplayData = flipud(se_P_plot{i_mu,i_alpha}.YDisplayData);
                alpha = param.Alpha(max([i_alpha, i_alpha_comp]));
                if alpha > 0.99 && alpha < 1
                    alpha_str = num2str(alpha, '%.3f');
                else
                    alpha_str = num2str(alpha, '%.2f');
                end
                se_P_plot{i_mu,i_alpha}.Title = ['k_{bar} = ', num2str(param.k_bar, '%02d'), ' SE state transitions for \mu = ', num2str(i_mu), ', \alpha = ', alpha_str];
                se_P_plot{i_mu,i_alpha}.XLabel = 'State now (urgency,karma)';
                se_P_plot{i_mu,i_alpha}.YLabel = 'State next (urgency,karma)';
                se_P_plot{i_mu,i_alpha}.FontName = 'Ubuntu';
                se_P_plot{i_mu,i_alpha}.FontSize = 10;
                if exist('colormap', 'var')
                    se_P_plot{i_mu,i_alpha}.Colormap = colormap;
                end
                se_P_plot{i_mu,i_alpha}.CellLabelFormat = '%.2f';
            end
        end
    else
        for i_mu = 1 : param.n_mu
            for i_alpha = 1 : param.n_alpha
                P_mat = zeros(se_param.n_x);
                for i_u = 1 : param.n_u
                    start_i_u = (i_u - 1) * se_param.n_k + 1;
                    end_i_u = i_u * se_param.n_k;
                    for i_un = 1 : param.n_u
                        start_i_un = (i_un - 1) * se_param.n_k + 1;
                        end_i_un = i_un * se_param.n_k;
                        P_mat(start_i_u:end_i_u,start_i_un:end_i_un) =...
                            squeeze(se_P_down_mu_alpha_u_k_up_un_kn(i_mu,i_alpha,i_u,:,i_un,:));
                    end
                end
                se_P_plot{i_mu,i_alpha}.ColorData = P_mat.';
                alpha = param.Alpha(max([i_alpha, i_alpha_comp]));
                if alpha > 0.99 && alpha < 1
                    alpha_str = num2str(alpha, '%.3f');
                else
                    alpha_str = num2str(alpha, '%.2f');
                end
                se_P_plot{i_mu,i_alpha}.Title = ['k_{bar} = ', num2str(param.k_bar, '%02d'), ' SE state transitions for \mu = ', num2str(i_mu), ', \alpha = ', alpha_str];
                se_P_plot{i_mu,i_alpha}.XLabel = 'State now (urgency,karma)';
            end
        end
    end
end