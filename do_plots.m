%% Screen size used to place plots
screensize = get(groot, 'ScreenSize');
% screenwidth = screensize(3);
% screenheight = screensize(4);
screenwidth = 1920;
screenheight = 1080;
default_width = screenwidth / 2;
default_height = screenheight / 2;
fg = 1;
vert_fg = 0;

%% Set the Color order
cmap = unique(lines, 'rows');
cmap = [cmap; unique(prism, 'rows')];
cmap(end,:) = [];
cmap_end = colorcube;
cmap_end(39:end,:) = [];
rng(1000);
cmap_end = cmap_end(randperm(size(cmap_end,1)),:);
cmap = [cmap; cmap_end];
cmap_parula = parula(round(1.2 * param.n_alpha_comp));
cmap_parula(param.n_alpha_comp+1:end,:) = [];
cmap_parula = flip(cmap_parula);
cmap_autumn = autumn(round(1.2 * param.n_fairness_horizon));

%% Scatter plot - Efficiency vs fairness
performance_comparison_fg = fg;
figure(performance_comparison_fg);
fg = fg + 1;
fig = gcf;
fig.WindowState = 'maximized';
set(fig, 'defaultAxesColorOrder', cmap);
hold off;
pl = plot(-IE_rand, -UF_rand,...
    'LineStyle', 'none',...
    'Marker', 'p',...
    'MarkerSize', 10);
pl.MarkerFaceColor = pl.Color;
lgd_text = "random";
hold on;
pl = plot(-IE_u, -UF_u,...
    'LineStyle', 'none',...
    'Marker', 'p',...
    'MarkerSize', 10);
pl.MarkerFaceColor = pl.Color;
lgd_text = [lgd_text, "best efficiency"];
pl = plot(-IE_a, -UF_a,...
    'LineStyle', 'none',...
    'Marker', 'p',...
    'MarkerSize', 10);
pl.MarkerFaceColor = pl.Color;
lgd_text = [lgd_text, "best fairness"];
% pl = plot(-IE_u_a, -UF_u_a,...
%     'LineStyle', 'none',...
%     'Marker', 'p',...
%     'MarkerSize', 10);
% pl.MarkerFaceColor = pl.Color;
% lgd_text = [lgd_text, "centralized urg-th-cost"];
if control.fairness_horizon_policies
    set(gca, 'ColorOrder', cmap_autumn);
    set(gca, 'ColorOrderIndex', 1);
    for i_fair_hor = 1 : param.n_fairness_horizon
        pl = plot(-IE_fair_hor_a(i_fair_hor), -UF_fair_hor_a(i_fair_hor),...
            'LineStyle', 'none',...
            'LineWidth', 1.5,...
            'Marker', '+',...
            'MarkerSize', 10);
        pl.MarkerFaceColor = pl.Color;
        lgd_text = [lgd_text, strcat("cent-cost-", int2str(param.fairness_horizon(i_fair_hor)))];
    end
    set(gca, 'ColorOrderIndex', 1);
    for i_fair_hor = 1 : param.n_fairness_horizon
        pl = plot(-IE_fair_hor_u_a(i_fair_hor), -UF_fair_hor_u_a(i_fair_hor),...
            'LineStyle', 'none',...
            'LineWidth', 1.5,...
            'Marker', '*',...
            'MarkerSize', 10);
        pl.MarkerFaceColor = pl.Color;
        lgd_text = [lgd_text, strcat("cent-urg-th-cost-", int2str(param.fairness_horizon(i_fair_hor)), ", ", num2str(ent_fair_hor_u_a(i_fair_hor), '%.2f'), "-bit")];
    end
end
if control.sw_policy
    set(gca, 'ColorOrder', cmap);
    set(gca, 'ColorOrderIndex', 1);
    pl = plot(-IE_sw, -UF_sw,...
        'LineStyle', 'none',...
        'Marker', 's',...
        'MarkerSize', 10);
    pl.MarkerFaceColor = pl.Color;
    lgd_text = [lgd_text, "social-welfare"];
end
if control.se_policies
    plot(-IE_se, -UF_se, 'LineWidth', 1.5, 'Color', [0.9290 0.6940 0.1250]);
    lgd_text = [lgd_text, "KARMA"];
    set(gca, 'ColorOrder', cmap_parula);
    set(gca, 'ColorOrderIndex', 1);
    for i_alpha_comp = 1 : param.n_alpha_comp
        pl = plot(-IE_se(i_alpha_comp), -UF_se(i_alpha_comp),...
            'LineStyle', 'none',...
            'Marker', 'o',...
            'MarkerSize', 10);
        pl.MarkerFaceColor = pl.Color;
        lgd_text = [lgd_text, strcat("$\alpha$ = ", num2str(param.Alpha(i_alpha_comp), '%.2f'))];
    end
    
%     pl = plot(-IE_se(end), -UF_se(end),...
%             'LineStyle', 'none',...
%             'Marker', 'o',...
%             'MarkerSize', 10,...
%             'Color', cmap_parula(end,:));
%     pl.MarkerFaceColor = pl.Color;
%     lgd_text = [lgd_text, strcat("$\alpha$ = ", num2str(param.Alpha(end), '%.3f'))];
end
axes = gca;
axis_semi_tight(axes, 1.2);
% axes.Title.Interpreter = 'latex';
% axes.Title.String = 'Simulation results';
% axes.Title.FontSize = 20;
axes.XAxis.TickLabelInterpreter = 'latex';
axes.XAxis.FontSize = 12;
axes.YAxis.TickLabelInterpreter = 'latex';
axes.YAxis.FontSize = 12;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = 'Efficiency (mean of rewards)';
axes.XLabel.FontSize = 20;
axes.YLabel.Interpreter = 'latex';
axes.YLabel.String = 'Fairness (negative std-dev of rewards)';
axes.YLabel.FontSize = 20;
lgd = legend(lgd_text);
lgd.Interpreter = 'latex';
lgd.FontSize = 16;
lgd.Location = 'bestoutside';

%% Performance vs. alpha plots
if control.se_policies
    performance_alpha_fg = fg;
    figure(performance_alpha_fg);
    fg = fg + 1;
    fig = gcf;
    fig.Position = [0, 0, screenwidth, screenheight];
    
    % Efficiency vs. alpha
    subplot(2,2,1);
    plot(param.Alpha, -IE_se, '--o', 'LineWidth', 2, 'MarkerSize', 10);
    hold on;
    plot(param.Alpha, -IE_rand * ones(param.n_alpha_comp, 1), 'LineWidth', 2);
    plot(param.Alpha, -IE_u * ones(param.n_alpha_comp, 1), 'LineWidth', 2);
    if control.highest_karma
        plot(param.Alpha, -IE_highest_karma * ones(param.n_alpha_comp, 1), 'LineWidth', 2);
    end
    axes = gca;
    axis_semi_tight(axes, 1.2);
    axes.Title.FontName = 'ubuntu';
    axes.Title.String = 'Efficiency vs. \alpha';
    axes.Title.FontSize = 20;
    axes.XAxis.FontName = 'ubuntu';
    axes.XAxis.FontSize = 20;
    axes.YAxis.FontName = 'ubuntu';
    axes.YAxis.FontSize = 20;
    axes.XLabel.FontName = 'ubuntu';
    axes.XLabel.FontWeight = 'bold';
    axes.XLabel.String = 'Future discount factor';
    axes.XLabel.FontSize = 20;
    axes.YLabel.FontName = 'ubuntu';
    axes.YLabel.FontWeight = 'bold';
    axes.YLabel.String = 'Negative mean of costs';
    axes.YLabel.FontSize = 20;

    % Fairness based on accumulated rewards vs. alpha
    subplot(2,2,2);
    plot(param.Alpha, -UF_se, '--o', 'LineWidth', 2, 'MarkerSize', 10);
    hold on;
    plot(param.Alpha, -UF_rand * ones(param.n_alpha_comp, 1), 'LineWidth', 2);
    plot(param.Alpha, -UF_u * ones(param.n_alpha_comp, 1), 'LineWidth', 2);
    plot(param.Alpha, -UF_a * ones(param.n_alpha_comp, 1), 'LineWidth', 2);
    if control.highest_karma
        plot(param.Alpha, -UF_highest_karma * ones(param.n_alpha_comp, 1), 'LineWidth', 2);
    end
    axes = gca;
    axis_semi_tight(axes, 1.2);
    axes.Title.FontName = 'ubuntu';
    axes.Title.String = 'Rewards Fairness vs. \alpha';
    axes.Title.FontSize = 20;
    axes.XAxis.FontName = 'ubuntu';
    axes.XAxis.FontSize = 20;
    axes.YAxis.FontName = 'ubuntu';
    axes.YAxis.FontSize = 20;
    axes.XLabel.FontName = 'ubuntu';
    axes.XLabel.FontWeight = 'bold';
    axes.XLabel.String = 'Future discount factor';
    axes.XLabel.FontSize = 20;
    axes.YLabel.FontName = 'ubuntu';
    axes.YLabel.FontWeight = 'bold';
    axes.YLabel.String = 'Rewards Fairness (negative std-dev of rewards)';
    axes.YLabel.FontSize = 20;
    
    % Fairness based on # of passes vs. alpha
    subplot(2,2,3);
    plot(param.Alpha, -UF_pass_se, '--o', 'LineWidth', 2, 'MarkerSize', 10);
    hold on;
    plot(param.Alpha, -UF_pass_rand * ones(param.n_alpha_comp, 1), 'LineWidth', 2);
    plot(param.Alpha, -UF_pass_u * ones(param.n_alpha_comp, 1), 'LineWidth', 2);
    plot(param.Alpha, -UF_pass_apass * ones(param.n_alpha_comp, 1), 'LineWidth', 2);
    if control.highest_karma
        plot(param.Alpha, -UF_pass_highest_karma * ones(param.n_alpha_comp, 1), 'LineWidth', 2);
    end
    axes = gca;
    axis_semi_tight(axes, 1.2);
    axes.Title.FontName = 'ubuntu';
    axes.Title.String = 'Pass Fairness vs. \alpha';
    axes.Title.FontSize = 20;
    axes.XAxis.FontName = 'ubuntu';
    axes.XAxis.FontSize = 20;
    axes.YAxis.FontName = 'ubuntu';
    axes.YAxis.FontSize = 20;
    axes.XLabel.FontName = 'ubuntu';
    axes.XLabel.FontWeight = 'bold';
    axes.XLabel.String = 'Future discount factor';
    axes.XLabel.FontSize = 20;
    axes.YLabel.FontName = 'ubuntu';
    axes.YLabel.FontWeight = 'bold';
    axes.YLabel.String = 'Pass Fairness (negative std-dev of #-pass)';
    axes.YLabel.FontSize = 20;
    
%     % GDP
%     subplot(2,2,3);
%     plot(param.Alpha, p_bar_se, '--o', 'LineWidth', 2);
%     axes = gca;
%     axis_semi_tight(axes, 1.2);
%     axes.Title.Interpreter = 'latex';
%     axes.Title.String = 'GDP vs. $\alpha$';
%     axes.Title.FontSize = 16;
%     axes.XAxis.TickLabelInterpreter = 'latex';
%     axes.XAxis.FontSize = 10;
%     axes.YAxis.TickLabelInterpreter = 'latex';
%     axes.YAxis.FontSize = 10;
%     axes.XLabel.Interpreter = 'latex';
%     axes.XLabel.String = 'Future discount factor';
%     axes.XLabel.FontSize = 14;
%     axes.YLabel.Interpreter = 'latex';
%     axes.YLabel.String = 'GDP (mean of payments)';
%     axes.YLabel.FontSize = 14;
% 
%     % GDP variance
%     subplot(2,2,4);
%     plot(param.Alpha, -p_var_se, '--o', 'LineWidth', 2);
%     axes = gca;
%     axis_semi_tight(axes, 1.2);
%     axes.Title.Interpreter = 'latex';
%     axes.Title.String = 'GDP fairness vs. $\alpha$';
%     axes.Title.FontSize = 16;
%     axes.XAxis.TickLabelInterpreter = 'latex';
%     axes.XAxis.FontSize = 10;
%     axes.YAxis.TickLabelInterpreter = 'latex';
%     axes.YAxis.FontSize = 10;
%     axes.XLabel.Interpreter = 'latex';
%     axes.XLabel.String = 'Future discount factor';
%     axes.XLabel.FontSize = 14;
%     axes.YLabel.Interpreter = 'latex';
%     axes.YLabel.String = 'GDP fairness (negative variance of payments)';
%     axes.YLabel.FontSize = 14;
end

%% Karma distribution plot
i_alpha_to_plot = 1;
if control.se_policies
    karma_dist_fg = fg;
    figure(karma_dist_fg);
    fg = fg + 1;
    fig = gcf;
    fig.Position = [0, 0, screenwidth, screenheight];
    
    subplot(2,2,1);
    bar(K_se{i_alpha_to_plot}, k_se_dist_pred{i_alpha_to_plot});
    axis tight;
    axes = gca;
    axes.Title.FontName = 'ubuntu';
    axes.Title.String = ['SE predicted karma distribution for \alpha = ', num2str(param.Alpha(i_alpha_to_plot), '%.2f')];
    axes.Title.FontSize = 20;
    axes.XAxis.FontName = 'ubuntu';
    axes.XAxis.FontSize = 20;
    axes.YAxis.FontName = 'ubuntu';
    axes.YAxis.FontSize = 20;
    axes.XLabel.FontName = 'ubuntu';
    axes.XLabel.FontWeight = 'bold';
    axes.XLabel.String = 'Karma';
    axes.XLabel.FontSize = 20;
    axes.YLabel.FontName = 'ubuntu';
    axes.YLabel.FontWeight = 'bold';
    axes.YLabel.String = 'Distribution';
    axes.YLabel.FontSize = 20;
    
    subplot(2,2,2);
    K = 0 : length(k_se_dist{i_alpha_to_plot}) - 1;
    bar(K, k_se_dist{i_alpha_to_plot});
    axis tight;
    axes = gca;
    axes.Title.FontName = 'ubuntu';
    axes.Title.String = ['Empirical karma distribution for \alpha = ', num2str(param.Alpha(i_alpha_to_plot), '%.2f')];
    axes.Title.FontSize = 20;
    axes.XAxis.FontName = 'ubuntu';
    axes.XAxis.FontSize = 20;
    axes.YAxis.FontName = 'ubuntu';
    axes.YAxis.FontSize = 20;
    axes.XLabel.FontName = 'ubuntu';
    axes.XLabel.FontWeight = 'bold';
    axes.XLabel.String = 'Karma';
    axes.XLabel.FontSize = 20;
    axes.YLabel.FontName = 'ubuntu';
    axes.YLabel.FontWeight = 'bold';
    axes.YLabel.String = 'Distribution';
    axes.YLabel.FontSize = 20;
    
    subplot(2,2,3);
    bar(K, k_se_dist_agents{i_alpha_to_plot}(:,1:param.n_a/2));
    axis tight;
    axes = gca;
    axes.Title.FontName = 'ubuntu';
    axes.Title.String = ['Empirical karma distribution per agent (batch 1) for \alpha = ', num2str(param.Alpha(i_alpha_to_plot), '%.2f')];
    axes.Title.FontSize = 20;
    axes.XAxis.FontName = 'ubuntu';
    axes.XAxis.FontSize = 20;
    axes.YAxis.FontName = 'ubuntu';
    axes.YAxis.FontSize = 20;
    axes.XLabel.FontName = 'ubuntu';
    axes.XLabel.FontWeight = 'bold';
    axes.XLabel.String = 'Karma';
    axes.XLabel.FontSize = 20;
    axes.YLabel.FontName = 'ubuntu';
    axes.YLabel.FontWeight = 'bold';
    axes.YLabel.String = 'Distribution';
    axes.YLabel.FontSize = 20;
    
    subplot(2,2,4);
    bar(K, k_se_dist_agents{i_alpha_to_plot}(:,param.n_a/2+1:end));
    axis tight;
    axes = gca;
    axes.Title.FontName = 'ubuntu';
    axes.Title.String = ['Empirical karma distribution per agent (batch 2) for \alpha = ', num2str(param.Alpha(i_alpha_to_plot), '%.2f')];
    axes.Title.FontSize = 20;
    axes.XAxis.FontName = 'ubuntu';
    axes.XAxis.FontSize = 20;
    axes.YAxis.FontName = 'ubuntu';
    axes.YAxis.FontSize = 20;
    axes.XLabel.FontName = 'ubuntu';
    axes.XLabel.FontWeight = 'bold';
    axes.XLabel.String = 'Karma';
    axes.XLabel.FontSize = 20;
    axes.YLabel.FontName = 'ubuntu';
    axes.YLabel.FontWeight = 'bold';
    axes.YLabel.String = 'Distribution';
    axes.YLabel.FontSize = 20;
end