clc;
screenwidth = 1920;
screenheight = 1080;

% %% Specify file
% file = 'stationary_equilibrium/results/se_U_1_1_10_phi1_0.95_0.05_0.00_0.00_0.50_0.50_0.95_0.05_0.00_alpha1_0.90_alpha2_1.00_pay_0_tax_0.005/k_bar_10_z_0.90_0.10.mat';
% 
% %% Cost fairness
% load(file);

% Truthful and non-truthful urgency types. We assume:
% - Urgency is i.i.d. in both urgency types
% - Only 1 alpha type
i_mutrue = 2;
i_mufalse = 3 - i_mutrue;
se_d_down_mutrue_up_utrue_k = squeeze(1 / param.g_up_mu_alpha(i_mutrue,1) * se_d_up_mu_alpha_u_k(i_mutrue,1,:,:));
se_d_down_mufalse_up_ufalse_k = squeeze(1 / param.g_up_mu_alpha(i_mufalse,1) * se_d_up_mu_alpha_u_k(i_mufalse,1,:,:));

% Efficiency per type
e_down_mu = zeros(param.n_mu, 1);
for i_mu = 1 : param.n_mu
    if i_mu == i_mutrue
        e_down_mu(i_mu) = dot(reshape(se_d_down_mutrue_up_utrue_k, [], 1), reshape(se_R_down_mu_alpha_u_k(i_mu,1,:,:), [], 1));
    else
        for i_u = 1 : param.n_u
            ratio_true = param.prob_down_mu_up_u(i_mutrue,i_u) / param.prob_down_mu_up_u(i_mufalse,i_u);
            ratio_true = min([ratio_true, 1]);
            ratio_true = max([ratio_true, 0]);
            
            e_down_mu(i_mu) = e_down_mu(i_mu) + ratio_true * dot(squeeze(se_d_down_mufalse_up_ufalse_k(i_u,:)), squeeze(se_R_down_mu_alpha_u_k(i_mu,1,i_u,:)));
            
            se_R_down_mufalse_utrue_k = einsum('ijklm,km->l', se_pi_down_mu_alpha_u_k_up_b(i_mu,1,i_u,:,:), reward_down_u_b(3-i_u,:));
            e_down_mu(i_mu) = e_down_mu(i_mu) + (1 - ratio_true) * dot(squeeze(se_d_down_mufalse_up_ufalse_k(i_u,:)), se_R_down_mufalse_utrue_k);
        end
    end
end

% Overall efficiency
e = dot(param.g_up_mu_alpha, e_down_mu);

e_array = e;
labels = "overall";
for i_mu = 1 : param.n_mu
    for i_alpha = 1 : param.n_alpha
        e_array = [e_array, e_down_mu(i_mu,i_alpha)];
        label_str = strcat("\mu=", int2str(i_mu), ", \alpha=", num2str(param.Alpha(i_alpha), '%.2f'));
        labels = [labels, label_str];
    end
end

%% Plot
figure(1);
fig = gcf;
fig.Position = [0, 0, screenheight, screenheight];
bar(categorical(labels), e_array, 'FaceColor', 'none', 'EdgeColor', 'flat', 'LineWidth', 2);
hold on;
axes = gca;
axes.XAxis.FontSize = 15;
axes.XAxis.FontName = 'ubuntu';
axes.YAxis.FontSize = 15;
axes.YAxis.FontName = 'ubuntu';
axes.XLabel.FontName = 'ubuntu';
axes.XLabel.FontWeight = 'bold';
axes.XLabel.FontSize = 20;
axes.YLabel.FontName = 'ubuntu';
axes.YLabel.FontWeight = 'bold';
axes.YLabel.String = 'Efficiency';
axes.YLabel.FontSize = 20;