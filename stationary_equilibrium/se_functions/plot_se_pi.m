% Plot SE policy
function plot_se_pi(fg, position, colormap, se_pi_down_mu_alpha_u_k_up_a, param, se_param, i_alpha_comp)
    persistent se_pi_plot
    if ~ishandle(fg)
        figure(fg);
        fig = gcf;
        fig.Position = position;
        se_pi_plot = cell(param.n_mu, param.n_alpha, param.n_u);
        n_subplots = param.n_mu * param.n_alpha * param.n_u;
        n_cols = ceil(sqrt(n_subplots));
        n_rows = ceil(n_subplots / n_cols);
        for i_mu = 1 : param.n_mu
            for i_alpha = 1 : param.n_alpha
                for i_u = 1 : param.n_u
                    pi_mat = squeeze(se_pi_down_mu_alpha_u_k_up_a(i_mu,i_alpha,i_u,:,:));
                    if ~param.limited_actions
                        for i_k = 1 : se_param.n_k - 1
                            pi_mat(i_k,i_k+1:se_param.n_k) = nan;
                        end
                    end
                    i_subplot = (i_mu - 1) * (param.n_alpha * param.n_u) + (i_alpha - 1) * param.n_u + i_u;
                    subplot(n_rows, n_cols, i_subplot);
                    se_pi_plot{i_mu,i_alpha,i_u} = heatmap(se_param.K, se_param.A, pi_mat.', 'MissingDataColor', '#C0C0C0', 'ColorbarVisible','off');
                    se_pi_plot{i_mu,i_alpha,i_u}.YDisplayData = flipud(se_pi_plot{i_mu,i_alpha,i_u}.YDisplayData);
                    alpha = param.Alpha(max([i_alpha, i_alpha_comp]));
                    if alpha > 0.99 && alpha < 1
                        alpha_str = num2str(alpha, '%.3f');
                    else
                        alpha_str = num2str(alpha, '%.2f');
                    end
                    se_pi_plot{i_mu,i_alpha,i_u}.Title = ['k_{bar} = ', num2str(param.k_bar, '%02d'), ' SE policy for \mu = ', num2str(i_mu), ', \alpha = ', alpha_str, ', u = ', num2str(param.U(i_u))];
                    se_pi_plot{i_mu,i_alpha,i_u}.XLabel = 'Karma';
                    if param.limited_actions
                        se_pi_plot{i_mu,i_alpha,i_u}.YLabel = 'Action';
                    else
                        se_pi_plot{i_mu,i_alpha,i_u}.YLabel = 'Bid';
                    end
                    se_pi_plot{i_mu,i_alpha,i_u}.FontName = 'Ubuntu';
                    se_pi_plot{i_mu,i_alpha,i_u}.FontSize = 20;
                    if exist('colormap', 'var')
                        se_pi_plot{i_mu,i_alpha,i_u}.Colormap = colormap;
                    end
                    se_pi_plot{i_mu,i_alpha,i_u}.ColorLimits = [0 1];
                    se_pi_plot{i_mu,i_alpha,i_u}.CellLabelColor = 'none';
                    se_pi_plot{i_mu,i_alpha,i_u}.GridVisible = false;
                    for i_k = 1 : se_param.n_k
                        if mod(i_k - 1, 5) ~= 0
                            se_pi_plot{i_mu,i_alpha,i_u}.XDisplayLabels{i_k} = '';
                            if ~param.limited_actions
                                se_pi_plot{i_mu,i_alpha,i_u}.YDisplayLabels{i_k} = '';
                            end
                        end
                    end
                end
            end
        end
    else
        for i_mu = 1 : param.n_mu
            for i_alpha = 1 : param.n_alpha
                for i_u = 1 : param.n_u
                    pi_mat = squeeze(se_pi_down_mu_alpha_u_k_up_a(i_mu,i_alpha,i_u,:,:));
                    if ~param.limited_actions
                        for i_k = 1 : se_param.n_k - 1
                            pi_mat(i_k,i_k+1:se_param.n_k) = nan;
                        end
                    end
                    se_pi_plot{i_mu,i_alpha,i_u}.ColorData = pi_mat.';
                    alpha = param.Alpha(max([i_alpha, i_alpha_comp]));
                    if alpha > 0.99 && alpha < 1
                        alpha_str = num2str(alpha, '%.3f');
                    else
                        alpha_str = num2str(alpha, '%.2f');
                    end
                    se_pi_plot{i_mu,i_alpha,i_u}.Title = ['k_{bar} = ', num2str(param.k_bar, '%02d'), ' SE policy for \mu = ', num2str(i_mu), ', \alpha = ', alpha_str, ', u = ', num2str(param.U(i_u))];
                end
            end
        end
    end
end