clc;
screenwidth = 1920;
screenheight = 1080;

%% Some parameters
alpha_eff_comp = [1 : -0.01 : 0.96, 0.95 : -0.05 : 0];
n_alpha_eff_comp = length(alpha_eff_comp);

file_start = 'stationary_equilibrium/results/se_U_1_1_10_phi1_0.95_0.05_0.00_0.00_0.50_0.50_0.95_0.05_0.00_pay_3/k_bar_10_alpha_';

%% Efficiency arrays
se_E = zeros(1, n_alpha_eff_comp);

for i_alpha_eff_comp = 1 : n_alpha_eff_comp
    alpha = alpha_eff_comp(i_alpha_eff_comp);
    file = [file_start, num2str(alpha, '%.2f'), '.mat'];
    if exist(file, 'file')
        load(file);

        % SE efficiency
        se_E(i_alpha_eff_comp) = dot(reshape(se_d_up_mu_alpha_u_k, [], 1), reshape(se_R_down_mu_alpha_u_k, [], 1));

        % Random efficiency
        rand_E = -0.5 * dot(se_upsilon_up_u, param.U);
        
        % Centralized urgency (best possible) efficiency
        u_E = 0;
        for i_u = 1 : param.n_u
            u_E = u_E - 0.5 * se_upsilon_up_u(i_u)^2 * param.U(i_u);
            for i_uj = i_u + 1 : param.n_u
                u_E = u_E - se_upsilon_up_u(i_u) * se_upsilon_up_u(i_uj) * param.U(i_u);
            end
        end
    else
        se_E(i_alpha_eff_comp) = rand_E;
    end
end

%% Plot
figure(1);
fig = gcf;
fig.Position = [0, 0, screenheight, screenheight];
plot(alpha_eff_comp, rand_E * ones(1, n_alpha_eff_comp), 'LineWidth', 2);
hold on;
plot(alpha_eff_comp, u_E * ones(1, n_alpha_eff_comp), 'LineWidth', 2);
plot(alpha_eff_comp, se_E, '--o', 'LineWidth', 2, 'MarkerSize', 10);
axes = gca;
axis_semi_tight(axes, 1.2);
axes.XAxis.FontSize = 15;
axes.XAxis.FontName = 'ubuntu';
axes.YAxis.FontSize = 15;
axes.YAxis.FontName = 'ubuntu';
axes.XLabel.FontName = 'ubuntu';
axes.XLabel.FontWeight = 'bold';
axes.XLabel.String = 'Future discount factor';
axes.XLabel.FontSize = 20;
axes.YLabel.FontName = 'ubuntu';
axes.YLabel.FontWeight = 'bold';
axes.YLabel.String = 'Efficiency';
axes.YLabel.FontSize = 20;