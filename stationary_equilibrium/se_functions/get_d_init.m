% Gets initial type-state distribution
function d_up_mu_alpha_u_k_init = get_d_init(k_bar, param, se_param)
    % Initial urgency distribution per urgency type
    prob_down_mu_up_u_init = param.prob_down_mu_up_u;
    
    % Initial karma distribution - independent of agent types
    switch se_param.karma_initialization
        case 0
            % All agents have average karma k_bar
            sigma_up_k_init = zeros(se_param.n_k, 1);
            i_k_bar = find(se_param.K == k_bar);
            sigma_up_k_init(i_k_bar) = 1;
        case 1
            % Uniform distribution over [0 : 2 * k_bar]
            sigma_up_k_init = get_sigma_up_k_uniform(k_bar, se_param);
    end
    
    % Initial type-state distribution
    d_up_mu_alpha_u_k_init = einsum('ij,ik,lm->ijkl', param.g_up_mu_alpha, prob_down_mu_up_u_init, sigma_up_k_init);
end