% Write corrective fairness to csv file
function write_corrective_fairness_csv(alpha_fair_comp, T, w_down_alpha_T, fileprefix)
    % Pre-processing
    n_alpha_fair_comp = length(alpha_fair_comp);
    n_T = length(T);
    T = reshape(T, [], 1);

    % Header
    header = ["alpha", "T", "w"];
    filename = [fileprefix, '-corrective-fairness.csv'];
    fout = fopen(filename, 'w');
    for i = 1 : length(header) - 1
        fprintf(fout, '%s,', header(i));
    end
    fprintf(fout, '%s\n', header(end));
    fclose(fout);

    % Data
    for i_alpha_fair_comp = 1 : n_alpha_fair_comp
        alpha = alpha_fair_comp(i_alpha_fair_comp);
        data = [alpha * ones(n_T, 1), T, w_down_alpha_T(i_alpha_fair_comp,:).'];
        dlmwrite(filename, data, '-append');
    end
    
    % Coin toss
    alpha = -1;
    data = [alpha * ones(n_T, 1), T, 0.5 * ones(n_T, 1)];
    dlmwrite(filename, data, '-append');
    
    % Ideal
    alpha = 100;
    data = [alpha * ones(n_T, 1), T, ones(n_T, 1)];
    dlmwrite(filename, data, '-append');
end