% Write efficiency array to csv file
function write_efficiency_csv(alpha_eff_comp, se_E, rand_E, u_E, fileprefix)
    % Pre-processing
    n_alpha_eff_comp = length(alpha_eff_comp);
    alpha_eff_comp = reshape(alpha_eff_comp, [], 1);
    se_E = reshape(se_E, [], 1);

    % Header
    header = ["alpha", "e", "e-rand", "e-u"];
    filename = [fileprefix, '-efficiency.csv'];
    fout = fopen(filename, 'w');
    for i = 1 : length(header) - 1
        fprintf(fout, '%s,', header(i));
    end
    fprintf(fout, '%s\n', header(end));
    fclose(fout);

    % Data
    data = [alpha_eff_comp, se_E, rand_E * ones(n_alpha_eff_comp, 1), u_E * ones(n_alpha_eff_comp, 1)];
    dlmwrite(filename, data, '-append');
end