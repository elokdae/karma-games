% Write efficiency of multiple urgency types to csv file
function write_efficiency_mult_mu_csv(w1_eff_comp, se_access_eff, se_cost_eff, se_cost_eff_mu1_truthful, se_cost_eff_mu2_truthful, param, fileprefix)
    % Pre-processing
    n_w1_eff_comp = length(w1_eff_comp);
    w1_eff_comp = reshape(w1_eff_comp, [], 1);
    MU = [(1 : param.n_mu).'; -1];

    % Header
    header = ["g-1", "g-2", "mu", "w", "e", "e-mu1-truth", "e-mu2-truth"];
    filename = [fileprefix, '-efficiency.csv'];
    fout = fopen(filename, 'w');
    for i = 1 : length(header) - 1
        fprintf(fout, '%s,', header(i));
    end
    fprintf(fout, '%s\n', header(end));
    fclose(fout);

    % Data
    for i_w1_eff_comp = 1 : n_w1_eff_comp
        W1 = w1_eff_comp(i_w1_eff_comp) * ones(param.n_mu + 1, 1);
        W2 = 1 - W1;
        data = [W1, W2, MU, se_access_eff(:,i_w1_eff_comp), se_cost_eff(:,i_w1_eff_comp), se_cost_eff_mu1_truthful(:,i_w1_eff_comp), se_cost_eff_mu2_truthful(:,i_w1_eff_comp)];
        dlmwrite(filename, data, '-append');
    end
end