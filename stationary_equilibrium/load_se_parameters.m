function se_param = load_se_parameters(param)
%% Set of karma values
% Maximum karma level. Used as a starting point
se_param.k_max = 60;

% Tolerance for maximum probability of k_max
se_param.max_sigma_k_max = 1e-4;  % Use for SE computation
% se_param.max_sigma_k_max = 1e-3;    % Use for SW computation

% Step size for increasing k_max on saturation detection
se_param.k_max_step = 5;

% Maximum k_max allowable. Code asserts if this needs to be exceeded
se_param.max_k_max = 120;

% Set of all karma values
se_param.K = (0 : se_param.k_max).';

% Number of karma values
se_param.n_k = length(se_param.K);

% Where is k_bar?
se_param.i_k_bar = find(se_param.K == param.k_bar);

% Number of states
se_param.n_x = param.n_u * se_param.n_k;

% Number of actions
if param.limited_actions
    se_param.n_a = length(param.b_down_a);
    se_param.A = (1 : se_param.n_a).';
else
    se_param.n_a = se_param.n_k;
    se_param.A = se_param.K;
end

% Set of output values
se_param.O = [0; 1];

% Number of output values
se_param.n_o = length(se_param.O);

%% Initialization
% Policy initialization
% 0 => Bid urgency
% 1 => Bid 0.5 * u / u_max * k (~ 'bid half if urgent')
% 2 => Bid 1 * u / u_max * k (~ 'bid all if urgent')
% 3 => Bid random
% 4 => Bid all
% 10 => From file
se_param.policy_initialization = 1;
se_param.policy_initialization_file = 'stationary_equilibrium/results/k_bar_10_alpha_0.90.mat';

% Karma distribution initialization
% 0 => All agents have average karma k_bar
% 1 => Uniform distribution over [0 : 2 * k_bar]
se_param.karma_initialization = 0;

%% Computation parameters
% Bounded rationality
se_param.lambda = 1000;
% se_param.lambda = 100000;

% Tolerance for convergence of equilibrium policy
se_param.se_pi_tol = 1e-6;

% Tolerance for convergence of type-state distribution
se_param.se_d_tol = 1e-6;

% Maximum number of iterations
se_param.max_iter = 2000;
% se_param.max_iter = 10000;

% Tolerance for convergence of value function
se_param.V_tol = 1e-6;

% Maximum number of iterations for convergence of value function
se_param.V_max_iter = 1000;

% Tolerance for best response single-stage deviation
se_param.br_Q_tol = 1e-4;

% Discrete step size
se_param.dt = 0.05;
% se_param.dt = 0.1;

% Policy update rate
se_param.delta = 1.0;
% se_param.delta = 0.1;

% Keep history of evolution
se_param.store_hist = false;

% Limit cycle check horizon
% 0 = do not check limit cycles
se_param.limit_cycle_horizon = 0;

% Do plots
se_param.plot = false;

% Save results
se_param.save = true;

end