\section{Stationary Nash Equilibrium in the case of no future discounting}
\label{sec:alpha1}

We have presented results for the case when agents do not discount their future rewards, i.e., when $\alpha_\tau=1$, which does not fit the dynamic population model of Section~\ref{sec:Model}. In particular, the expected infinite horizon reward $V_\tau(d,\pi)$~\eqref{eq:V-function-full} is not well defined for $\alpha_\tau = 1$.
Nevertheless, the case of no future discounting can be treated by considering that the agents face an \emph{average reward per time step} problem rather than a discounted problem~\cite{bertsekas2007dynamic}.
Analysis of the average reward per time step problem is intricate and we refer the reader to~\cite[Chapter~4]{bertsekas2007dynamic} for the details. For our purposes, it suffices to use the following proposition.

\begin{proposition}[\cite{bertsekas2007dynamic}~Propositions~4.2.1--4.2.2]
For a fixed social state $(d,\pi)$, if a scalar $\sigma_\tau(d,\pi)$ and a vector $Y_\tau(d,\pi)$ satisfy
\begin{multline}
    \label{eq:alpha1-V-function}
    \sigma_\tau(d,\pi) + Y_\tau[u,k](d,\pi) \\
    = R_\tau[u,k](d,\pi) + \sum_{u^+,k^+} P_\tau[u^+,k^+ \mid u,k](d,\pi) \: Y_\tau[u^+,k^+](d,\pi), \quad \forall u,k,
\end{multline}
then $\sigma_\tau(d,\pi)$ is the expected average reward per time step of the ego agent of type $\tau$ starting from any state $[u,k]$, supposing that $(d,\pi)$ is not time-varying.

Furthermore, if
\begin{multline}
    \label{eq:alpha1Optimality}
    \sigma_\tau(d,\pi) + Y_\tau[u,k](d,\pi) \\
    = \max_{b \in \Bids^k} \left[\reward[u,b](d,\pi) + \sum_{u^+,k^+} \transition_\tau[u^+,k^+ \mid u,k,b](d,\pi) \: Y_\tau[u^+,k^+](d,\pi)\right], \quad \forall u,k,
\end{multline}
then $\sigma_\tau(d,\pi)$ is the optimal expected average reward per time step of the ego agent of type $\tau$, and $\pi_\tau$ is an optimal policy for $\alpha_\tau=1$.
\end{proposition}

A stationary Nash equilibrium for $\alpha_\tau=1$ is then a social state $(\sd,\epi)$ where $\sd$ is stationary, i.e., \eqref{eq:SNE-1} holds, and $\epi_\tau$ satisfies~\eqref{eq:alpha1Optimality}.
Noting the similarity between~\eqref{eq:alpha1-V-function} and~\eqref{eq:V-function-full}, Algorithm~\ref{alg:StationaryEquilibrium} can be readily modified to seek a stationary Nash equilibrium for $\alpha_\tau=1$.
Instead of computing $V_\tau(d,\pi)$, we compute the pair $(\sigma_\tau(d,\pi),Y_\tau(d,\pi))$ as follows.
Noting that if $Y_\tau(d,\pi)$ satisfies~\eqref{eq:alpha1-V-function}, so will $\tilde{Y}_\tau(d,\pi) = Y_\tau(d,\pi) + y \mathbbm{1}$ for any constant offset $y$, we can eliminate this degree of freedom by fixing $Y_\tau[u_1,0](d,\pi) = 0$ for the arbitrary `default state' $[u,k]=[u_1,0]$, which allows us to re-write~\eqref{eq:alpha1-V-function} as
\begin{align*}
    Y_\tau[u_1,0](d,\pi) &= 0, \\
    \sigma_\tau(d,\pi) &= R_\tau[u_1,0](d,\pi) + \sum_{u^+,k^+} P_\tau[u^+,k^+ \mid u_1,0](d,\pi) \: Y_\tau[u^+,k^+](d,\pi), \\
    Y_\tau[u,k](d,\pi) &= R_\tau[u,k](d,\pi) + \sum_{u^+,k^+} P_\tau[u^+,k^+ \mid u,k](d,\pi) \: Y_\tau[u^+,k^+](d,\pi)\\
    &\phantom{=} \quad  - \sigma_\tau(d,\pi), \quad \forall u,k.
\end{align*}
This system of equations can be solved iteratively by using an initial guess $Y_\tau^0(d,\pi)$ and updating $\sigma_\tau(d,\pi)$ and $Y_\tau(d,\pi)$ until convergence, a procedure known as \emph{relative value iteration}~\cite[Section~4.3.1]{bertsekas2007dynamic}.
The vector $Y_\tau(d,\pi)$ can be interpreted as a relative rewards vector, with $Y_\tau[u,k](d,\pi)$ representing transient reward advantages/disadvantages of state $[u,k]$ with respect to the `default state' $[u_1,0]$.
With this, we can define the single-stage deviation rewards $Q_\tau[u,k,b](d,\pi)$~\eqref{eq:SingleStageDeviation} with respect to $Y_\tau(d,\pi)$ instead of $V_\tau(d,\pi)$, and proceed with the remainder of Algorithm~\ref{alg:StationaryEquilibrium} unmodified.

%\begin{figure*}[tb!]
%    \centering
%% 	\includegraphics[height=.2\textwidth]{tikz/tikz-PBS-alpha-1-policy.pdf}
%% 	\hfil
%% 	\includegraphics[height=.2\textwidth]{tikz/tikz-PBS-alpha-1-karma-dist.pdf}
%% 	\medskip
%% 	\hrule
%	
%	\includegraphics[trim=27.95416pt 0 16.01173pt 0]{tikz/tikz-U-1-1-10-PBS-alpha-1-i-u-1-policy.pdf}
%	\hfil
%	\includegraphics[trim=16.53604pt 0 16.53604pt 0]{tikz/tikz-U-1-1-10-PBS-alpha-1-i-u-2-policy.pdf}
%	\hfil
%	\includegraphics[trim=17.30687pt 0 17.30687pt 0]{tikz/tikz-U-1-1-10-PBS-alpha-1-i-u-3-policy.pdf}
%	
%	\includegraphics[trim=27.95416pt 0 10.86873pt 0]{tikz/tikz-U-1-1-10-PBS-alpha-1-i-u-1-dist.pdf}
%	\hfil
%	\includegraphics[trim=17.21321pt 0 10.86873pt 0]{tikz/tikz-U-1-1-10-PBS-alpha-1-i-u-2-dist.pdf}
%	\hfil
%	\includegraphics[trim=17.21321pt 0 10.86873pt 0]{tikz/tikz-U-1-1-10-PBS-alpha-1-i-u-3-dist.pdf}
%	
%	\caption{Stationary Nash equilibrium when agents do not discount the future ($\alpha=1$).}
%	\label{fig:PBS-alpha-1}
%\end{figure*}
\input{figures-code/alpha-1}

Figure~\ref{fig:PBS-alpha-1} shows the stationary Nash equilibrium under karma payment rule $\texttt{PBS}$ when all agents have $\alpha=1$ (we drop the subscript $\tau$ here because there is only one type), computed using this method.
The defining feature of the equilibrium bidding policy is that it is very sparing on karma, with the agents bidding 1 only in the high urgency state and zero otherwise, no matter how much karma they have (unless they have zero karma).
While on first investigation, this behaviour resembles a truthful declaration of the urgency, and indeed $\alpha=1$ performs well for the efficiency, it has a few negative consequences.
First, it leads to a high stationary mass of the high urgency, zero karma state, which means that a high fraction of times the high urgency agents do not have one karma to bid.
Second, the homogeneity of the bids leads to the karma losing its effectiveness as a turn-taking device, and results in the poor ex-post access fairness observed in Figure~\ref{fig:U-1-1-10-PBP-PBS-performance} at $\alpha=1$.

% Consequently, the \emph{karma hoarding} behavior that emerges when the agents do not discount the future is hurtful for the social welfare, both in terms of the efficiency and the fairness.
As we explored in Section~\ref{subsec:HeterogeneousAlpha}, an effective measure against this \emph{karma hoarding} behavior is to let the karma expire, e.g., by applying a karma tax.