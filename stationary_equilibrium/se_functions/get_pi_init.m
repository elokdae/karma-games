% Gets initial policy guess
function pi_down_mu_alpha_u_k_up_a_init = get_pi_init(param, se_param)
    % Initial policy - independent of agent types
    if se_param.policy_initialization == 10 % Initialize from file
        load(se_param.policy_initialization_file, 'se_pi_down_mu_alpha_u_k_up_a');
        pi_down_mu_alpha_u_k_up_a_init = se_pi_down_mu_alpha_u_k_up_a;
    else
        prob_down_u_k_up_a_init = zeros(param.n_u, se_param.n_k, se_param.n_a);
        for i_u = 1 : param.n_u
            for i_k = 1 : se_param.n_k
                if param.limited_actions
                    % Limited action initialization: bid according to the ratio to u_max
                    i_a = 1 + round(param.U(i_u) / param.U(end) * (se_param.n_a - 1));
                    prob_down_u_k_up_a_init(i_u,i_k,i_a) = 1;
                else
                    switch se_param.policy_initialization
                        case 0 
                            % Bid urgency
                            if param.n_u > 1
                                i_b = min([i_u, i_k]);
                            else
                                % Bid 1 if there is only 1 urgency level
                                i_b = min([2, i_k]);
                            end
                            prob_down_u_k_up_a_init(i_u,i_k,i_b) = 1;
                        case 1
                            % Bid 0.5 * u / u_max * k (~ 'bid half if urgent')
                            b = round(0.5 * param.U(i_u) / param.U(end) * se_param.K(i_k));
                            i_b = se_param.K == b;
                            prob_down_u_k_up_a_init(i_u,i_k,i_b) = 1;
                        case 2
                            % Bid 1 * u / u_max * k (~ 'bid all if urgent')
                            b = round(param.U(i_u) / param.U(end) * se_param.K(i_k));
                            i_b = se_param.K == b;
                            prob_down_u_k_up_a_init(i_u,i_k,i_b) = 1;
                        case 3
                            % Bid random
                            prob_down_u_k_up_a_init(i_u,i_k,1:i_k) = 1 / i_k;
                        case 4
                            % Bid all
                            i_b = i_k;
                            prob_down_u_k_up_a_init(i_u,i_k,i_b) = 1;
                    end
                end
            end
        end

        % Duplicate for agent types
        pi_down_mu_alpha_u_k_up_a_init = zeros(param.n_mu, param.n_alpha, param.n_u, se_param.n_k, se_param.n_a);
        for i_mu = 1 : param.n_mu
            for i_alpha = 1 : param.n_alpha
                pi_down_mu_alpha_u_k_up_a_init(i_mu,i_alpha,:,:,:) = prob_down_u_k_up_a_init;
            end
        end
    end
end