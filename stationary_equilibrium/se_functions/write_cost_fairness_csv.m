% Write cost fairness to csv file
function write_cost_fairness_csv(e, e_down_mu_alpha, param, fileprefix)
    % Header
    header = ["mu", "alpha", "index", "e"];
    filename = [fileprefix, '-cost-fairness.csv'];
    fout = fopen(filename, 'w');
    for i = 1 : length(header) - 1
        fprintf(fout, '%s,', header(i));
    end
    fprintf(fout, '%s\n', header(end));
    fclose(fout);

    % Data
    index_tot = floor(param.n_alpha / 2) + 1;
    data = [-1, -1, index_tot, e]; % Overall efficiency encoded as -1
    dlmwrite(filename, data, '-append');
    for i_mu = 1 : param.n_mu
        for i_alpha = 1 : param.n_alpha
            if i_alpha < index_tot
                index = i_alpha;
            else
                index = i_alpha + 1;
            end
            data = [i_mu, param.Alpha(i_alpha), index, e_down_mu_alpha(i_mu,i_alpha)];
            dlmwrite(filename, data, '-append');
        end
    end
end