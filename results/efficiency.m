clear;
close all;
clc;

%% Screen size used to place plots
screensize = get(groot, 'ScreenSize');
screenwidth = screensize(3);
screenheight = screensize(4);
default_width = screenwidth / 2;
default_height = screenheight / 2;
fg = 1;

%% Some parameters
k_bar_vec = 10;
alpha_comp_vec = [0.05 : 0.05 : 0.95, 0.96 : 0.01 : 1.00];

n_k_bar = length(k_bar_vec);
n_alpha_comp = length(alpha_comp_vec);

file_str = 'results/n_a_200_T_100000_U_1_10_phi1_0.50_0.50_0.50_pay_0/k_bar_';

sw_computed = true;

compare = false;
if compare
    compare_file_str = 'results/n_a_200_T_100000_U_1_10_phi1_0.80_0.20_0.40_0.60_pay_0/k_bar_';
end

% Flag to save plots
save_plots = true;

%% Inefficiency arrays
se_IE = nan(n_k_bar, n_alpha_comp);
if sw_computed
    sw_IE = zeros(n_k_bar, 1);
end
if compare
    compare_se_IE = nan(n_k_bar, n_alpha_comp);
end

for i_k_bar = 1 : n_k_bar
    % SE inefficiency
    for i_alpha_comp = 1 : n_alpha_comp
        alpha = alpha_comp_vec(i_alpha_comp);
        if alpha > 0.99 && alpha < 1
            file = [file_str, num2str(k_bar_vec(i_k_bar), '%02d'), '_alpha_', num2str(alpha, '%.3f'), '.mat'];
        else
            file = [file_str, num2str(k_bar_vec(i_k_bar), '%02d'), '_alpha_', num2str(alpha, '%.2f'), '.mat'];
        end
        if ~exist(file, 'file')
            file = [file_str, num2str(k_bar_vec(i_k_bar), '%02d'), '.mat'];
        end
        load(file, 'IE_se', 'param');
        i_alpha_sim = find(abs(param.Alpha - alpha) <= eps);
        if ~isempty(i_alpha_sim)
            se_IE(i_k_bar,i_alpha_comp) = IE_se{i_alpha_sim}(end);
        end
    end
    % SW inefficiency
    if sw_computed
        load(file, 'IE_sw');
        if exist('IE_sw', 'var')
            sw_IE(i_k_bar) = IE_sw(end);
            clear IE_sw
        else
            sw_IE(i_k_bar) = nan;
        end
    end
    % Compare inefficiency
    if compare
        for i_alpha_comp = 1 : n_alpha_comp
            alpha = alpha_comp_vec(i_alpha_comp);
            if alpha > 0.99 && alpha < 1
                compare_file = [compare_file_str, num2str(k_bar_vec(i_k_bar), '%02d'), '_alpha_', num2str(alpha, '%.3f'), '.mat'];
            else
                compare_file = [compare_file_str, num2str(k_bar_vec(i_k_bar), '%02d'), '_alpha_', num2str(alpha, '%.2f'), '.mat'];
            end
            if ~exist(compare_file, 'file')
                compare_file = [compare_file_str, num2str(k_bar_vec(i_k_bar), '%02d'), '.mat'];
            end
            load(compare_file, 'IE_se', 'param');
            i_alpha_sim = find(abs(param.Alpha - alpha) <= eps);
            if ~isempty(i_alpha_sim)
                compare_se_IE(i_k_bar,i_alpha_comp) = IE_se{i_alpha_sim}(end);
            end
        end
    end
    % Centralized urgency inefficiency (get once for comparison)
    if i_k_bar == 1
        load(file, 'IE_rand', 'IE_u');
        rand_IE = IE_rand(end);
        u_IE = IE_u(end);
    end
end

if save_plots
    if ~exist('results/efficiency', 'dir')
        mkdir('results/efficiency');
    end
end

%% Plot all
figure(fg);
fg = fg + 1;
fig = gcf;
fig.Position = [0, 0, screenwidth, screenheight];
plot(k_bar_vec, repmat(-u_IE, 1, n_k_bar), 'm-', 'LineWidth', 4);
lgd_text = "centralized-urgency";
hold on;
plot(k_bar_vec, repmat(-rand_IE, 1, n_k_bar), 'r-', 'LineWidth', 4);
lgd_text = [lgd_text; "baseline-random"];
if sw_computed
    plot(k_bar_vec, -sw_IE, 'g-', 'LineWidth', 3);
    lgd_text = [lgd_text; "social-welfare"];
end
for i_alpha_comp = 1 : n_alpha_comp
    plot(k_bar_vec, -se_IE(:,i_alpha_comp), '-x', 'LineWidth', 2);
    lgd_text = [lgd_text; strcat("$\alpha$ = ", num2str(alpha_comp_vec(i_alpha_comp), '%.2f'))];
end
if compare
    for i_alpha_comp = 1 : n_alpha_comp
        plot(k_bar_vec, -compare_se_IE(:,i_alpha_comp), '-x', 'LineWidth', 2);
        lgd_text = [lgd_text; strcat("$\alpha$ = ", num2str(alpha_comp_vec(i_alpha_comp), '%.2f'), "-2")];
    end
end
axis tight;
axes = gca;
axes.Title.Interpreter = 'latex';
axes.Title.String = 'Efficiency of different policies';
axes.Title.FontSize = 16;
axes.XAxis.FontSize = 10;
axes.YAxis.FontSize = 10;
axes.XLabel.Interpreter = 'latex';
axes.XLabel.String = 'Average karma';
axes.XLabel.FontSize = 14;
axes.YLabel.Interpreter = 'latex';
axes.YLabel.String = 'Efficiency';
axes.YLabel.FontSize = 14;
lgd = legend(lgd_text);
lgd.FontSize = 12;
lgd.Interpreter = 'latex';
lgd.Location = 'bestoutside';
yl = ylim;

if save_plots
    saveas(gcf, 'results/efficiency/all.png');
end

%% Plot slices through k_bar
figure(fg);
fg = fg + 1;
fig = gcf;
fig.Position = [0, 0, screenwidth, screenheight];
if n_k_bar == 1
    num_cols = 1;
    num_rows = 1;
else
    num_cols = round(sqrt(screenwidth / screenheight * (n_k_bar + 1)));
    num_rows = ceil((n_k_bar + 1) / num_cols);
end
for i_k_bar = 1 : n_k_bar
    subplot(num_rows,num_cols,i_k_bar);
    semilogx(alpha_comp_vec, repmat(-u_IE, 1, n_alpha_comp), 'm-', 'LineWidth', 4);
    hold on;
    semilogx(alpha_comp_vec, repmat(-rand_IE, 1, n_alpha_comp), 'r-', 'LineWidth', 4);
    if sw_computed
        semilogx(alpha_comp_vec, repmat(-sw_IE(i_k_bar), 1, n_alpha_comp), 'g-', 'LineWidth', 3);
    end
    semilogx(alpha_comp_vec, -se_IE(i_k_bar,:), '-x', 'LineWidth', 2);
    if compare
        semilogx(alpha_comp_vec, -compare_se_IE(i_k_bar,:), '-o', 'LineWidth', 2);
    end
    axis tight;
    ylim(yl);
    axes = gca;
    axes.Title.Interpreter = 'latex';
    axes.Title.String = ['$k_{bar}$ = ', num2str(k_bar_vec(i_k_bar), '%02d')];
    axes.Title.FontSize = 14;
    axes.XAxis.FontSize = 10;
    axes.YAxis.FontSize = 10;
    axes.XLabel.Interpreter = 'latex';
    axes.XLabel.String = 'Future discount factor';
    axes.XLabel.FontSize = 12;
    axes.YLabel.Interpreter = 'latex';
    axes.YLabel.String = 'Efficiency';
    axes.YLabel.FontSize = 12;
end
if n_k_bar == 1
    lgd_text = "centralized-urgency";
    lgd_text = [lgd_text; "baseline-random"];
    if sw_computed
        lgd_text = [lgd_text; "social-welfare"];
    end
    lgd_text = [lgd_text; "stat-equilibrium"];
    if compare
        lgd_text = [lgd_text; "stat-equilibrium-2"];
    end
else
    subplot(num_rows,num_cols,n_k_bar+1);
    plot(nan, nan, 'm-', 'LineWidth', 4);
    lgd_text = "centralized-urgency";
    hold on;
    plot(nan, nan, 'r-', 'LineWidth', 4);
    lgd_text = [lgd_text; "baseline-random"];
    if sw_computed
        plot(nan, nan, 'g-', 'LineWidth', 3);
        lgd_text = [lgd_text; "social-welfare"];
    end
    plot(nan, nan, '-x', 'LineWidth', 2);
    lgd_text = [lgd_text; "stat-equilibrium"];
    if compare
        plot(nan, nan, '-o', 'LineWidth', 2);
        lgd_text = [lgd_text; "stat-equilibrium-2"];
    end
    axis off;
end
lgd = legend(lgd_text);
lgd.FontSize = 12;
lgd.Interpreter = 'latex';
lgd.Location = 'bestoutside';
title = sgtitle('Efficiency as a function of $\alpha$ for different average karmas');
title.Interpreter = 'latex';
title.FontSize = 16;

if save_plots
    saveas(gcf, 'results/efficiency/k_bar_slices.png');
end

%% Plot slices through alpha
figure(fg);
fg = fg + 1;
fig = gcf;
fig.Position = [0, 0, screenwidth, screenheight];
if n_alpha_comp == 1
    num_cols = 1;
    num_rows = 1;
else
    num_cols = round(sqrt(screenwidth / screenheight * (n_alpha_comp + 1)));
    num_rows = ceil((n_alpha_comp + 1) / num_cols);
end
for i_alpha_comp = 1 : n_alpha_comp
    subplot(num_rows,num_cols,i_alpha_comp);
    plot(k_bar_vec, repmat(-u_IE, 1, n_k_bar), 'm-', 'LineWidth', 4);
    hold on;
    plot(k_bar_vec, repmat(-rand_IE, 1, n_k_bar), 'r-', 'LineWidth', 4);
    if sw_computed
        plot(k_bar_vec, -sw_IE, 'g-', 'LineWidth', 3);
    end
    plot(k_bar_vec, -se_IE(:,i_alpha_comp), '-x', 'LineWidth', 2);
    axis tight;
    ylim(yl);
    axes = gca;
    axes.Title.Interpreter = 'latex';
    axes.Title.String = ['$\alpha$ = ', num2str(alpha_comp_vec(i_alpha_comp), '%0.2f')];
    axes.Title.FontSize = 14;
    axes.XAxis.FontSize = 10;
    axes.YAxis.FontSize = 10;
    axes.XLabel.Interpreter = 'latex';
    axes.XLabel.String = 'Average karma';
    axes.XLabel.FontSize = 12;
    axes.YLabel.Interpreter = 'latex';
    axes.YLabel.String = 'Efficiency';
    axes.YLabel.FontSize = 12;
end
if n_alpha_comp == 1
    lgd_text = "centralized-urgency";
    lgd_text = [lgd_text; "baseline-random"];
    if sw_computed
        lgd_text = [lgd_text; "social-welfare"];
    end
    lgd_text = [lgd_text; "stat-equilibrium"];
else
    subplot(num_rows,num_cols,n_alpha_comp+1);
    plot(nan, nan, 'm-', 'LineWidth', 4);
    lgd_text = "centralized-urgency";
    hold on;
    plot(nan, nan, 'r-', 'LineWidth', 4);
    lgd_text = [lgd_text; "baseline-random"];
    if sw_computed
        plot(nan, nan, 'g-', 'LineWidth', 3);
        lgd_text = [lgd_text; "social-welfare"];
    end
    plot(nan, nan, '-x', 'LineWidth', 2);
    lgd_text = [lgd_text; "stat-equilibrium"];
    axis off;
end
lgd = legend(lgd_text);
lgd.FontSize = 12;
lgd.Interpreter = 'latex';
lgd.Location = 'bestoutside';
title = sgtitle('Efficiency as a function of $k_{bar}$ for different future discount factors');
title.Interpreter = 'latex';
title.FontSize = 16;

if save_plots
    saveas(gcf, 'results/efficiency/alpha_slices.png');
end