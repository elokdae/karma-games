% Write policy to csv file for loading in Python ABS code
function write_policy_to_python_csv(pi_down_mu_alpha_u_k_up_b, param, se_param, i_alpha_comp, fileprefix)
    % Convert Matlab policy to Python policy, which could be pure or mixed
    python_policy = cell(param.n_mu, param.n_alpha, param.n_u, se_param.n_k);
    for i_mu = 1 : param.n_mu
        for i_alpha = 1 : param.n_alpha
            for i_u = 1 : param.n_u
                python_policy{i_mu,i_alpha,i_u,1} = 0;
                for i_k = 2 : se_param.n_k
                    i_max = 1 : i_k;
                    [pi_max, i_pi_max] = max(pi_down_mu_alpha_u_k_up_b(i_mu,i_alpha,i_u,i_k,i_max));
                    i_max(i_pi_max) = [];
                    pi_max_2 = max(pi_down_mu_alpha_u_k_up_b(i_mu,i_alpha,i_u,i_k,i_max));
                    if pi_max_2 / pi_max < param.pure_policy_tol
                        python_policy{i_mu,i_alpha,i_u,i_k} = se_param.K(i_pi_max);
                    else
                        python_policy{i_mu,i_alpha,i_u,i_k} = reshape(pi_down_mu_alpha_u_k_up_b(i_mu,i_alpha,i_u,i_k,1:i_k), 1, []);
                    end
                end
            end
        end
    end

    % Header
    header = ["mu", "alpha", "i-u", "u", "k", "policy"];
    filename = [fileprefix, '.csv'];
    fout = fopen(filename, 'w');
    for i = 1 : length(header) - 1
        fprintf(fout, '%s,', header(i));
    end
    fprintf(fout, '%s\n', header(end));
    fclose(fout);

    % Data
    for i_mu = 1 : param.n_mu
        for i_alpha = 1 : param.n_alpha
            if param.n_alpha == 1
                alpha = param.Alpha(i_alpha_comp);
            else
                alpha = param.Alpha(i_alpha);
            end
            for i_u = 1 : param.n_u
                u = param.U(i_u);
                for i_k = 1 : se_param.n_k
                    k = i_k - 1;
                    line = [i_mu, alpha, i_u - 1, u, k, python_policy{i_mu,i_alpha,i_u,i_k}];
                    dlmwrite(filename, line, '-append');
                end
            end
        end
    end
end