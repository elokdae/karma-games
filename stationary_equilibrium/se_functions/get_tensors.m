% Gets game tensors
function [prob_down_b_bj_up_o, zeta_down_u_o] = get_tensors(param, se_param)
    % Probability of winning/losing given bids
    prob_down_b_bj_up_o = zeros(se_param.n_k, se_param.n_k, se_param.n_o);
    for i_b = 1 : se_param.n_k
        b = se_param.K(i_b);
        for i_bj = 1 : se_param.n_k
            bj = se_param.K(i_bj);
            prob_down_b_bj_up_o(i_b,i_bj,1) = max([0, min([(b - bj + 1) / 2, 1])]);
            prob_down_b_bj_up_o(i_b,i_bj,2) = 1 - prob_down_b_bj_up_o(i_b,i_bj,1);
        end
    end

    % Expected stage cost tensor
    zeta_down_u_o = param.U * se_param.O.';
end
