clear;
clc;

%% Screen size used to place plots
screensize = get(groot, 'ScreenSize');
screenwidth = screensize(3);
screenheight = screensize(4);

%% Some parameters
file_str = 'results/n_a_200_T_100000_U_1_10_phi1_0.50_0.50_0.50_pay_0/k_bar_';

% Flag to save plots
save_plots = true;

%% Select k_bar(s) and alpha(s) to process
while true
    k_bar_vec = input('Enter k_bar(s): ');
    alpha_comp_vec = input('Enter alpha(s): ');
    
    for i_k_bar = 1 : length(k_bar_vec)
        k_bar = k_bar_vec(i_k_bar);
        
        for i_alpha_comp = 1 : length(alpha_comp_vec)
            alpha = alpha_comp_vec(i_alpha_comp);
            file = [file_str, num2str(k_bar, '%02d'), '_alpha_', num2str(abs(alpha), '%.2f'), '.mat'];
            if ~exist(file, 'file')
                file = [file_str, num2str(k_bar, '%02d'), '.mat'];
            end
            if alpha == -1
                load(file, 'k_bid_u', 'param');
                k = k_bid_u;
            else
                load(file, 'k_sw', 'K_sw', 'param');
                i_alpha_sim = find(param.alpha == alpha);
                if isempty(i_alpha_sim)
                    continue;
                end
                k = k_sw{i_alpha_sim};
            end
            
            k_min = min(k(:));
            k_max = max(k(:));
            K = k_min : k_max;
            num_K = length(K);
            k_dist = zeros(param.tot_num_inter, length(K));
            for t = 1 : param.tot_num_inter
                for i_k = 1 : num_K
                    k_dist(t,i_k) = length(find(k(t,:) == K(i_k))) / param.N;
                end
            end

            % Plot
            close all;
            karma_dist_plot_fg = 5;
            karma_dist_plot_pos = [0, screenheight / 2, screenwidth / 2, screenheight / 2];
            plot_karma_dist(karma_dist_plot_fg, karma_dist_plot_pos, k_dist, K, k_bar, alpha);
            drawnow;
            
            if save_plots
                if ~exist('results/karma_distributions', 'dir')
                    mkdir('results/karma_distributions');
                end
                saveas(karma_dist_plot_fg, ['results/karma_distributions/k_bar_', num2str(k_bar, '%02d'), '_alpha_', num2str(alpha, '%.2f'), '.png']);
                save(['results/karma_distributions/k_bar_', num2str(k_bar, '%02d'), '_alpha_', num2str(alpha, '%.2f'), '.mat'], 'k_dist');
            end
        end
    end
end