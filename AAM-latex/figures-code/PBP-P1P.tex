\begin{figure*}[bt]
	\centerfloat
	\subfloat{
		\tikzsetnextfilename{tikz-PBP-alpha-0.7-policy}
		\begin{tikzpicture}
		\begin{axis}[AxisStylePolicy,
			width = 0.37 \textwidth,
			title = {$\texttt{PBP}$, $\alpha=0.7$},
			xlabel = {},
			]
			\addplot3[surf, mesh/cols=22] table[x=k2, y=b2, z=P(b)] {data/PBP-alpha-0.7-policy.csv};
		\end{axis}
		\end{tikzpicture}
		\label{fig:PBP-alpha-0.7-policy}
	}
	\hfil
	\subfloat{
		\tikzsetnextfilename{tikz-PBP-alpha-0.95-policy}
		\begin{tikzpicture}
		\begin{axis}[AxisStylePolicy,
			width = 0.37 \textwidth,
			title = {$\texttt{PBP}$, $\alpha=0.95$},
			xlabel = {},
			ylabel = {},
			]
			\addplot3[surf, mesh/cols=22] table[x=k2, y=b2, z=P(b)] {data/PBP-alpha-0.95-policy.csv};
		\end{axis}
		\end{tikzpicture}
		\label{fig:PBP-alpha-0.95-policy}
	}
	\hfil
	\subfloat{
		\tikzsetnextfilename{tikz-PBP-alpha-0.99-policy}
		\begin{tikzpicture}
			\begin{axis}[AxisStylePolicy,
				width = 0.37 \textwidth,
				title = {$\texttt{PBP}$, $\alpha=0.99$},
				xlabel = {},
				ylabel = {},
				]
				\addplot3[surf, mesh/cols=22] table[x=k2, y=b2, z=P(b)] {data/PBP-alpha-0.99-policy.csv};
			\end{axis}
		\end{tikzpicture}
		\label{fig:PBP-alpha-0.99-policy}
	}

	\tikzsetnextfilename{tikz-PBP-alpha-0.7-karma-dist}
	\subfloat{
		\begin{tikzpicture}
		\begin{axis}[AxisStyleKarmaDist,
			width = 0.37 \textwidth,
			xticklabels={, 0, 10, $\quad \; \geq 20$},
			]
			\addplot table[x=k2, y=P(k)] {data/PBP-alpha-0.7-karma-dist.csv};
		\end{axis}
		\end{tikzpicture}
		\label{fig:PBP-alpha-0.7-karma-dist}
	}
	\hfil
	\tikzsetnextfilename{tikz-PBP-alpha-0.95-karma-dist}
	\subfloat{
		\begin{tikzpicture}
		\begin{axis}[AxisStyleKarmaDist,
		width = 0.37 \textwidth,
		ylabel = {},
		xticklabels={, 0, 10, $\quad \; \geq 20$},
		]
		\addplot table[x=k2, y=P(k)] {data/PBP-alpha-0.95-karma-dist.csv};
		\end{axis}
		\end{tikzpicture}
		\label{fig:PBP-alpha-0.95-karma-dist}
	}
	\hfil
	\tikzsetnextfilename{tikz-PBP-alpha-0.99-karma-dist}
	\subfloat{
		\begin{tikzpicture}
		\begin{axis}[AxisStyleKarmaDist,
		width = 0.37 \textwidth,
		ylabel = {},
		xticklabels={, 0, 10, $\quad \; \geq 20$},
		]
		\addplot table[x=k2, y=P(k)] {data/PBP-alpha-0.99-karma-dist.csv};
		\end{axis}
		\end{tikzpicture}
		\label{fig:PBP-alpha-0.99-karma-dist}
	}
	\medskip
	\hrule
	
	\subfloat{
		\tikzsetnextfilename{tikz-PBS-alpha-0.7-policy}
		\begin{tikzpicture}
		\begin{axis}[AxisStylePolicy,
		width = 0.37 \textwidth,
		title = {$\texttt{PBS}$, $\alpha=0.7$},
		xlabel = {},
		]
		\addplot3[surf, mesh/cols=22] table[x=k2, y=b2, z=P(b)] {data/PBS-alpha-0.7-policy.csv};
		\end{axis}
		\end{tikzpicture}
	}
	\hfil
	\subfloat{
		\tikzsetnextfilename{tikz-PBS-alpha-0.95-policy}
		\begin{tikzpicture}
		\begin{axis}[AxisStylePolicy,
		width = 0.37 \textwidth,
		title = {$\texttt{PBS}$, $\alpha=0.95$},
		xlabel = {},
		ylabel = {},
		]
		\addplot3[surf, mesh/cols=22] table[x=k2, y=b2, z=P(b)] {data/PBS-alpha-0.95-policy.csv};
		\end{axis}
		\end{tikzpicture}
	}
	\hfil
	\subfloat{
		\tikzsetnextfilename{tikz-PBS-alpha-0.99-policy}
		\begin{tikzpicture}
		\begin{axis}[AxisStylePolicy,
		width = 0.37 \textwidth,
		title = {$\texttt{PBS}$, $\alpha=0.99$},
		xlabel = {},
		ylabel = {},
		]
		\addplot3[surf, mesh/cols=22] table[x=k2, y=b2, z=P(b)] {data/PBS-alpha-0.99-policy.csv};
		\end{axis}
		\end{tikzpicture}
	}
	
	\tikzsetnextfilename{tikz-PBS-alpha-0.7-karma-dist}
	\subfloat{
		\begin{tikzpicture}
		\begin{axis}[AxisStyleKarmaDist,
		width = 0.37 \textwidth,
		xticklabels={, 0, 10, $\quad \; \geq 20$},
		]
		\addplot table[x=k2, y=P(k)] {data/PBS-alpha-0.7-karma-dist.csv};
		\end{axis}
		\end{tikzpicture}
	}
	\hfil
	\tikzsetnextfilename{tikz-PBS-alpha-0.95-karma-dist}
	\subfloat{
		\begin{tikzpicture}
		\begin{axis}[AxisStyleKarmaDist,
		width = 0.37 \textwidth,
		ylabel = {},
		xticklabels={, 0, 10, $\quad \; \geq 20$},
		]
		\addplot table[x=k2, y=P(k)] {data/PBS-alpha-0.95-karma-dist.csv};
		\end{axis}
		\end{tikzpicture}
	}
	\hfil
	\tikzsetnextfilename{tikz-PBS-alpha-0.99-karma-dist}
	\subfloat{
		\begin{tikzpicture}
		\begin{axis}[AxisStyleKarmaDist,
		width = 0.37 \textwidth,
		ylabel = {},
		xticklabels={, 0, 10, $\quad \; \geq 20$},
		]
		\addplot table[x=k2, y=P(k)] {data/PBS-alpha-0.99-karma-dist.csv};
		\end{axis}
		\end{tikzpicture}
	}
	\medskip
	\hrule
	
	\subfloat{
		\tikzsetnextfilename{tikz-P1P-alpha-0.7-policy}
		\begin{tikzpicture}
		\begin{axis}[AxisStylePolicy,
			width = 0.37 \textwidth,
			title = {$\texttt{P1P}$, $\alpha=0.7$},
			xlabel = {},
			]
			\addplot3[surf, mesh/cols=22] table[x=k2, y=b2, z=P(b)] {data/P1P-alpha-0.7-policy.csv};
		\end{axis}
		\end{tikzpicture}
		\label{fig:P1P-alpha-0.7-policy}
	}
	\hfil
	\subfloat{
		\tikzsetnextfilename{tikz-P1P-alpha-0.95-policy}
		\begin{tikzpicture}
		\begin{axis}[AxisStylePolicy,
			width = 0.37 \textwidth,
			title = {$\texttt{P1P}$, $\alpha=0.95$},
			xlabel = {},
			ylabel = {},
			]
			\addplot3[surf, mesh/cols=22] table[x=k2, y=b2, z=P(b)] {data/P1P-alpha-0.95-policy.csv};
		\end{axis}
		\end{tikzpicture}
		\label{fig:P1P-alpha-0.95-policy}
	}
	\hfil
	\subfloat{
		\tikzsetnextfilename{tikz-P1P-alpha-0.99-policy}
		\begin{tikzpicture}
		\begin{axis}[AxisStylePolicy,
			width = 0.37 \textwidth,
			title = {$\texttt{P1P}$, $\alpha=0.99$},
			xlabel = {},
			ylabel = {},
			]
			\addplot3[surf, mesh/cols=22] table[x=k2, y=b2, z=P(b)] {data/P1P-alpha-0.99-policy.csv};
		\end{axis}
		\end{tikzpicture}
		\label{fig:P1P-alpha-0.99-policy}
	}

	\tikzsetnextfilename{tikz-P1P-alpha-0.7-karma-dist}
	\subfloat{
		\begin{tikzpicture}
		\begin{axis}[AxisStyleKarmaDist,
		width = 0.37 \textwidth,
		xticklabels={, 0, 10, $\quad \; \geq 20$},
		]
		\addplot table[x=k2, y=P(k)] {data/P1P-alpha-0.7-karma-dist.csv};
		\end{axis}
		\end{tikzpicture}
		\label{fig:P1P-alpha-0.7-karma-dist}
	}
	\hfil
	\tikzsetnextfilename{tikz-P1P-alpha-0.95-karma-dist}
	\subfloat{
		\begin{tikzpicture}
		\begin{axis}[AxisStyleKarmaDist,
		width = 0.37 \textwidth,
		ylabel = {},
		xticklabels={, 0, 10, $\quad \; \geq 20$},
		]
		\addplot table[x=k2, y=P(k)] {data/P1P-alpha-0.95-karma-dist.csv};
		\end{axis}
		\end{tikzpicture}
		\label{fig:P1P-alpha-0.95-karma-dist}
	}
	\hfil
	\tikzsetnextfilename{tikz-P1P-alpha-0.99-karma-dist}
	\subfloat{
		\begin{tikzpicture}
		\begin{axis}[AxisStyleKarmaDist,
		width = 0.37 \textwidth,
		ylabel = {},
		xticklabels={, 0, 10, $\quad \; \geq 20$},
		]
		\addplot table[x=k2, y=P(k)] {data/P1P-alpha-0.99-karma-dist.csv};
		\end{axis}
		\end{tikzpicture}
		\label{fig:P1P-alpha-0.99-karma-dist}
	}
\end{figure*}