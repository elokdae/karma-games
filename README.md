# Dependencies

## simulation routine

None

## stationary_equilibrium routine

einsum v1.0.0 - https://www.mathworks.com/matlabcentral/fileexchange/69479-einsum

## social_welfare routine (obsolete)

Dependencies for stationary_equilibrium routine plus:

YALMIP v16-January-2020 - https://yalmip.github.io/

OPTI Toolbox v2.28 - https://inverseproblem.co.nz/OPTI/


# Instructions

## Running a simulation

1. Select parameters in load_parameters.m

	1.1. Multiple alpha values can be specified as a row vector
	
	1.2. Only one k_bar value can be specified
	
2. Run simulation.m

Note: simulation.m uses stationary equilibrium and/or social welfare computation results stored in stationary_equilibrium/results folder. An error might occur if a parameter set is used for which SE and/or SW computation has not been performed

## Computing stationary equilibria

1. Add stationary_equilibrium folder to Matlab path
2. Select high level parameters in load_parameters.m

	2.1. Multiple alpha values can be specified as a row vector. It is recommended to specify in descending order
	
	2.2. Only one k_bar value can be specified
	
3. Select SE computation parameters in load_se_parameters.m
4. Run stationary_equilibrium.m
5. Once computation is complete, move newly generated .mat files in stationary_equilibrium/results to correct subfolder

## Computing social welfare policies (obsolete)

1. Add stationary_equilibrium folder to Matlab path
2. Select high level parameters in load_parameters.m

	2.1 alpha is irrelevant
	
	2.2 Multiple k_bar values can be specified as a vector. It is recommended to specify in ascending order
	
3. Select SW computation parameters in load_se_parameters.m
4. Run social_welfare.m
5. Once computation is complete, move newly generated .mat file in stationary_equilibrium/results to correct subfolder

Note: It takes very long to set up optimization problem for large k_bar, before optimization solver starts output text. Be patient

# Conventions

## Tensors

It is relied on multi-dimensional arrays, referred to as tensors. The variable naming convention captures what the dimensions correspond to. There are two types of tensors:

1. Functional tensors: Represent functions of the subsript quantities (denoted by `down`).

	1.1 Example: `zeta_down_u_b_bj` gives expected stage cost as a function of urgency `u`, bid `b`, and opponent's bid `bj`. This is a 3-dimensional tensor with `u` in dimension 1, `b` in dimension 2, and `bj` in dimension 3.

2. Probability tensors: Represent (joint) probabilities if they have superscript quantities only (denoted by `up`) and conditional probabilities if they have both sub- and superscript quantities

	2.1 Example: `se_d_up_mu_alpha_u_k` gives stationary equilibrium joint type-state distribution. This is a 4-dimensional tensor with `mu` in dimension 1, `alpha` in dimension 2, `u` in dimension 3, and `k` in dimension 4.

	2.2 Example: `se_pi_down_mu_alpha_u_k_up_b` gives stationary equilibrium policy, which is the probability of playing bid `b` given type-state (`mu,alpha,u,k`). This is a 5-dimensional tensor with (`mu,alpha,u,k`) in the first 4 dimensions (the conditional quantities) and `b` in the 5th dimension.

## einsum

It is relied on the einsum function to perform efficient computations on tensors. See the documentation for more details on the syntax. For example:

```
se_nu_up_bj = einsum('ijklm,ijkl->m', se_pi_down_mu_alpha_u_k_up_b, se_d_up_mu_alpha_u_k);
```

Here, in the first string argument, `i` corresponds to dimension `mu`, `j` to dimension `alpha`, `k` to dimension `u`, `l` to dimension `k`, and `m` to dimension `b`. The string `ijklm,ijkl->m` will 'eliminate' dimensions `ijkl` with an appropriate weighted sum of the argument tensors to get a tensor of dimension `m`. In this context, this will get the marginal distribution of the opponents' bids `bj` from the conditional distribution of bids `se_pi_down_mu_alpha_u_k_up_b` (conditional on the type-state) and the distribution of type-states `se_d_up_mu_alpha_u_k`. An equivalent for-loop implementation would be:

```
se_nu_up_bj = zeros(n_bj,1)
for i_mu = 1 : n_mu
  for i_alpha = 1 : n_alpha
    for i_u = 1 : n_u
      for i_k = 1 : n_k
        se_nu_up_bj = se_nu_up_bj + se_pi_down_mu_alpha_u_k_up_b(i_mu,i_alpha,i_u,i_k,:) * se_d_up_mu_alpha_u_k(i_mu,i_alpha,i_u,i_k);
      end
    end
  end
end
```
