clear;
close all;
clc;

%% Add functions folder to path
addpath('functions');

%% Code control bits
% Flag to simulate fairness horizon policies
control.fairness_horizon_policies = false;

% Flag to simulate highest karma policy
control.highest_karma = true;

% Flag to simulate stationary equilibrium policies
control.se_policies = true;

% Flag to simulate social welfare policy
control.sw_policy = false;

%% Parameters
param = load_parameters();

%% Run simulation for each seed
for i_seed = 1 : param.n_seed
    seed = param.Seed(i_seed);
    if param.n_seed > 1
        fprintf('%%%%SEED = %d%%%%\n\n', seed);
    end
    
    %% Control randomization
    [u_stream, agent_stream, karma_stream] = RandStream.create('mrg32k3a', 'NumStreams', 3, 'Seed', seed);

    %% Simulation initialization
    % Population of agent indices to sample from
    agents = 1 : param.n_agents;

    %% Types of the agents
    agents_to_sample = agents;
    agents_per_type = cell(param.n_mu, param.n_alpha);
    agents_per_mu = cell(param.n_mu, 1);
    n_a_per_type = zeros(param.n_mu, param.n_alpha);
    n_a_per_mu = zeros(param.n_mu, 1);
    agent_types = zeros(2, param.n_agents);  % First row is urgency type, second is future awareness type
    for i_mu = 1 : param.n_mu
        agents_per_mu{i_mu} = [];
        for i_alpha = 1 : param.n_alpha
            if i_mu == param.n_mu && i_alpha == param.n_alpha
                n_a_per_type(i_mu,i_alpha) = param.n_agents - sum(n_a_per_type(:));
            else
                n_a_per_type(i_mu,i_alpha) = round(param.g_up_mu_alpha(i_mu,i_alpha) * param.n_agents);
            end
            [agents_per_type{i_mu,i_alpha}, i_agents] = datasample(agent_stream, agents_to_sample, n_a_per_type(i_mu,i_alpha), 'Replace', false);
            agents_per_mu{i_mu} = [agents_per_mu{i_mu}, agents_per_type{i_mu,i_alpha}];
            agents_to_sample(i_agents) = [];
            agent_types(:,agents_per_type{i_mu,i_alpha}) = repmat([i_mu; i_alpha], 1, n_a_per_type(i_mu,i_alpha));
        end
    end
    n_a_per_mu = sum(n_a_per_type, 2);

    %% Urgency initialization
    u_hist = allocate_mat(param);
    i_u_last = zeros(1, param.n_agents); % Use indices for speed
    for i_mu = 1 : param.n_mu
        % Initialize as per stationary distribution of Markov chain
        start_i = 0;
        for i_u = 1 : param.n_u - 1
            n_a_per_u = round(param.prob_down_mu_up_u(i_mu,i_u) * n_a_per_mu(i_mu));
            i_agents = agents_per_mu{i_mu}(start_i+1:start_i+n_a_per_u);
            i_u_last(i_agents) = i_u;
            start_i = start_i + n_a_per_u;
        end
        i_agents = agents_per_mu{i_mu}(start_i+1:end);
        i_u_last(i_agents) = param.n_u;
    end

    %% Cost matrices for different policies
    % Row => Timestep
    % Col => Agent

    % Benchmark policies
    % Cost for random
    c_rand = allocate_mat(param);
    % Cost for centralized urgency
    c_u = allocate_mat(param);
    % Cost for centralized accumulated cost
    c_a = allocate_mat(param);
    % Cost for centralized urgency then accumulated cost
    c_u_a = allocate_mat(param);
    % Cost for centralized accumulated pass
    c_apass = allocate_mat(param);

    % Centralized fairness horizon policies
    if control.fairness_horizon_policies
        c_fair_hor_a = cell(param.n_fairness_horizon, 1);
        c_fair_hor_u_a = cell(param.n_fairness_horizon, 1);
        for i_fair_hor = 1 : param.n_fairness_horizon
            c_fair_hor_a{i_fair_hor} = allocate_mat(param);
            c_fair_hor_u_a{i_fair_hor} = allocate_mat(param);
        end
    end

    % Highest karma policy
    if control.highest_karma
        c_highest_karma = allocate_mat(param);
    end

    % Stationary equilibrium policies
    if control.se_policies
        c_se = cell(param.n_alpha_comp, 1);
        for i_alpha_comp = 1 : param.n_alpha_comp
            c_se{i_alpha_comp} = allocate_mat(param);
        end
    end

    % Social welfare policy
    if control.sw_policy
        c_sw = allocate_mat(param);

        if control.capital_tax
            c_sw_ctax = c_sw;
        end
    end

    %% Who passed?
    % Row => Timestep
    % Col => Agent

    % Benchmark policies
    % # of pass for random
    pass_rand = allocate_mat(param);
    % # of pass for centralized urgency
    pass_u = allocate_mat(param);
    % # of pass for centralized accumulated cost
    pass_a = allocate_mat(param);
    % # of pass for centralized urgency then accumulated cost
    pass_u_a = allocate_mat(param);
    % # of pass for centralized accumulated pass
    pass_apass = allocate_mat(param);

    % Centralized fairness horizon policies
    if control.fairness_horizon_policies
        pass_fair_hor_a = cell(param.n_fairness_horizon, 1);
        pass_fair_hor_u_a = cell(param.n_fairness_horizon, 1);
        for i_fair_hor = 1 : param.n_fairness_horizon
            pass_fair_hor_a{i_fair_hor} = allocate_mat(param);
            pass_fair_hor_u_a{i_fair_hor} = allocate_mat(param);
        end
    end

    % Highest karma policy
    if control.highest_karma
        pass_highest_karma = allocate_mat(param);
    end

    % Stationary equilibrium policies
    if control.se_policies
        pass_se = cell(param.n_alpha_comp, 1);
        for i_alpha_comp = 1 : param.n_alpha_comp
            pass_se{i_alpha_comp} = allocate_mat(param);
        end
    end

    % Social welfare policy
    if control.sw_policy
        pass_sw = allocate_mat(param);

        if control.capital_tax
            pass_sw_ctax = pass_sw;
        end
    end

    %% Karma and policy matrices for karma policies
    % Initial karma, for cases when it is common amongst multiple policies
    if control.highest_karma || (control.se_policies || control.sw_policy) && param.karma_initialization ~= 2
        if param.karma_initialization == 0
            init_k = param.k_bar * ones(1, param.n_agents);
        else
            [sigma_up_k_uniform, K_uniform] = get_sigma_up_k_uniform_inf(param.k_bar);
            init_k = get_init_k(sigma_up_k_uniform, K_uniform, param);
        end
    end

    % Highest karma policy
    if control.highest_karma
        k_highest_karma = allocate_karma(param, init_k);
    end

    % Stationary equilibrium policies and distributions
    if control.se_policies
        K_se = cell(param.n_alpha_comp, 1);
        pi_se = cell(param.n_alpha_comp, 1);
        pi_se_pure = cell(param.n_alpha_comp, 1);
        k_se_dist_pred = cell(param.n_alpha_comp, 1);
        k_se = cell(param.n_alpha_comp, 1);
        surplus_k = zeros(param.n_alpha_comp, 1);
        t_since_redist = zeros(1, param.n_agents);

        se_file_str = 'stationary_equilibrium/results/se_U_';
        for i_u = 1 : param.n_u
            se_file_str = [se_file_str, int2str(param.U(i_u)), '_'];
        end
        for i_mu = 1 : param.n_mu
            se_file_str = [se_file_str, 'phi', int2str(i_mu), '_'];
            for i_u = 1 : param.n_u
                for i_un = 1 : param.n_u
                    se_file_str = [se_file_str, num2str(param.phi_down_mu_u_up_un(i_mu,i_u,i_un), '%.2f'), '_'];
                end
            end
        end
        if param.n_alpha > 1
            for i_alpha = 1 : param.n_alpha
                alpha = param.Alpha(i_alpha);
                if alpha > 0.99 && alpha < 1
                    alpha_str = num2str(alpha, '%.3f');
                else
                    alpha_str = num2str(alpha, '%.2f');
                end
                se_file_str = [se_file_str, 'alpha', int2str(i_alpha), '_', alpha_str, '_', num2str(param.z_up_alpha(i_alpha), '%.2f'), '_'];
            end
        end
        se_file_str = [se_file_str, 'pay_', int2str(param.transition_mechanism)];
        if param.tax > 0.00
            se_file_str = [se_file_str, '_tax_', num2str(param.tax, '%.3f')];
        end
        se_file_str = [se_file_str, '/k_bar_', num2str(param.k_bar, '%02d')];
        if param.n_alpha == 1
            se_file_str = [se_file_str, '_alpha'];
            for i_alpha_comp = 1 : param.n_alpha_comp
                alpha = param.Alpha(i_alpha_comp);
                % alpha = 0 yields bid all and all of the karma with one agent
                % under pay as bid rule
                if alpha == 0 && (param.transition_mechanism == 0 || param.transition_mechanism == 4)
                    K_se{i_alpha_comp} = (0 : param.k_tot).';
                    pi_se_pure{i_alpha_comp} = reshape(repmat(K_se{i_alpha_comp}.', param.n_mu * param.n_alpha * param.n_u, 1), param.n_mu, param.n_alpha, param.n_u, []);
                    k_se_dist_pred{i_alpha_comp} = zeros(param.k_tot + 1, 1);
                    k_se_dist_pred{i_alpha_comp}(end) = 1;
                    if param.karma_initialization == 2
                        k_se{i_alpha_comp} = allocate_mat(param);
                        k_se{i_alpha_comp}(1,:) = [zeros(1, param.n_agents - 1), param.k_tot];
                    else
                        k_se{i_alpha_comp} = allocate_karma(param, init_k);
                    end
                    continue;
                end

                if alpha > 0.99 && alpha < 1
                    alpha_str = num2str(alpha, '%.3f');
                else
                    alpha_str = num2str(alpha, '%.2f');
                end
                se_file = [se_file_str, '_', alpha_str, '.mat'];

                load(se_file, 'se_param', 'se_pi_down_mu_alpha_u_k_up_b', 'se_sigma_up_k');
                K_se{i_alpha_comp} = se_param.K;
                pi_se{i_alpha_comp} = se_pi_down_mu_alpha_u_k_up_b;
                pi_se_pure{i_alpha_comp} = get_pure_policy(pi_se{i_alpha_comp}, K_se{i_alpha_comp}, param);
                k_se_dist_pred{i_alpha_comp} = se_sigma_up_k;
                
                if param.karma_initialization == 2
                    % Initialize karma as per stationary distribution predicted by SE
                    % algorithm
                    load(se_file, 'se_sigma_up_k');
                    se_init_k = get_init_k(se_sigma_up_k, K_se{i_alpha_comp}, param);
                else
                    se_init_k = init_k;
                end
                k_se{i_alpha_comp} = allocate_karma(param, se_init_k);
            end
        else
            se_file = [se_file_str, '.mat'];

            load(se_file, 'se_param', 'se_pi_down_mu_alpha_u_k_up_b');
            K_se{1} = se_param.K;
            pi_se{1} = se_pi_down_mu_alpha_u_k_up_b;
            pi_se_pure{1} = get_pure_policy(pi_se{1}, K_se{1}, param);

            if param.karma_initialization == 2
                % Initialize karma as per stationary distribution predicted by SE
                % algorithm
                load(se_file, 'se_sigma_up_k');
                se_init_k = get_init_k(se_sigma_up_k, K_se{1}, param);
            else
                se_init_k = init_k;
            end
            k_se{1} = allocate_karma(param, se_init_k);
        end
    end

    % Social welfare karma policy distribution
    if control.sw_policy
        sw_file = 'stationary_equilibrium/results/sw_U_';
        for i_u = 1 : param.n_u
            sw_file = [sw_file, num2str(param.U(i_u)), '_'];
        end
        for i_mu = 1 : param.n_mu
        sw_file = [sw_file, 'phi', int2str(i_mu), '_'];
            for i_u = 1 : param.n_u
                for i_un = 1 : param.n_u
                    sw_file = [sw_file, num2str(param.phi_down_mu_u_up_un(i_mu,i_u,i_un), '%.2f'), '_'];
                end
            end
        end
        sw_file = [sw_file, 'pay_', int2str(param.payment_rule),...
            '/k_bar_', num2str(param.k_bar, '%02d'), '.mat'];

        load(sw_file, 'se_param', 'sw_pi_down_u_k_up_b');
        K_sw = se_param.K;
        pi_sw = sw_pi_down_u_k_up_b;
        pi_sw_pure = get_pure_policy(pi_sw, K_sw, param);

        if param.karma_initialization == 2
            % Initialize karma as per stationary distribution predicted by SW
            % algorithm
            load(sw_file, 'sw_sigma_up_k');
            sw_init_k = get_init_k(sw_sigma_up_k, K_sw, param);
        else
            sw_init_k = init_k;
        end
        k_sw = allocate_karma(param, sw_init_k);

        if control.capital_tax
            k_sw_ctax = k_sw;
        end
    end

    %% Hisotry of payments for karma policies
    if control.highest_karma
        p_highest_karma = nan(param.T, 1);
    end

    if control.se_policies
        p_se = nan(param.T, param.n_alpha_comp);
    end
    if control.sw_policy
        p_sw = nan(param.T, 1);

        if control.capital_tax
            p_sw_ctax = p_sw;
        end
    end

    %% Number of times each agent was in an interaction
    t_i = zeros(1, param.n_agents);

    %% Some memory allocations for the loop
    this_u = zeros(1, 2);
    this_i_u = zeros(1, 2);
    this_k = zeros(1, 2);
    this_b = zeros(1, 2);
    this_tax = zeros(1, 2);

    %% Simulation run
    agents_to_sample = agents;
    for t = 1 : param.T
        % Tell user where we are
        fprintf('Timestep: %d\n', t);
        
        % Sample agents i & j uniformly at random
        if isempty(agents_to_sample)
            agents_to_sample = agents;
        end
        if param.matching == 2
            [this_agents, i_agents] = datasample(agent_stream, agents_to_sample, 2, 'Replace', false, 'Weights', param.matching_weights);
        else
            [this_agents, i_agents] = datasample(agent_stream, agents_to_sample, 2, 'Replace', false);
        end
        if param.matching == 1
            agents_to_sample(i_agents) = [];
        end
        
        % Increment number of interactions for picked agents
        this_t_i = t_i(this_agents) + 1;
        t_i(this_agents) = this_t_i;
        t_since_redist(this_agents) = t_since_redist(this_agents) + 1;

        % Assert if an agent is picked too many times for memory allocated
        assert(max(this_t_i) < param.max_T_i,...
            'One agent was picked too many times. Increase maximum allowed number of interactions per agent.');

        % Urgency of sampled agents
        this_mu = agent_types(1,this_agents);
        this_i_u_last = i_u_last(this_agents);
        % See if we can sample urgency of both agents from same prob
        % distribution. This is faster than sampling per agent
        if this_mu(1) == this_mu(2) && (param.u_iid(this_mu(1)) || this_i_u_last(1) == this_i_u_last(2))
            this_i_u = datasample(u_stream, param.i_U, 2, 'Weights', squeeze(param.phi_down_mu_u_up_un(this_mu(1),this_i_u_last(1),:)));
        else
            for i_agent = 1 : 2
                this_i_u(i_agent) = datasample(u_stream, param.i_U, 1, 'Weights', squeeze(param.phi_down_mu_u_up_un(this_mu(i_agent),this_i_u_last(i_agent),:)));
            end
        end
    %     % Hack.com
    %     this_i_u = 3 - this_i_u_last;
    %     % End hack.com
        i_u_last(this_agents) = this_i_u;

        % Future awareness of sampled agents
        this_i_alpha = agent_types(2,this_agents);

        % Update history of urgency
        for i_agent = 1 : 2
            this_u(i_agent) = param.U(this_i_u(i_agent));
            u_hist(this_t_i(i_agent),this_agents(i_agent)) = this_u(i_agent);
        end


        %% RANDOM POLICY %%
        % Simply choose 'first' agent, since there is already randomization
        % in the agent order
        i_win = 1;

        % Update cost. Winning agent incurs 0 cost. Losing agent incurs
        % cost equal to their urgency
        for i_agent = 1 : 2
            if i_agent == i_win
                c_rand(this_t_i(i_agent),this_agents(i_agent)) = 0;
                pass_rand(this_t_i(i_agent),this_agents(i_agent)) = 1;
            else
                c_rand(this_t_i(i_agent),this_agents(i_agent)) = this_u(i_agent);
                pass_rand(this_t_i(i_agent),this_agents(i_agent)) = 0;
            end
        end

        %% CENTRALIZED POLICIES %%
        %% Centralized urgency policy
        % Agent with max urgency passes. Coin flip on tie
        % Find agent(s) with max urgency, which are candidates for passing
        [~, i_max_u] = multi_maxes(this_i_u);
        num_max_u = length(i_max_u);

        % Choose 'first' urgent agent (there is already randomization in
        % the agent order)
        i_win = i_max_u(1);

        % Update cost. Winning agent incurs 0 cost. Losing agent incurs
        % cost equal to their urgency
        for i_agent = 1 : 2
            if i_agent == i_win
                c_u(this_t_i(i_agent),this_agents(i_agent)) = 0;
                pass_u(this_t_i(i_agent),this_agents(i_agent)) = 1;
            else
                c_u(this_t_i(i_agent),this_agents(i_agent)) = this_u(i_agent);
                pass_u(this_t_i(i_agent),this_agents(i_agent)) = 0;
            end
        end

        %% Centralized accumulated cost policy
        % Agent with maximum accumulated cost (counting current urgency)
        % passes. Coin flip on tie
        a = accumulate_cost(c_a, this_agents, this_u, t_i, inf);

        [~, i_win] = max(a);

        % Update cost. Winning agent incurs 0 cost. Losing agent incurs
        % cost equal to their urgency
        for i_agent = 1 : 2
            if i_agent == i_win
                c_a(this_t_i(i_agent),this_agents(i_agent)) = 0;
                pass_a(this_t_i(i_agent),this_agents(i_agent)) = 1;
            else
                c_a(this_t_i(i_agent),this_agents(i_agent)) = this_u(i_agent);
                pass_a(this_t_i(i_agent),this_agents(i_agent)) = 0;
            end
        end

        %% Centralized urgency then accumulated cost policy
        % Agents with max urgency, which are candidates for passing, were
        % already found in first step of centralized urgency policy
        % If there are multiple agents with max urgency, pick one based on
        % accumulated cost like in centralized accumulated cost policy
        if num_max_u > 1
            a = accumulate_cost(c_u_a, this_agents, this_u, t_i, inf);
            [~, i_win] = max(a);
        else
            i_win = i_max_u;
        end

        % Update cost. Winning agent incurs 0 cost. Losing agent incurs
        % cost equal to their urgency
        for i_agent = 1 : 2
            if i_agent == i_win
                c_u_a(this_t_i(i_agent),this_agents(i_agent)) = 0;
                pass_u_a(this_t_i(i_agent),this_agents(i_agent)) = 1;
            else
                c_u_a(this_t_i(i_agent),this_agents(i_agent)) = this_u(i_agent);
                pass_u_a(this_t_i(i_agent),this_agents(i_agent)) = 0;
            end
        end
        
        %% Centralized accumulated pass policy
        % Agent with minimum accumulated # of pass passes. Coin flip on tie
        apass = accumulate_pass(pass_apass, this_agents, t_i, inf);

        [~, i_win] = min(apass);

        % Update cost. Winning agent incurs 0 cost. Losing agent incurs
        % cost equal to their urgency
        for i_agent = 1 : 2
            if i_agent == i_win
                c_apass(this_t_i(i_agent),this_agents(i_agent)) = 0;
                pass_apass(this_t_i(i_agent),this_agents(i_agent)) = 1;
            else
                c_apass(this_t_i(i_agent),this_agents(i_agent)) = this_u(i_agent);
                pass_apass(this_t_i(i_agent),this_agents(i_agent)) = 0;
            end
        end
        
        %% Fairness horizon policies
        if control.fairness_horizon_policies
            for i_fair_hor = 1 : param.n_fairness_horizon
                %% Centralized accumulated cost with fairness horizon
                % Agent with maximum accumulated cost up to fairness horizon
                % (counting current urgency) passes
                % Coin flip on tie
                a = accumulate_cost(c_fair_hor_a{i_fair_hor}, this_agents, this_u, t_i, param.fairness_horizon(i_fair_hor));
                [~, i_win] = max(a);

                % Update cost. Winning agent incurs 0 cost. Losing agent
                % incurs cost equal to their urgency
                for i_agent = 1 : 2
                    if i_agent == i_win
                        c_fair_hor_a{i_fair_hor}(this_t_i(i_agent),this_agents(i_agent)) = 0;
                        pass_fair_hor_a{i_fair_hor}(this_t_i(i_agent),this_agents(i_agent)) = 1;
                    else
                        c_fair_hor_a{i_fair_hor}(this_t_i(i_agent),this_agents(i_agent)) = this_u(i_agent);
                        pass_fair_hor_a{i_fair_hor}(this_t_i(i_agent),this_agents(i_agent)) = 0;
                    end
                end

                %% Centralized urgency then accumulated cost with fairness horizon
                % Agents with max urgency, which are candidates for passing,
                % were already found in first step of centralized urgency policy
                % If there are multiple agents with max urgency, pick one based
                % on accumulated cost up to fairness horizon
                if num_max_u > 1
                    a = accumulate_cost(c_fair_hor_u_a{i_fair_hor}, this_agents, this_u, t_i, param.fairness_horizon(i_fair_hor));
                    [~, i_win] = max(a);
                else
                    i_win = i_max_u;
                end

                % Update cost. Winning agent incurs 0 cost. Losing agent
                % incurs cost equal to their urgency
                for i_agent = 1 : 2
                    if i_agent == i_win
                        c_fair_hor_u_a{i_fair_hor}(this_t_i(i_agent),this_agents(i_agent)) = 0;
                        pass_fair_hor_u_a{i_fair_hor}(this_t_i(i_agent),this_agents(i_agent)) = 1;
                    else
                        c_fair_hor_u_a{i_fair_hor}(this_t_i(i_agent),this_agents(i_agent)) = this_u(i_agent);
                        pass_fair_hor_u_a{i_fair_hor}(this_t_i(i_agent),this_agents(i_agent)) = 0;
                    end
                end
            end
        end

        %% KARMA POLICIES %%
        % Save state of random stream to revert to before randomization of
        % the different policies. This ensures 'ties' are handled the same
        karma_stream_state = karma_stream.State;

        %% Highest karma policy
        if control.highest_karma
            for i_agent = 1 : 2
                this_k(i_agent) = k_highest_karma(this_t_i(i_agent),this_agents(i_agent));
            end
            [~, i_win] = max(this_k);
            p = min([this_k(i_win), 1]);
            p_highest_karma(t) = p;
            for i_agent = 1 : 2
                if i_agent == i_win
                    c_highest_karma(this_t_i(i_agent),this_agents(i_agent)) = 0;
                    pass_highest_karma(this_t_i(i_agent),this_agents(i_agent)) = 1;
                    k_highest_karma(this_t_i(i_agent)+1,this_agents(i_agent)) = this_k(i_agent) - p;
                else
                    c_highest_karma(this_t_i(i_agent),this_agents(i_agent)) = this_u(i_agent);
                    pass_highest_karma(this_t_i(i_agent),this_agents(i_agent)) = 0;
                    k_highest_karma(this_t_i(i_agent)+1,this_agents(i_agent)) = this_k(i_agent) + p;
                end
            end
        end

        %% Stationary Nash equilibrium policies
        if control.se_policies
            % Loop over future awareness
            for i_alpha_comp = 1 : param.n_alpha_comp
                % Get agents' bids from their policies
                for i_agent = 1 : 2
                    this_k(i_agent) = k_se{i_alpha_comp}(this_t_i(i_agent),this_agents(i_agent));
                    i_k = min([floor(this_k(i_agent)) + 1, length(K_se{i_alpha_comp})]); % Uses policy of k_max if it is exceeded
                    this_b(i_agent) = pi_se_pure{i_alpha_comp}(this_mu(i_agent),this_i_alpha(i_agent),this_i_u(i_agent),i_k);
                    if isnan(this_b(i_agent))
                        karma_stream.State = karma_stream_state; % Reset karma stream to current state for consitency across policies
                        this_b(i_agent) = datasample(karma_stream, K_se{i_alpha_comp}, 1, 'Weights', squeeze(pi_se{i_alpha_comp}(this_mu(i_agent),this_i_alpha(i_agent),this_i_u(i_agent),i_k,:)));
                    end
                end

                % Agent bidding max karma passes and pays karma bidded
                [b_win, i_win] = max(this_b);
                i_lose = 3 - i_win;
                b_lose = this_b(i_lose);

                switch param.sim_transition_mechanism
                    case {0, 1} % Pay as bid to peer, pay as bid to society
                        p = b_win;
                    case {2, 4, 10, 11}    % Pay 2nd price to society, pay other's bid to peer, loser pays as bid to peer, loser pays as bid to society
                        p = b_lose;
                    case 3  % Pay 1 to peer
                        p = min([1, b_win]);
                end
                p_se(t,i_alpha_comp) = p;

                % Update cost and karma. Winning agent incurs 0 cost and pays
                % karma. Losing agent incurs cost equal to their urgency and
                % gets karma
                for i_agent = 1 : 2
                    if i_agent == i_win
                        c_se{i_alpha_comp}(this_t_i(i_agent),this_agents(i_agent)) = 0;
                        pass_se{i_alpha_comp}(this_t_i(i_agent),this_agents(i_agent)) = 1;
                        switch param.sim_transition_mechanism
                            case {0, 1, 2, 3, 4}  % Pay as bid to peer, pay as bid to society, pay 2nd price to society, pay 1 to peer, pay other's bid to peer
                                k_se{i_alpha_comp}(this_t_i(i_agent)+1,this_agents(i_agent)) = this_k(i_agent) - p;
                            case 10 % Loser pays as bid to peer
                                k_se{i_alpha_comp}(this_t_i(i_agent)+1,this_agents(i_agent)) = this_k(i_agent) + p;
                            case 11 % Loser pays as bid to society
                                k_se{i_alpha_comp}(this_t_i(i_agent)+1,this_agents(i_agent)) = this_k(i_agent);
                        end
                    else
                        c_se{i_alpha_comp}(this_t_i(i_agent),this_agents(i_agent)) = this_u(i_agent);
                        pass_se{i_alpha_comp}(this_t_i(i_agent),this_agents(i_agent)) = 0;
                        switch param.sim_transition_mechanism
                            case {0, 3, 4}  % Pay as bid to peer, pay 1 to peer, pay other's bid to peer
                                k_se{i_alpha_comp}(this_t_i(i_agent)+1,this_agents(i_agent)) = this_k(i_agent) + p;
                            case {1, 2} % Pay as bid to society, pay 2nd price to society
                                k_se{i_alpha_comp}(this_t_i(i_agent)+1,this_agents(i_agent)) = this_k(i_agent);
                            case {10, 11}   % Loser pays as bid to peer, loser pays as bid to society
                                k_se{i_alpha_comp}(this_t_i(i_agent)+1,this_agents(i_agent)) = this_k(i_agent) - p;
                        end
                    end
                end

                % Payment to surplus karma
                switch param.sim_transition_mechanism
                    case {1, 2, 11} % Pay as bid to society, pay 2nd price to society, loser pays as bid to society
%                         redist = p / param.n_agents;
%                         k_se{i_alpha_comp}(redist_ind) =  k_se{i_alpha_comp}(redist_ind) + redist;
                        
%                         for i_agent = 1 : param.n_agents
%                             k_se{i_alpha_comp}(t_i(i_agent)+1,i_agent) =  k_se{i_alpha_comp}(t_i(i_agent)+1,i_agent) + redist;
%                         end

                        surplus_k(i_alpha_comp) = surplus_k(i_alpha_comp) + p;
                end

                % Apply tax
                % Tax is taken from participating agents at the end
                % of the interaction, and then re-ditributed to all agents
                if param.tax > 0.0
                    for i_agent = 1 : 2
                        this_tax(i_agent) = param.tax * k_se{i_alpha_comp}(this_t_i(i_agent)+1,this_agents(i_agent))^2;
                        k_se{i_alpha_comp}(this_t_i(i_agent)+1,this_agents(i_agent)) = k_se{i_alpha_comp}(this_t_i(i_agent)+1,this_agents(i_agent)) - this_tax(i_agent);
                    end
%                     redist = sum(this_tax) / param.n_agents;
%                     k_se{i_alpha_comp}(redist_ind) =  k_se{i_alpha_comp}(redist_ind) + redist;
                    
%                     for i_agent = 1 : param.n_agents
%                         k_se{i_alpha_comp}(t_i(i_agent)+1,i_agent) =  k_se{i_alpha_comp}(t_i(i_agent)+1,i_agent) + redist;
%                     end
                    
                    surplus_k(i_alpha_comp) = surplus_k(i_alpha_comp) + sum(this_tax);
                end
            end
            
            % Perform karma redistribution
            if mod(t, param.n_agents / 2) == 0
                if param.interaction_weighted_redistribution
                    redist = surplus_k * t_since_redist / sum(t_since_redist);
                else
                    redist = surplus_k / param.n_agents;
                end
                redist_ind = sub2ind([param.max_T_i, param.n_agents], t_i+1, agents);
                for i_alpha_comp = 1 : param.n_alpha_comp
                    k_se{i_alpha_comp}(redist_ind) =  k_se{i_alpha_comp}(redist_ind) + redist(i_alpha_comp,:);
                end
                surplus_k = zeros(param.n_alpha_comp, 1);
                t_since_redist = zeros(1, param.n_agents);
            end
        end

        %% Social welfare karma policy
        if control.sw_policy
            % Get agents' bids from their policies
            for i_agent = 1 : 2
                this_k(i_agent) = k_sw(this_t_i(i_agent),this_agents(i_agent));
                i_k = min([this_k(i_agent) + 1, length(K_sw)]); % Uses policy of k_max if it is exceeded
                this_b(i_agent) = pi_sw_pure(this_i_u(i_agent),i_k);
                if isnan(this_b(i_agent))
                    karma_stream.State = karma_stream_state; % Reset karma stream to current state for consitency across policies
                    this_b(i_agent) = datasample(karma_stream, K_sw, 1, 'Weights', squeeze(pi_sw(this_i_u(i_agent),i_k,:)));
                end
            end

            % Agent bidding max karma passes and pays karma bidded
            [b_win, i_win] = max(this_b);

            switch param.sim_payment_rule
                case 0
                    % Pay as bid
                    p = b_win;
                case 1
                    % Pay difference
                    b_lose = min(this_b);
                    p = b_win - b_lose;
                case 2
                    % Pay 1
                    p = min([1, b_win]);
            end
            p_sw(t) = p;

            % Update cost and karma. Winning agent incurs 0 cost and pays
            % karma. Losing agent incurs cost equal to their urgency and gets
            % karma
            for i_agent = 1 : 2
                if i_agent == i_win
                    c_sw(this_t_i(i_agent),this_agents(i_agent)) = 0;
                    pass_sw(this_t_i(i_agent),this_agents(i_agent)) = 1;
                    k_sw(this_t_i(i_agent)+1,this_agents(i_agent)) = this_k(i_agent) - p;
                else
                    c_sw(this_t_i(i_agent),this_agents(i_agent)) = this_u(i_agent);
                    pass_sw(this_t_i(i_agent),this_agents(i_agent)) = 0;
                    k_sw(this_t_i(i_agent)+1,this_agents(i_agent)) = this_k(i_agent) + p;
                end
            end
        end
    end

    %% Perfromance measures
    fprintf('Computing performance measures\n');

    %% Accumulated costs per agent at each time step
    fprintf('Computing accumulated costs\n');
    a_rand = get_accumulated_cost(c_rand, t_i, param);
    a_u = get_accumulated_cost(c_u, t_i, param);
    a_a = get_accumulated_cost(c_a, t_i, param);
    a_u_a = get_accumulated_cost(c_u_a, t_i, param);
    a_apass = get_accumulated_cost(c_apass, t_i, param);
    if control.fairness_horizon_policies
        a_fair_hor_a = cell(param.n_fairness_horizon, 1);
        a_fair_hor_u_a = cell(param.n_fairness_horizon, 1);
        for i_fair_hor = 1 : param.n_fairness_horizon
            a_fair_hor_a{i_fair_hor} = get_accumulated_cost(c_fair_hor_a{i_fair_hor}, t_i, param);
            a_fair_hor_u_a{i_fair_hor} = get_accumulated_cost(c_fair_hor_u_a{i_fair_hor}, t_i, param);
        end
    end
    if control.highest_karma
        a_highest_karma = get_accumulated_cost(c_highest_karma, t_i, param);
    end
    if control.se_policies
        a_se = cell(param.n_alpha_comp, 1);
        for i_alpha_comp = 1 : param.n_alpha_comp
            a_se{i_alpha_comp} = get_accumulated_cost(c_se{i_alpha_comp}, t_i, param);
        end
    end
    if control.sw_policy
        a_sw = get_accumulated_cost(c_sw, t_i, param);
    end

    %% Accumulated number of passes per agent at each time step
    fprintf('Computing # of passes\n');
    a_pass_rand = get_accumulated_cost(pass_rand, t_i, param);
    a_pass_u = get_accumulated_cost(pass_u, t_i, param);
    a_pass_a = get_accumulated_cost(pass_a, t_i, param);
    a_pass_u_a = get_accumulated_cost(pass_u_a, t_i, param);
    a_pass_apass = get_accumulated_cost(pass_apass, t_i, param);
    if control.fairness_horizon_policies
        a_pass_fair_hor_a = cell(param.n_fairness_horizon, 1);
        a_pass_fair_hor_u_a = cell(param.n_fairness_horizon, 1);
        for i_fair_hor = 1 : param.n_fairness_horizon
            a_pass_fair_hor_a{i_fair_hor} = get_accumulated_cost(pass_fair_hor_a{i_fair_hor}, t_i, param);
            a_pass_fair_hor_u_a{i_fair_hor} = get_accumulated_cost(pass_fair_hor_u_a{i_fair_hor}, t_i, param);
        end
    end
    if control.highest_karma
        a_pass_highest_karma = get_accumulated_cost(pass_highest_karma, t_i, param);
    end
    if control.se_policies
        a_pass_se = cell(param.n_alpha_comp, 1);
        for i_alpha_comp = 1 : param.n_alpha_comp
            a_pass_se{i_alpha_comp} = get_accumulated_cost(pass_se{i_alpha_comp}, t_i, param);
        end
    end
    if control.sw_policy
        a_pass_sw = get_accumulated_cost(pass_sw, t_i, param);
    end

    %% Inefficiency vs. time
    fprintf('Computing efficiencies\n');
    IE_rand_per_t = mean(a_rand, 2);
    IE_rand = IE_rand_per_t(end);
    IE_u_per_t = mean(a_u, 2);
    IE_u = IE_u_per_t(end);
    IE_a_per_t = mean(a_a, 2);
    IE_a = IE_a_per_t(end);
    IE_u_a_per_t = mean(a_u_a, 2);
    IE_u_a = IE_u_a_per_t(end);
    IE_apass_per_t = mean(a_apass, 2);
    IE_apass = IE_apass_per_t(end);
    if control.fairness_horizon_policies
        IE_fair_hor_a_per_t = cell(param.n_fairness_horizon, 1);
        IE_fair_hor_a = zeros(param.n_fairness_horizon, 1);
        IE_fair_hor_u_a_per_t = cell(param.n_fairness_horizon, 1);
        IE_fair_hor_u_a = zeros(param.n_fairness_horizon, 1);
        for i_fair_hor = 1 : param.n_fairness_horizon
            IE_fair_hor_a_per_t{i_fair_hor} = mean(a_fair_hor_a{i_fair_hor}, 2);
            IE_fair_hor_a(i_fair_hor) = IE_fair_hor_a_per_t{i_fair_hor}(end);
            IE_fair_hor_u_a_per_t{i_fair_hor} = mean(a_fair_hor_u_a{i_fair_hor}, 2);
            IE_fair_hor_u_a(i_fair_hor) = IE_fair_hor_u_a_per_t{i_fair_hor}(end);
        end
    end
    if control.highest_karma
        IE_highest_karma_per_t = mean(a_highest_karma, 2);
        IE_highest_karma = IE_highest_karma_per_t(end);
    end
    if control.se_policies
        IE_se_per_t = cell(param.n_alpha_comp, 1);
        IE_se = zeros(param.n_alpha_comp, 1);
        for i_alpha_comp = 1 : param.n_alpha_comp
            IE_se_per_t{i_alpha_comp} = mean(a_se{i_alpha_comp}, 2);
            IE_se(i_alpha_comp) = IE_se_per_t{i_alpha_comp}(end);
        end
    end
    if control.sw_policy
        IE_sw_per_t = mean(a_sw, 2);
        IE_sw = IE_sw_per_t(end);
    end

    %% Unfairness based on accumulated cost vs. time
    fprintf('Computing accumulated cost-based fairness\n');
    UF_rand_per_t = std(a_rand, [], 2);
    UF_rand = UF_rand_per_t(end);
    UF_u_per_t = std(a_u, [], 2);
    UF_u = UF_u_per_t(end);
    UF_a_per_t = std(a_a, [], 2);
    UF_a = UF_a_per_t(end);
    UF_u_a_per_t = std(a_u_a, [], 2);
    UF_u_a = UF_u_a_per_t(end);
    UF_apass_per_t = std(a_apass, [], 2);
    UF_apass = UF_apass_per_t(end);
    if control.fairness_horizon_policies
        UF_fair_hor_a_per_t = cell(param.n_fairness_horizon, 1);
        UF_fair_hor_a = zeros(param.n_fairness_horizon, 1);
        UF_fair_hor_u_a_per_t = cell(param.n_fairness_horizon, 1);
        UF_fair_hor_u_a = zeros(param.n_fairness_horizon, 1);
        for i_fair_hor = 1 : param.n_fairness_horizon
            UF_fair_hor_a_per_t{i_fair_hor} = std(a_fair_hor_a{i_fair_hor}, [], 2);
            UF_fair_hor_a(i_fair_hor) = UF_fair_hor_a_per_t{i_fair_hor}(end);
            UF_fair_hor_u_a_per_t{i_fair_hor} = std(a_fair_hor_u_a{i_fair_hor}, [], 2);
            UF_fair_hor_u_a(i_fair_hor) = UF_fair_hor_u_a_per_t{i_fair_hor}(end);
        end
    end
    if control.highest_karma
        UF_highest_karma_per_t = std(a_highest_karma, [], 2);
        UF_highest_karma = UF_highest_karma_per_t(end);
    end
    if control.se_policies
        UF_se_per_t = cell(param.n_alpha_comp, 1);
        UF_se = zeros(param.n_alpha_comp, 1);
        for i_alpha_comp = 1 : param.n_alpha_comp
            UF_se_per_t{i_alpha_comp} = std(a_se{i_alpha_comp}, [], 2);
            UF_se(i_alpha_comp) = UF_se_per_t{i_alpha_comp}(end);
        end
    end
    if control.sw_policy
        UF_sw_per_t = std(a_sw, [], 2);
        UF_sw = UF_sw_per_t(end);
    end

    %% Unfairness based on # of passes vs. time
    fprintf('Computing # of passes-based fairness\n');
    UF_pass_rand_per_t = std(a_pass_rand, [], 2);
    UF_pass_rand = UF_pass_rand_per_t(end);
    UF_pass_u_per_t = std(a_pass_u, [], 2);
    UF_pass_u = UF_pass_u_per_t(end);
    UF_pass_a_per_t = std(a_pass_a, [], 2);
    UF_pass_a = UF_pass_a_per_t(end);
    UF_pass_u_a_per_t = std(a_pass_u_a, [], 2);
    UF_pass_u_a = UF_pass_u_a_per_t(end);
    UF_pass_apass_per_t = std(a_pass_apass, [], 2);
    UF_pass_apass = UF_pass_apass_per_t(end);
    if control.fairness_horizon_policies
        UF_pass_fair_hor_a_per_t = cell(param.n_fairness_horizon, 1);
        UF_pass_fair_hor_a = zeros(param.n_fairness_horizon, 1);
        UF_pass_fair_hor_u_a_per_t = cell(param.n_fairness_horizon, 1);
        UF_pass_fair_hor_u_a = zeros(param.n_fairness_horizon, 1);
        for i_fair_hor = 1 : param.n_fairness_horizon
            UF_pass_fair_hor_a_per_t{i_fair_hor} = std(a_pass_fair_hor_a{i_fair_hor}, [], 2);
            UF_pass_fair_hor_a(i_fair_hor) = UF_pass_fair_hor_a_per_t{i_fair_hor}(end);
            UF_pass_fair_hor_u_a_per_t{i_fair_hor} = std(a_pass_fair_hor_u_a{i_fair_hor}, [], 2);
            UF_pass_fair_hor_u_a(i_fair_hor) = UF_pass_fair_hor_u_a_per_t{i_fair_hor}(end);
        end
    end
    if control.highest_karma
        UF_pass_highest_karma_per_t = std(a_pass_highest_karma, [], 2);
        UF_pass_highest_karma = UF_pass_highest_karma_per_t(end);
    end
    if control.se_policies
        UF_pass_se_per_t = cell(param.n_alpha_comp, 1);
        UF_pass_se = zeros(param.n_alpha_comp, 1);
        for i_alpha_comp = 1 : param.n_alpha_comp
            UF_pass_se_per_t{i_alpha_comp} = std(a_pass_se{i_alpha_comp}, [], 2);
            UF_pass_se(i_alpha_comp) = UF_pass_se_per_t{i_alpha_comp}(end);
        end
    end
    if control.sw_policy
        UF_pass_sw_per_t = std(a_pass_sw, [], 2);
        UF_pass_sw = UF_pass_sw_per_t(end);
    end

    %% Average payment
    % a.k.a. GDP
    if control.highest_karma
        p_bar_highest_karma = mean(p_highest_karma);
        p_std_highest_karma = std(p_highest_karma);
    end
    if control.se_policies
        p_bar_se = mean(p_se, 1);
        p_std_se = std(p_se, [], 1);
    end
    if control.sw_policy
        p_bar_sw = mean(p_sw);
        p_std_sw = std(p_sw);
    end

    %% Karma distributions
    if control.highest_karma
        [k_highest_karma_dist, k_highest_karma_dist_agents] = get_karma_dist(k_highest_karma, param);
    end
    if control.se_policies
        fprintf('Computing karma distributions\n');
        k_se_dist = cell(param.n_alpha_comp, 1);
        k_se_dist_agents = cell(param.n_alpha_comp, 1);
        for i_alpha_comp = 1 : param.n_alpha_comp
            [k_se_dist{i_alpha_comp}, k_se_dist_agents{i_alpha_comp}] = get_karma_dist(k_se{i_alpha_comp}, param);
        end
    end
    if control.sw_policy
        [k_sw_dist, k_sw_dist_agents] = get_karma_dist(k_sw, param);
    end

    %% Store results
    if param.save
        fprintf('Saving workspace\n');
        if control.se_policies || control.sw_policy
            if param.n_alpha == 1 && param.n_alpha_comp == 1
                save(['results/k_bar_', num2str(param.k_bar, '%02d'), '_alpha_', num2str(param.Alpha, '%.2f'), '_take_', int2str(seed), '.mat']);
            else
                save(['results/k_bar_', num2str(param.k_bar, '%02d'), '_take_', int2str(seed), '.mat']);
            end
        else
            save('results/centralized_policies.mat');
        end
    end
end

%% Plots
fprintf('Plotting\n');
do_plots;

%% Inform user when done and sound
fprintf('DONE\n\n');
load mtlb.mat;
sound(mtlb);