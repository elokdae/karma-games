clc;
screenwidth = 1920;
screenheight = 1080;

% %% Specify file
% file = 'stationary_equilibrium/results/se_U_1_1_10_phi1_0.95_0.05_0.00_0.00_0.50_0.50_0.95_0.05_0.00_alpha1_0.90_alpha2_1.00_pay_0_tax_0.005/k_bar_10_z_0.90_0.10.mat';
% 
% %% Cost fairness
% load(file);

% Overall efficiency
e = dot(reshape(se_d_up_mu_alpha_u_k, [], 1), reshape(se_R_down_mu_alpha_u_k, [], 1));

% Efficiency per type
e_down_mu_alpha = zeros(param.n_mu, param.n_alpha);
for i_mu = 1 : param.n_mu
    for i_alpha = 1 : param.n_alpha
        e_down_mu_alpha(i_mu,i_alpha) = 1 / param.g_up_mu_alpha(i_mu,i_alpha) * dot(reshape(se_d_up_mu_alpha_u_k(i_mu,i_alpha,:,:), [], 1), reshape(se_R_down_mu_alpha_u_k(i_mu,i_alpha,:,:), [], 1));
    end
end

e_array = e;
labels = "overall";
for i_mu = 1 : param.n_mu
    for i_alpha = 1 : param.n_alpha
        e_array = [e_array, e_down_mu_alpha(i_mu,i_alpha)];
        label_str = strcat("\mu=", int2str(i_mu), ", \alpha=", num2str(param.Alpha(i_alpha), '%.2f'));
        labels = [labels, label_str];
    end
end


%% Plot
figure(2);
fig = gcf;
fig.Position = [0, 0, screenheight, screenheight];
% bar(categorical(labels), e_array, 'FaceColor', 'none', 'EdgeColor', 'flat', 'LineWidth', 2);
bar(categorical(labels), -e_array);
text(categorical(labels),-e_array,num2str(-e_array.'),'vert','bottom','horiz','center', 'FontSize', 20, 'FontWeight', 'bold');
hold on;
axis tight;
axes = gca;
axes.XAxis.FontSize = 15;
axes.XAxis.FontName = 'ubuntu';
axes.YAxis.FontSize = 15;
axes.YAxis.FontName = 'ubuntu';
axes.XLabel.FontName = 'ubuntu';
axes.XLabel.FontWeight = 'bold';
axes.XLabel.FontSize = 20;
axes.YLabel.FontName = 'ubuntu';
axes.YLabel.FontWeight = 'bold';
axes.YLabel.String = 'Average cost';
axes.YLabel.FontSize = 20;