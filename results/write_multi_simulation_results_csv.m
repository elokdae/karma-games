clc;

%% Some parameters
Seed = 0 : 9;
n_seed = length(Seed);

read_file_start = 'results/U_1_1_10_phi1_0.95_0.05_0.00_0.00_0.50_0.50_0.95_0.05_0.00_pay_1/k_bar_10_take_';
write_file_start = '~/papers/karma-mechanisms/data/U-1-1-10-PBS-take-';

%% Get data
for i_seeds = 1 : n_seed
    seed = Seed(i_seeds);
    read_file = [read_file_start, int2str(seed), '.mat'];
    if exist(read_file, 'file')
        load(read_file);
        
        if i_seeds == 1
            IE_se_array = zeros(param.n_alpha_comp, n_seed);
            UF_se_array = zeros(param.n_alpha_comp, n_seed);
            UF_pass_se_array = zeros(param.n_alpha_comp, n_seed);
            IE_rand_array = zeros(1, n_seed);
            UF_rand_array = zeros(1, n_seed);
            UF_pass_rand_array = zeros(1, n_seed);
            IE_u_array = zeros(1, n_seed);
            UF_u_array = zeros(1, n_seed);
            UF_pass_u_array = zeros(1, n_seed);
            IE_a_array = zeros(1, n_seed);
            UF_a_array = zeros(1, n_seed);
            UF_pass_a_array = zeros(1, n_seed);
            IE_apass_array = zeros(1, n_seed);
            UF_apass_array = zeros(1, n_seed);
            UF_pass_apass_array = zeros(1, n_seed);
        end
        
        IE_se_array(:,i_seeds) = IE_se;
        UF_se_array(:,i_seeds) = UF_se;
        UF_pass_se_array(:,i_seeds) = UF_pass_se;
        IE_rand_array(i_seeds) = IE_rand;
        UF_rand_array(i_seeds) = UF_rand;
        UF_pass_rand_array(i_seeds) = UF_pass_rand;
        IE_u_array(i_seeds) = IE_u;
        UF_u_array(i_seeds) = UF_u;
        UF_pass_u_array(i_seeds) = UF_pass_u;
        IE_a_array(i_seeds) = IE_a;
        UF_a_array(i_seeds) = UF_a;
        UF_pass_a_array(i_seeds) = UF_pass_a;
        IE_apass_array(i_seeds) = IE_apass;
        UF_apass_array(i_seeds) = UF_apass;
        UF_pass_apass_array(i_seeds) = UF_pass_apass;
        
        write_file = [write_file_start, int2str(seed)];
        write_simulation_results_csv(param.Alpha, IE_se, UF_se, UF_pass_se, IE_rand, UF_rand, UF_pass_rand, IE_u, UF_u, UF_pass_u, IE_a, UF_a, UF_pass_a, IE_a, UF_a, UF_pass_a, write_file);
    end
end

%% Mean data
IE_se_mean = mean(IE_se_array, 2);
UF_se_mean = mean(UF_se_array, 2);
UF_pass_se_mean = mean(UF_pass_se_array, 2);
IE_rand_mean = mean(IE_rand_array, 2);
UF_rand_mean = mean(UF_rand_array, 2);
UF_pass_rand_mean = mean(UF_pass_rand_array, 2);
IE_u_mean = mean(IE_u_array, 2);
UF_u_mean = mean(UF_u_array, 2);
UF_pass_u_mean = mean(UF_pass_u_array, 2);
IE_a_mean = mean(IE_a_array, 2);
UF_a_mean = mean(UF_a_array, 2);
UF_pass_a_mean = mean(UF_pass_a_array, 2);
IE_apass_mean = mean(IE_apass_array, 2);
UF_apass_mean = mean(UF_apass_array, 2);
UF_pass_apass_mean = mean(UF_pass_apass_array, 2);

write_file = [write_file_start, 'mean'];
write_simulation_results_csv(param.Alpha, IE_se_mean, UF_se_mean, UF_pass_se_mean, IE_rand_mean, UF_rand_mean, UF_pass_rand_mean, IE_u_mean, UF_u_mean, UF_pass_u_mean, IE_a_mean, UF_a_mean, UF_pass_a_mean, IE_apass_mean, UF_apass_mean, UF_pass_apass_mean, write_file);

%% Min data
% Min efficiency/fairness -> max inefficiency/unfairness
IE_se_max = max(IE_se_array, [], 2);
UF_se_max = max(UF_se_array, [], 2);
UF_pass_se_max = max(UF_pass_se_array, [], 2);
IE_rand_max = max(IE_rand_array, [], 2);
UF_rand_max = max(UF_rand_array, [], 2);
UF_pass_rand_max = max(UF_pass_rand_array, [], 2);
IE_u_max = max(IE_u_array, [], 2);
UF_u_max = max(UF_u_array, [], 2);
UF_pass_u_max = max(UF_pass_u_array, [], 2);
IE_a_max = max(IE_a_array, [], 2);
UF_a_max = max(UF_a_array, [], 2);
UF_pass_a_max = max(UF_pass_a_array, [], 2);
IE_apass_max = max(IE_apass_array, [], 2);
UF_apass_max = max(UF_apass_array, [], 2);
UF_pass_apass_max = max(UF_pass_apass_array, [], 2);

write_file = [write_file_start, 'min'];
write_simulation_results_csv(param.Alpha, IE_se_max, UF_se_max, UF_pass_se_max, IE_rand_max, UF_rand_max, UF_pass_rand_max, IE_u_max, UF_u_max, UF_pass_u_max, IE_a_max, UF_a_max, UF_pass_a_max, IE_apass_max, UF_apass_max, UF_pass_apass_max, write_file);

%% Max data
% Max efficiency/fairness -> min inefficiency/unfairness
IE_se_min = min(IE_se_array, [], 2);
UF_se_min = min(UF_se_array, [], 2);
UF_pass_se_min = min(UF_pass_se_array, [], 2);
IE_rand_min = min(IE_rand_array, [], 2);
UF_rand_min = min(UF_rand_array, [], 2);
UF_pass_rand_min = min(UF_pass_rand_array, [], 2);
IE_u_min = min(IE_u_array, [], 2);
UF_u_min = min(UF_u_array, [], 2);
UF_pass_u_min = min(UF_pass_u_array, [], 2);
IE_a_min = min(IE_a_array, [], 2);
UF_a_min = min(UF_a_array, [], 2);
UF_pass_a_min = min(UF_pass_a_array, [], 2);
IE_apass_min = min(IE_apass_array, [], 2);
UF_apass_min = min(UF_apass_array, [], 2);
UF_pass_apass_min = min(UF_pass_apass_array, [], 2);

write_file = [write_file_start, 'max'];
write_simulation_results_csv(param.Alpha, IE_se_min, UF_se_min, UF_pass_se_min, IE_rand_min, UF_rand_min, UF_pass_rand_min, IE_u_min, UF_u_min, UF_pass_u_min, IE_a_min, UF_a_min, UF_pass_a_min, IE_apass_min, UF_apass_min, UF_pass_apass_min, write_file);