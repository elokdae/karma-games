clear;
clc;

%% Screen size used to place plots
screensize = get(groot, 'ScreenSize');
screenwidth = screensize(3);
screenheight = screensize(4);
load('stationary_equilibrium/RedColormap.mat');

file_str = 'stationary_equilibrium/results/sw_U_1_2_phi1_0.50_0.50_0.50_0.50_pay_0/k_bar_';

% Flag to save data
save_data = false;

%% Select alpha(s) to process
while true
    k_bar_vec = input('Enter k_bar(s): ');
    
    for i_k_bar = 1 : length(k_bar_vec)
        k_bar = k_bar_vec(i_k_bar);

        % Load workspace for computation for that alpha
        file = [file_str, num2str(k_bar, '%02d'), '.mat'];
        load(file, 'se_param', 'sw_pi_down_u_k_up_m', 'sw_d_up_u_k',...
            'sw_R_down_u_k', 'sw_P_down_u_k_up_un_kn', 'sw_sigma_up_k');

        % Plots
        close all;
        % SW policy plot
        sw_pi_plot_fg = 1;
        sw_pi_plot_pos = [0, 0, screenwidth, screenheight / 2];
        plot_sw_pi(sw_pi_plot_fg, sw_pi_plot_pos, RedColormap, sw_pi_down_u_k_up_m, se_param.U, se_param.K, se_param.K, k_bar);

        % SW stationary distribution plot
        sw_d_plot_fg = 2;
        sw_d_plot_pos = [0, screenheight / 2, screenwidth, screenheight / 2];
        plot_sw_d(sw_d_plot_fg, sw_d_plot_pos, sw_d_up_u_k, se_param.U, se_param.K, k_bar);

        % SW stationary karma distribution plot
        sw_sigma_plot_fg = 5;
        sw_sigma_plot_pos = [0, screenheight / 2, screenwidth / 2, screenheight / 2];
        plot_sw_sigma(sw_sigma_plot_fg, sw_sigma_plot_pos, sw_sigma_up_k, se_param.K, k_bar);
        
        % SW stage rewards plot
        sw_R_plot_fg = 3;
        sw_R_plot_pos = [0, 0, screenwidth / 2, screenheight / 2];
        plot_sw_R(sw_R_plot_fg, sw_R_plot_pos, sw_R_down_u_k, se_param.U, se_param.K, k_bar);

        % SW state transitions plot
        sw_P_plot_fg = 4;
        sw_P_plot_pos = [0, 0, screenwidth, screenheight];
        plot_sw_P(sw_P_plot_fg, sw_P_plot_pos, RedColormap, sw_P_down_u_k_up_un_kn, se_param.U, se_param.K, k_bar);
        
        if save_data
            if ~exist('stationary_equilibrium/results/sw_policies', 'dir')
                mkdir('stationary_equilibrium/results/sw_policies');
            end
            if ~exist('stationary_equilibrium/results/sw_distributions', 'dir')
                mkdir('stationary_equilibrium/results/sw_distributions');
            end
            if ~exist('stationary_equilibrium/results/sw_karma_distributions', 'dir')
                mkdir('stationary_equilibrium/results/sw_karma_distributions');
            end
            if ~exist('stationary_equilibrium/results/sw_stage_rewards', 'dir')
                mkdir('stationary_equilibrium/results/sw_stage_rewards');
            end
            if ~exist('stationary_equilibrium/results/sw_state_transitions', 'dir')
                mkdir('stationary_equilibrium/results/sw_state_transitions');
            end

            saveas(sw_pi_plot_fg, ['stationary_equilibrium/results/policies/k_ave_', num2str(k_bar, '%02d'), '.png']);
            saveas(sw_d_plot_fg, ['stationary_equilibrium/results/stationary_distributions/k_ave_', num2str(k_bar, '%02d'), '.png']);
            saveas(sw_sigma_plot_fg, ['stationary_equilibrium/results/stationary_karma_distributions/k_ave_', num2str(k_bar, '%02d'), '.png']);
            saveas(sw_R_plot_fg, ['stationary_equilibrium/results/expected_costs/k_ave_', num2str(k_bar, '%02d'), '.png']);
            saveas(sw_P_plot_fg, ['stationary_equilibrium/results/state_transitions/k_ave_', num2str(k_bar, '%02d'), '.png']);
        end
    end
end