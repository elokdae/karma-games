clear;
clc;

%% Add functions folder to path
addpath('functions');
addpath('stationary_equilibrium/se_functions');

%% Screen size used to place plots
screensize = get(groot, 'ScreenSize');
% screenwidth = screensize(3);
% screenheight = screensize(4);
screenwidth = 1920;
screenheight = 1080;
load('stationary_equilibrium/RedColormap.mat');

file_str = 'stationary_equilibrium/results/se_U_1_phi1_1.00_pay_3/k_bar_';

% Flag to save plots
save_plots = true;

%% Select k_bar(s) and alpha(s) to process
k_bar_vec = input('Enter k_bar(s): ');
alpha_comp_vec = input('Enter alpha(s): ');
z1_vec = input('Enter z1(s): ');

for i_k_bar = 1 : length(k_bar_vec)
    k_bar = k_bar_vec(i_k_bar);
    file_str2 = [file_str, num2str(k_bar, '%02d')];
    for i_alpha_comp = 1 : max([length(alpha_comp_vec), 1])
        for i_z = 1 : max([length(z1_vec), 1])
            if length(alpha_comp_vec) >= 1
                alpha = alpha_comp_vec(i_alpha_comp);
                if alpha > 0.99 && alpha < 1
                    alpha_str = num2str(alpha, '%.3f');
                else
                    alpha_str = num2str(alpha, '%.2f');
                end
                file = [file_str2, '_alpha_', alpha_str, '.mat'];
            else
                file = [file_str2, '_z_', num2str(z1_vec(i_z), '%.2f'), '_', num2str(1 - z1_vec(i_z), '%.2f'), '.mat'];
            end
            if ~exist(file, 'file')
                continue;
            end
            load(file, 'param', 'se_param', 'se_pi_down_mu_alpha_u_k_up_b',...
                'se_d_up_mu_alpha_u_k', 'se_nu_up_bj', 'br_pi_down_mu_alpha_u_k_up_b',...
                'se_sigma_down_mu_alpha_up_k', 'se_V_down_mu_alpha_u_k',...
                'se_Q_down_mu_alpha_u_k_b', 'se_P_down_mu_alpha_u_k_up_un_kn',...
                'se_pi_error_hist');

            % Plots
            close all;
            if length(alpha_comp_vec) >= 1
                i_alpha_plot = find(abs(param.Alpha - alpha) < eps);
            else
                i_alpha_plot = i_alpha_comp;
            end
            % SE policy plot
            se_pi_plot_fg = 1;
            se_pi_plot_pos = [0, 0, screenwidth, screenheight];
            plot_se_pi(se_pi_plot_fg, se_pi_plot_pos, RedColormap, se_pi_down_mu_alpha_u_k_up_b, param, se_param, i_alpha_plot);

            % SE stationary distribution plot
            se_d_plot_fg = 2;
            se_d_plot_pos = [0, 0, screenwidth, screenheight];
            plot_se_d(se_d_plot_fg, se_d_plot_pos, se_d_up_mu_alpha_u_k, param, se_param, i_alpha_plot);

            % SE distribution of bids plot
            se_nu_plot_fg = 3;
            se_nu_plot_pos = [0, 0, screenwidth, screenheight];
            plot_se_nu(se_nu_plot_fg, se_nu_plot_pos, se_nu_up_bj, param, se_param, i_alpha_plot);
            
            % Best response policy plot
            br_pi_plot_fg = 4;
            br_pi_plot_pos = [0, 0, screenwidth, screenheight];
            plot_br_pi(br_pi_plot_fg, br_pi_plot_pos, RedColormap, br_pi_down_mu_alpha_u_k_up_b, param, se_param, i_alpha_plot);

            % SE karma distribution plot
            se_sigma_plot_fg = 5;
            se_sigma_plot_pos = [0, 0, screenwidth, screenheight];
            plot_se_sigma(se_sigma_plot_fg, se_sigma_plot_pos, se_sigma_down_mu_alpha_up_k, param, se_param, i_alpha_plot);

            % SE value function plot
            se_V_plot_fg = 6;
            se_V_plot_pos = [0, 0, screenwidth, screenheight];
            plot_se_V(se_V_plot_fg, se_V_plot_pos, se_V_down_mu_alpha_u_k, param, se_param, i_alpha_plot);

            % SE single-stage deviation rewards plot
            se_Q_plot_fg = 7;
            se_Q_plot_pos = [0, 0, screenwidth, screenheight];
            plot_se_Q(se_Q_plot_fg, se_Q_plot_pos, parula, se_Q_down_mu_alpha_u_k_b, param, se_param, i_alpha_plot);

            % SE state transitions plot
            se_P_plot_fg = 8;
            se_P_plot_pos = [0, 0, screenwidth, screenheight];
            plot_se_P(se_P_plot_fg, se_P_plot_pos, RedColormap, se_P_down_mu_alpha_u_k_up_un_kn, param, se_param, i_alpha_plot);

            % SE policy error plot
            se_pi_error_plot_fg = 9;
            se_pi_error_plot_pos = [0, 0, screenwidth, screenheight];
            plot_se_pi_error(se_pi_error_plot_fg, se_pi_error_plot_pos, se_pi_error_hist, param, i_alpha_plot);

            if save_plots
                if ~exist('stationary_equilibrium/results/se_policies', 'dir')
                    mkdir('stationary_equilibrium/results/se_policies');
                end
                if ~exist('stationary_equilibrium/results/se_distributions', 'dir')
                    mkdir('stationary_equilibrium/results/se_distributions');
                end
                if ~exist('stationary_equilibrium/results/se_bid_distributions', 'dir')
                    mkdir('stationary_equilibrium/results/se_bid_distributions');
                end
                if ~exist('stationary_equilibrium/results/se_karma_distributions', 'dir')
                    mkdir('stationary_equilibrium/results/se_karma_distributions');
                end
                if ~exist('stationary_equilibrium/results/se_value_functions', 'dir')
                    mkdir('stationary_equilibrium/results/se_value_functions');
                end
                if ~exist('stationary_equilibrium/results/se_state_transitions', 'dir')
                    mkdir('stationary_equilibrium/results/se_state_transitions');
                end
                if ~exist('stationary_equilibrium/results/se_policy_errors', 'dir')
                    mkdir('stationary_equilibrium/results/se_policy_errors');
                end
                
                se_pi_file = ['stationary_equilibrium/results/se_policies/k_bar_', num2str(k_bar, '%02d')];
                se_d_file = ['stationary_equilibrium/results/se_distributions/k_bar_', num2str(k_bar, '%02d')];
                se_nu_file = ['stationary_equilibrium/results/se_bid_distributions/k_bar_', num2str(k_bar, '%02d')];
                se_sigma_file = ['stationary_equilibrium/results/se_karma_distributions/k_bar_', num2str(k_bar, '%02d')];
                se_V_file = ['stationary_equilibrium/results/se_value_functions/k_bar_', num2str(k_bar, '%02d')];
                se_P_file = ['stationary_equilibrium/results/se_state_transitions/k_bar_', num2str(k_bar, '%02d')];
                se_pi_error_file = ['stationary_equilibrium/results/se_policy_errors/k_bar_', num2str(k_bar, '%02d')];
                if length(alpha_comp_vec) >= 1
                    se_pi_file = [se_pi_file, '_alpha_', alpha_str, '.svg'];
                    se_d_file = [se_d_file, '_alpha_', alpha_str, '.svg'];
                    se_nu_file = [se_nu_file, '_alpha_', alpha_str, '.svg'];
                    se_sigma_file = [se_sigma_file, '_alpha_', alpha_str, '.svg'];
                    se_V_file = [se_V_file, '_alpha_', alpha_str, '.svg'];
                    se_P_file = [se_P_file, '_alpha_', alpha_str, '.svg'];
                    se_pi_error_file = [se_pi_error_file, '_alpha_', alpha_str, '.svg'];
                else
                    se_pi_file = [se_pi_file, '_z_', num2str(z1_vec(i_z), '%.2f'), '_', num2str(1 - z1_vec(i_z), '%.2f'), '.svg'];
                    se_d_file = [se_d_file, '_z_', num2str(z1_vec(i_z), '%.2f'), '_', num2str(1 - z1_vec(i_z), '%.2f'), '.svg'];
                    se_nu_file = [se_nu_file, '_z_', num2str(z1_vec(i_z), '%.2f'), '_', num2str(1 - z1_vec(i_z), '%.2f'), '.svg'];
                    se_sigma_file = [se_sigma_file, '_z_', num2str(z1_vec(i_z), '%.2f'), '_', num2str(1 - z1_vec(i_z), '%.2f'), '.svg'];
                    se_V_file = [se_V_file, '_z_', num2str(z1_vec(i_z), '%.2f'), '_', num2str(1 - z1_vec(i_z), '%.2f'), '.svg'];
                    se_P_file = [se_P_file, '_z_', num2str(z1_vec(i_z), '%.2f'), '_', num2str(1 - z1_vec(i_z), '%.2f'), '.svg'];
                    se_pi_error_file = [se_pi_error_file, '_z_', num2str(z1_vec(i_z), '%.2f'), '_', num2str(1 - z1_vec(i_z), '%.2f'), '.svg'];
                end
                saveas(se_pi_plot_fg, se_pi_file);
                saveas(se_d_plot_fg, se_d_file);
                saveas(se_nu_plot_fg, se_nu_file);
                saveas(se_sigma_plot_fg, se_sigma_file);
                saveas(se_V_plot_fg, se_V_file);
                saveas(se_P_plot_fg, se_P_file);
                saveas(se_pi_error_plot_fg, se_pi_error_file);
            end
        end
    end
end

%% Inform user when done
fprintf('DONE\n\n');