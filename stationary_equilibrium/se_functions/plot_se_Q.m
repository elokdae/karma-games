% Plot SE single-stage deviation rewards
function plot_se_Q(fg, position, colormap, se_Q_down_mu_alpha_u_k_b, param, se_param, i_alpha_comp)
    persistent se_Q_plot
    if ~ishandle(fg)
        figure(fg);
        fig = gcf;
        fig.Position = position;
        se_Q_plot = cell(param.n_mu, param.n_alpha, param.n_u);
        n_subplots = param.n_mu * param.n_alpha * param.n_u;
        n_cols = ceil(sqrt(n_subplots));
        n_rows = ceil(n_subplots / n_cols);
        for i_mu = 1 : param.n_mu
            for i_alpha = 1 : param.n_alpha
                for i_u = 1 : param.n_u
                    Q_mat = squeeze(se_Q_down_mu_alpha_u_k_b(i_mu,i_alpha,i_u,:,:));
                    for i_k = 1 : se_param.n_k
                        Q_mat(i_k,se_param.K>se_param.K(i_k)) = nan;
                    end
                    i_subplot = (i_mu - 1) * (param.n_alpha * param.n_u) + (i_alpha - 1) * param.n_u + i_u;
                    subplot(n_rows, n_cols, i_subplot);
                    se_Q_plot{i_mu,i_alpha,i_u} = heatmap(se_param.K, se_param.K, Q_mat.', 'ColorbarVisible','off');
                    se_Q_plot{i_mu,i_alpha,i_u}.YDisplayData = flipud(se_Q_plot{i_mu,i_alpha,i_u}.YDisplayData);
                    alpha = param.Alpha(max([i_alpha, i_alpha_comp]));
                    if alpha > 0.99 && alpha < 1
                        alpha_str = num2str(alpha, '%.3f');
                    else
                        alpha_str = num2str(alpha, '%.2f');
                    end
                    se_Q_plot{i_mu,i_alpha,i_u}.Title = ['k_{bar} = ', num2str(param.k_bar, '%02d'), ' SE payoffs per bid for \mu = ', num2str(i_mu), ', \alpha = ', alpha_str, ', u = ', num2str(param.U(i_u))];
                    se_Q_plot{i_mu,i_alpha,i_u}.XLabel = 'Karma';
                    se_Q_plot{i_mu,i_alpha,i_u}.YLabel = 'Bid';
                    se_Q_plot{i_mu,i_alpha,i_u}.FontName = 'Ubuntu';
                    se_Q_plot{i_mu,i_alpha,i_u}.FontSize = 10;
                    if exist('colormap', 'var')
                        se_Q_plot{i_mu,i_alpha,i_u}.Colormap = colormap;
                    end
                    se_Q_plot{i_mu,i_alpha,i_u}.CellLabelFormat = '%.2f';
                end
            end
        end
    else
        for i_mu = 1 : param.n_mu
            for i_alpha = 1 : param.n_alpha
                for i_u = 1 : param.n_u
                    Q_mat = squeeze(se_Q_down_mu_alpha_u_k_b(i_mu,i_alpha,i_u,:,:));
                    for i_k = 1 : se_param.n_k
                        Q_mat(i_k,se_param.K>se_param.K(i_k)) = nan;
                    end
                    se_Q_plot{i_mu,i_alpha,i_u}.ColorData = Q_mat.';
                    alpha = param.Alpha(max([i_alpha, i_alpha_comp]));
                    if alpha > 0.99 && alpha < 1
                        alpha_str = num2str(alpha, '%.3f');
                    else
                        alpha_str = num2str(alpha, '%.2f');
                    end
                    se_Q_plot{i_mu,i_alpha,i_u}.Title = ['k_{bar} = ', num2str(param.k_bar, '%02d'), ' SE payoffs per bid for \mu = ', num2str(i_mu), ', \alpha = ', alpha_str, ', u = ', num2str(param.U(i_u))];
                end
            end
        end
    end
end