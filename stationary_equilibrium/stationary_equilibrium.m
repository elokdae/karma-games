clear;
close all;
clc;

%% Add functions folder to path
addpath('functions');
addpath('stationary_equilibrium/se_functions');

%% Screen size used to place plots
screensize = get(groot, 'ScreenSize');
% screenwidth = screensize(3);
% screenheight = screensize(4);
screenwidth = 1920;
screenheight = 1080;
default_width = screenwidth / 2;
default_height = screenheight / 2;
load('stationary_equilibrium/RedColormap.mat');

%% Parameters
% Setting parameters
param = load_parameters();
% SE computation parameters
se_param = load_se_parameters(param);

%% Evolutionary dynamics algorithm to find stationary equilibrium %%
%% Some pre-processing
[prob_down_b_bj_up_o, zeta_down_u_o] = get_tensors(param, se_param);

%% Initialization
% Initial policy
pi_down_mu_alpha_u_k_up_a_init = get_pi_init(param, se_param);

% Initial type-state distribution
d_up_mu_alpha_u_k_init = get_d_init(param.k_bar, param, se_param);

%% Loop over alphas
i_alpha_comp = 1;
while i_alpha_comp <= param.n_alpha_comp
    if param.n_alpha_comp > 1
        fprintf('%%%%ALPHA = %.3f%%%%\n\n', param.Alpha(i_alpha_comp));
    end

    %% Initialization
    se_pi_down_mu_alpha_u_k_up_a = pi_down_mu_alpha_u_k_up_a_init;
    se_d_up_mu_alpha_u_k = d_up_mu_alpha_u_k_init;
    % Plot
    if se_param.plot
        se_pi_plot_fg = 1;
        se_pi_plot_pos = [0, 0, screenwidth, screenheight];
        plot_se_pi(se_pi_plot_fg, se_pi_plot_pos, RedColormap, se_pi_down_mu_alpha_u_k_up_a, param, se_param, i_alpha_comp);
        
        se_d_plot_fg = 2;
        se_d_plot_pos = [0, 0, screenwidth, screenheight];
        plot_se_d(se_d_plot_fg, se_d_plot_pos, se_d_up_mu_alpha_u_k, param, se_param, i_alpha_comp);
    end
    
    iter = 1;
    se_pi_error = inf;
    se_pi_error_hist = zeros(se_param.max_iter, 1);
    se_d_error = inf;
    se_d_error_hist = zeros(se_param.max_iter, 1);
    kappa_down_k_a_o_up_kn_pre_tax = zeros(se_param.n_k, se_param.n_a, se_param.n_o, se_param.n_k);
    kappa_down_k_a_o_up_kn = zeros(se_param.n_k, se_param.n_a, se_param.n_o, se_param.n_k);
    se_V_down_mu_alpha_u_k = zeros(param.n_mu, param.n_alpha, param.n_u, se_param.n_k);
    se_V_down_mu_alpha_u_k_next = zeros(param.n_mu, param.n_alpha, param.n_u, se_param.n_k);
    se_Q_down_mu_alpha_u_k_a = zeros(param.n_mu, param.n_alpha, param.n_u, se_param.n_k, se_param.n_a);
    br_pi_down_mu_alpha_u_k_up_a = zeros(param.n_mu, param.n_alpha, param.n_u, se_param.n_k, se_param.n_a);
    
    if param.limited_actions
        se_prob_down_k_a_up_b = zeros(se_param.n_k, se_param.n_a, se_param.n_k); 
        for i_k = 1 : se_param.n_k
            switch param.transition_mechanism
                case {22, 23}   % 0 or bid all
                    se_prob_down_k_a_up_b(i_k,1,1) = 1;
                    se_prob_down_k_a_up_b(i_k,2,i_k) = 1;
                case {24, 25}   % 0 or bid half
                    se_prob_down_k_a_up_b(i_k,1,1) = 1;
                    se_prob_down_k_a_up_b(i_k,2,round(i_k/2)) = 1;
                otherwise
                    for i_a = 1 : se_param.n_a
                        i_b = min([param.b_down_a(i_a) + 1, i_k]);
                        se_prob_down_k_a_up_b(i_k,i_a,i_b) = 1;
                    end
            end
        end
    end
    
    k_max_saturated = false;
    
    %% History of policy and type-state distribution
    if se_param.store_hist
        se_pi_hist = zeros(se_param.max_iter + 1, param.n_mu * param.n_alpha * param.n_u * se_param.n_k * se_param.n_a);
        se_pi_hist(1,:) = reshape(se_pi_down_mu_alpha_u_k_up_a, 1, []);
        se_d_hist = zeros(se_param.se_pi_max_iter + 1, param.n_mu * param.n_alpha * param.n_u * se_param.n_k);
        se_d_hist(1,:) = reshape(se_d_up_mu_alpha_u_k, 1, []);
    end
    
    %% Main loop of algorithm
    while (se_pi_error > se_param.se_pi_tol || se_d_error > se_param.se_d_tol) && iter <= se_param.max_iter
        %% Some pre-processing
        if param.limited_actions
            se_pi_down_mu_alpha_u_k_up_b = einsum('ijklm,lmn->ijkln', se_pi_down_mu_alpha_u_k_up_a, se_prob_down_k_a_up_b);
        else
            se_pi_down_mu_alpha_u_k_up_b = se_pi_down_mu_alpha_u_k_up_a;
        end
        
        % Distribution of others' bids
        se_nu_up_bj = einsum('ijklm,ijkl->m', se_pi_down_mu_alpha_u_k_up_b, se_d_up_mu_alpha_u_k);
        % Plot
        if se_param.plot
            se_nu_plot_fg = 3;
            se_nu_plot_pos = [0, 0, screenwidth, screenheight];
            plot_se_nu(se_nu_plot_fg, se_nu_plot_pos, se_nu_up_bj, param, se_param, i_alpha_comp);
        end
        
        % Probability of winning given bid
        gamma_down_b_up_o = einsum('ijk,jl->ikl', prob_down_b_bj_up_o, se_nu_up_bj);
        
        if param.limited_actions
            prob_down_k_a_up_o = einsum('ijk,kl->ijl', se_prob_down_k_a_up_b, gamma_down_b_up_o);
        else
            prob_down_k_a_up_o = einsum('ij,kl->kij', gamma_down_b_up_o, ones(se_param.n_k, 1));
        end
        
        %% Stage rewards
        reward_down_u_k_a = -einsum('ij,klj->ikl', zeta_down_u_o, prob_down_k_a_up_o);
        se_R_down_mu_alpha_u_k = einsum('ijklm,klm->ijkl', se_pi_down_mu_alpha_u_k_up_a, reward_down_u_k_a);
        
        %% Pre-taxation karma transition function
        switch param.transition_mechanism
            case 0  % Pay bid to peer
                for i_k = 1 : se_param.n_k
                    for i_b = 1 : i_k
                        prob_down_o_up_kn_pre_tax = zeros(se_param.n_o, 2 * se_param.n_k - 1);
                        prob_down_o_up_kn_pre_tax(1,i_k-i_b+1) = 1.0;
                        prob_down_o_up_kn_pre_tax(2,i_k+i_b-1) = 0.5 * se_nu_up_bj(i_b);
                        prob_down_o_up_kn_pre_tax(2,i_k+i_b:i_k+se_param.n_k-1) = se_nu_up_bj(i_b+1:se_param.n_k);
                        if gamma_down_b_up_o(i_b,2) > 0
                            prob_down_o_up_kn_pre_tax(2,:) = prob_down_o_up_kn_pre_tax(2,:) / gamma_down_b_up_o(i_b,2);
                        end
                        
                        kappa_down_k_a_o_up_kn_pre_tax(i_k,i_b,:,1:se_param.n_k-1) = prob_down_o_up_kn_pre_tax(:,1:se_param.n_k-1);
                        kappa_down_k_a_o_up_kn_pre_tax(i_k,i_b,:,se_param.n_k) = sum(prob_down_o_up_kn_pre_tax(:,se_param.n_k:end), 2);
                    end
                end
                
            case 1  % Pay bid to society
                for i_k = 1 : se_param.n_k
                    for i_b = 1 : i_k
                        prob_down_o_up_kn_pre_tax = zeros(se_param.n_o, se_param.n_k);
                        prob_down_o_up_kn_pre_tax(1,i_k-i_b+1) = 1.0;
                        prob_down_o_up_kn_pre_tax(2,i_k) = 1.0;
                        
                        kappa_down_k_a_o_up_kn_pre_tax(i_k,i_b,:,:) = prob_down_o_up_kn_pre_tax;
                    end
                end
                
            case 2  % Pay 2nd price to society
                for i_k = 1 : se_param.n_k
                    for i_b = 1 : i_k
                        prob_down_o_up_kn_pre_tax = zeros(se_param.n_o, se_param.n_k);
                        prob_down_o_up_kn_pre_tax(1,i_k-i_b+2:i_k) = flip(se_nu_up_bj(1:i_b-1));
                        prob_down_o_up_kn_pre_tax(1,i_k-i_b+1) = 0.5 * se_nu_up_bj(i_b);
                        if gamma_down_b_up_o(i_b,1) > 0
                            prob_down_o_up_kn_pre_tax(1,:) = prob_down_o_up_kn_pre_tax(1,:) / gamma_down_b_up_o(i_b,1);
                        end
                        prob_down_o_up_kn_pre_tax(2,i_k) = 1.0;
                        
                        kappa_down_k_a_o_up_kn_pre_tax(i_k,i_b,:,:) = prob_down_o_up_kn_pre_tax;
                    end
                end
            
            case 3  % Pay 1 to peer
                for i_k = 1 : se_param.n_k
                    for i_b = 1 : i_k
                        prob_down_o_up_kn_pre_tax = zeros(se_param.n_o, se_param.n_k + 1);
                        if i_b == 1
                            prob_down_o_up_kn_pre_tax(1,i_k) = 1;
                            prob_down_o_up_kn_pre_tax(2,i_k) = 0.5 * se_nu_up_bj(i_b);
                            prob_down_o_up_kn_pre_tax(2,i_k+1) = gamma_down_b_up_o(i_b,2) - 0.5 * se_nu_up_bj(i_b);
                            if gamma_down_b_up_o(i_b,2) > 0
                                prob_down_o_up_kn_pre_tax(2,:) = prob_down_o_up_kn_pre_tax(2,:) / gamma_down_b_up_o(i_b,2);
                            end
                        else
                            prob_down_o_up_kn_pre_tax(1,i_k-1) = 1;
                            prob_down_o_up_kn_pre_tax(2,i_k+1) = 1;
                        end
                        
                        kappa_down_k_a_o_up_kn_pre_tax(i_k,i_b,:,1:se_param.n_k-1) = prob_down_o_up_kn_pre_tax(:,1:se_param.n_k-1);
                        kappa_down_k_a_o_up_kn_pre_tax(i_k,i_b,:,se_param.n_k) = sum(prob_down_o_up_kn_pre_tax(:,se_param.n_k:end), 2);
                    end
                end
                
            case 4  % Pay other's bid to peer
                for i_k = 1 : se_param.n_k
                    for i_b = 1 : i_k
                        prob_down_o_up_kn_pre_tax = zeros(se_param.n_o, 2 * se_param.n_k - 1);
                        
                        prob_down_o_up_kn_pre_tax(1,i_k-i_b+2:i_k) = flip(se_nu_up_bj(1:i_b-1));
                        prob_down_o_up_kn_pre_tax(1,i_k-i_b+1) = 0.5 * se_nu_up_bj(i_b);
                        if gamma_down_b_up_o(i_b,1) > 0
                            prob_down_o_up_kn_pre_tax(1,:) = prob_down_o_up_kn_pre_tax(1,:) / gamma_down_b_up_o(i_b,1);
                        end
                        prob_down_o_up_kn_pre_tax(2,i_k+i_b-1) = prob_down_o_up_kn_pre_tax(i_k+i_b-1) + gamma_down_b_up_o(i_b,2);
                        
                        kappa_down_k_a_o_up_kn_pre_tax(i_k,i_b,1:se_param.n_k-1) = prob_down_o_up_kn_pre_tax(1:se_param.n_k-1);
                        kappa_down_k_a_o_up_kn_pre_tax(i_k,i_b,se_param.n_k) = sum(prob_down_o_up_kn_pre_tax(se_param.n_k:end));
                    end
                end
            
            case 5  % Pay bid to society always (winner and loser)
                for i_k = 1 : se_param.n_k
                    for i_b = 1 : i_k
                        prob_down_o_up_kn_pre_tax = zeros(se_param.n_o, se_param.n_k);
                        prob_down_o_up_kn_pre_tax(1,i_k-i_b+1) = 1.0;
                        prob_down_o_up_kn_pre_tax(2,i_k-i_b+1) = 1.0;
                        
                        kappa_down_k_a_o_up_kn_pre_tax(i_k,i_b,:,:) = prob_down_o_up_kn_pre_tax;
                    end
                end
                
            case 10  % Loser pays as bid to peer
                for i_k = 1 : se_param.n_k
                    for i_b = 1 : i_k
                        prob_down_o_up_kn_pre_tax = zeros(se_param.n_o, 2 * se_param.n_k - 1);
                        prob_down_o_up_kn_pre_tax(1,i_k:i_k+i_b-2) = se_nu_up_bj(1:i_b-1);
                        prob_down_o_up_kn_pre_tax(1,i_k+i_b-1) = 0.5 * se_nu_up_bj(i_b);
                        if gamma_down_b_up_o(i_b,1) > 0
                            prob_down_o_up_kn_pre_tax(1,:) = prob_down_o_up_kn_pre_tax(1,:) / gamma_down_b_up_o(i_b,1);
                        end
                        prob_down_o_up_kn_pre_tax(2,i_k-i_b+1) = 1;
                        
                        kappa_down_k_a_o_up_kn_pre_tax(i_k,i_b,:,1:se_param.n_k-1) = prob_down_o_up_kn_pre_tax(:,1:se_param.n_k-1);
                        kappa_down_k_a_o_up_kn_pre_tax(i_k,i_b,:,se_param.n_k) = sum(prob_down_o_up_kn_pre_tax(:,se_param.n_k:end), 2);
                    end
                end
                
            case 11  % Loser pays as bid to society
                for i_k = 1 : se_param.n_k
                    for i_b = 1 : i_k
                        prob_down_o_up_kn_pre_tax = zeros(se_param.n_o, se_param.n_k);
                        prob_down_o_up_kn_pre_tax(1,i_k) = 1.0;
                        prob_down_o_up_kn_pre_tax(2,i_k-i_b+1) = 1.0;
                        
                        kappa_down_k_a_o_up_kn_pre_tax(i_k,i_b,:,:) = prob_down_o_up_kn_pre_tax;
                    end
                end
            
            case {20, 30}   % Discrete action bid, pay bid to peer, fixed bids as per parameter
                for i_k = 1 : se_param.n_k
                    for i_a = 1 : se_param.n_a
                        i_b = min([param.b_down_a(i_a) + 1, i_k]);
                        
                        prob_down_o_up_kn_pre_tax = zeros(se_param.n_o, 2 * se_param.n_k - 1);
                        prob_down_o_up_kn_pre_tax(1,i_k-i_b+1) = 1;
                        prob_down_o_up_kn_pre_tax(2,i_k+i_b-1) = 0.5 * se_nu_up_bj(i_b);
                        prob_down_o_up_kn_pre_tax(2,i_k+i_b:i_k+se_param.n_k-1) = se_nu_up_bj(i_b+1:se_param.n_k);
                        if gamma_down_b_up_o(i_b,2) > 0
                            prob_down_o_up_kn_pre_tax(2,:) = prob_down_o_up_kn_pre_tax(2,:) / gamma_down_b_up_o(i_b,2);
                        end
                        
                        kappa_down_k_a_o_up_kn_pre_tax(i_k,i_a,:,1:se_param.n_k-1) = prob_down_o_up_kn_pre_tax(:,1:se_param.n_k-1);
                        kappa_down_k_a_o_up_kn_pre_tax(i_k,i_a,:,se_param.n_k) = sum(prob_down_o_up_kn_pre_tax(:,se_param.n_k:end), 2);
                    end
                end
                
            case {21, 31}   % Discrete action bid, pay bid to society, fixed bids as per parameter
                for i_k = 1 : se_param.n_k
                    for i_a = 1 : se_param.n_a
                        i_b = min([param.b_down_a(i_a) + 1, i_k]);
                        
                        prob_down_o_up_kn_pre_tax = zeros(se_param.n_o, se_param.n_k);
                        prob_down_o_up_kn_pre_tax(1,i_k-i_b+1) = 1.0;
                        prob_down_o_up_kn_pre_tax(2,i_k) = 1.0;
                        
                        kappa_down_k_a_o_up_kn_pre_tax(i_k,i_a,:,:) = prob_down_o_up_kn_pre_tax;
                    end
                end
                
            case {22, 24} % Binary bid, pay bid to peer, 0 or bid all / bid half
                for i_k = 1 : se_param.n_k
                    for i_a = 1 : se_param.n_a
                        if i_a == 1
                            i_b = 1;
                        else
                            switch param.transition_mechanism
                                case 22
                                    i_b = i_k;
                                case 24
                                    i_b = round(i_k / 2);
                            end
                        end
                        
                        prob_down_o_up_kn_pre_tax = zeros(se_param.n_o, 2 * se_param.n_k - 1);
                        prob_down_o_up_kn_pre_tax(1,i_k-i_b+1) = 1;
                        prob_down_o_up_kn_pre_tax(2,i_k+i_b-1) = 0.5 * se_nu_up_bj(i_b);
                        prob_down_o_up_kn_pre_tax(2,i_k+i_b:i_k+se_param.n_k-1) = se_nu_up_bj(i_b+1:se_param.n_k);
                        if gamma_down_b_up_o(i_b,2) > 0
                            prob_down_o_up_kn_pre_tax(2,:) = prob_down_o_up_kn_pre_tax(2,:) / gamma_down_b_up_o(i_b,2);
                        end
                        
                        kappa_down_k_a_o_up_kn_pre_tax(i_k,i_a,:,1:se_param.n_k-1) = prob_down_o_up_kn_pre_tax(:,1:se_param.n_k-1);
                        kappa_down_k_a_o_up_kn_pre_tax(i_k,i_a,:,se_param.n_k) = sum(prob_down_o_up_kn_pre_tax(:,se_param.n_k:end), 2);
                    end
                end
                
            case {23, 25} % Binary, pay bid to society, 0 or bid all / bid half
                for i_k = 1 : se_param.n_k
                    for i_a = 1 : se_param.n_a
                        if i_a == 1
                            i_b = 1;
                        else
                            switch param.transition_mechanism
                                case 23
                                    i_b = i_k;
                                case 25
                                    i_b = round(i_k / 2);
                            end
                        end
                        
                        prob_down_o_up_kn_pre_tax = zeros(se_param.n_o, se_param.n_k);
                        prob_down_o_up_kn_pre_tax(1,i_k-i_b+1) = 1.0;
                        prob_down_o_up_kn_pre_tax(2,i_k) = 1.0;
                        
                        kappa_down_k_a_o_up_kn_pre_tax(i_k,i_a,:,:) = prob_down_o_up_kn_pre_tax;
                    end
                end
                
            case 32 % 3-way bid (gain-maintain-spend) pay bid to society, maintain bid matches average payment
                for i_k = 1 : se_param.n_k
                    for i_a = 1 : se_param.n_a
                        prob_down_o_up_kn_pre_tax = zeros(se_param.n_o, se_param.n_k);
                        for i_b = 1 : i_k
                            prob_down_o_up_kn_pre_tax(1,i_k-i_b+1) = se_prob_down_k_a_up_b(i_k,i_a,i_b) * gamma_down_b_up_o(i_b,1);
                        end
                        if prob_down_k_a_up_o(i_k,i_a,1) > 0
                            prob_down_o_up_kn_pre_tax(1,:) = prob_down_o_up_kn_pre_tax(1,:) / prob_down_k_a_up_o(i_k,i_a,1);
                        end
                        prob_down_o_up_kn_pre_tax(2,i_k) = 1.0;
                        
                        kappa_down_k_a_o_up_kn_pre_tax(i_k,i_a,:,:) = prob_down_o_up_kn_pre_tax;
                    end
                end
                
                p_bar = dot(se_nu_up_bj .* gamma_down_b_up_o(:,1), se_param.K);
                p_bar_low = floor(p_bar);
                p_bar_high = p_bar_low + 1;
                f_low = p_bar_high - p_bar;
                f_high = 1 - f_low;
                se_prob_down_k_a_up_b(:,2,:) = 0;
                for i_k = 1 : se_param.n_k
                    i_b_low = min([p_bar_low + 1, i_k]);
                    i_b_high = min([p_bar_high + 1, i_k]);
                    se_prob_down_k_a_up_b(i_k,2,i_b_low) = f_low;
                    se_prob_down_k_a_up_b(i_k,2,i_b_high) = se_prob_down_k_a_up_b(i_k,2,i_b_high) + f_high;
                end
        end
        
        %% Post-taxation karma transition function
        if param.tax == 0
            kappa_down_k_a_o_up_kn_pre_redist = kappa_down_k_a_o_up_kn_pre_tax;
        else
            tax_down_k = param.tax * se_param.K.^2;
            tax_low_down_k = floor(tax_p_down_k);
            tax_high_down_k = tax_low_down_k + 1;
            tax_f_low = tax_high_down_k - tax_down_k;
            tax_f_high = tax_down_k - tax_low_down_k;
            prob_down_kn_pre_tax_up_kn_pre_redist = zeros(se_param.n_k, se_param.n_k);
            for i_k = 1 : se_param.n_k
                i_kn_low = max([i_k - tax_low_down_k(i_k), 1]);
                i_kn_high = max([i_k - tax_high_down_k(i_k), 1]);
                prob_down_kn_pre_tax_up_kn_pre_redist(i_k,i_kn_low) = tax_f_low(i_k);
                prob_down_kn_pre_tax_up_kn_pre_redist(i_k,i_kn_high) = prob_down_kn_pre_tax_up_kn_pre_redist(i_k,i_kn_high) + tax_f_high(i_k);
            end
            kappa_down_k_a_o_up_kn_pre_redist = einsum('ijkl,lm->ijkm', kappa_down_k_a_o_up_kn_pre_tax, prob_down_kn_pre_tax_up_kn_pre_redist);
        end
        
        %% Post-redistribution karma transition function
        prob_down_k_a_up_kn_pre_redist = einsum('ijkl,ijk->ijl', kappa_down_k_a_o_up_kn_pre_redist, prob_down_k_a_up_o);
        prob_up_k_a = einsum('ijkl,ijklm->lm', se_d_up_mu_alpha_u_k, se_pi_down_mu_alpha_u_k_up_a);
        prob_up_kn_pre_redist = einsum('ij,ijl->l', prob_up_k_a, prob_down_k_a_up_kn_pre_redist);
        p_bar = max([param.k_bar - dot(prob_up_kn_pre_redist, se_param.K), 0.0]);
        p_bar_low = floor(p_bar);
        p_bar_high = p_bar_low + 1;
        saturation_adjustment_term = 0;
        for i = 1 : p_bar_low
            saturation_adjustment_term = saturation_adjustment_term + prob_up_kn_pre_redist(se_param.n_k - i) * i;
        end
        p_bar_adjusted = (p_bar - saturation_adjustment_term) / sum(prob_up_kn_pre_redist(1:se_param.n_k-p_bar_high));
        p_bar_adjusted_low = floor(p_bar_adjusted);
        p_bar_adjusted_high = p_bar_adjusted_low + 1;
        f_low = p_bar_adjusted_high - p_bar_adjusted;
        f_high =  1 - f_low;
        prob_down_kn_pre_redist_up_kn = zeros(se_param.n_k, se_param.n_k);
        for i_k = 1 : se_param.n_k
            i_kn_low = min([i_k + p_bar_adjusted_low, se_param.n_k]);
            i_kn_high = min([i_k + p_bar_adjusted_high, se_param.n_k]);
            prob_down_kn_pre_redist_up_kn(i_k,i_kn_low) = f_low;
            prob_down_kn_pre_redist_up_kn(i_k,i_kn_high) = prob_down_kn_pre_redist_up_kn(i_k,i_kn_high) + f_high;
        end
        kappa_down_k_a_o_up_kn = einsum('ijkl,lm->ijkm', kappa_down_k_a_o_up_kn_pre_redist, prob_down_kn_pre_redist_up_kn);
        
        %% Stochastic matrix
        prob_down_k_a_up_kn = einsum('ijkl,ijk->ijl', kappa_down_k_a_o_up_kn, prob_down_k_a_up_o);
        prob_down_mu_u_k_a_up_un_kn = einsum('ijk,lmn->ijlmkn', param.phi_down_mu_u_up_un, prob_down_k_a_up_kn);
        se_P_down_mu_alpha_u_k_up_un_kn = einsum('ijklm,iklmno->ijklno', se_pi_down_mu_alpha_u_k_up_a, prob_down_mu_u_k_a_up_un_kn);

        %% Value function
        % Get individually for each alpha type
        for i_alpha = 1 : param.n_alpha
            alpha = param.Alpha(max([i_alpha, i_alpha_comp]));
            V_iter = 1;
            V_error = inf;
            while V_error > se_param.V_tol && V_iter <= se_param.V_max_iter
                if alpha == 1   % alpha = 1 is treated as an average cost per stage problem
                    if iter <= 10 || mod(iter, 1) == 0
                        for i_mu = 1 : param.n_mu
                            rel_V = se_R_down_mu_alpha_u_k(i_mu,i_alpha,1,se_param.i_k_bar) + einsum('ijklmn,ijmn->ijkl', se_P_down_mu_alpha_u_k_up_un_kn(i_mu,i_alpha,1,se_param.i_k_bar,:,:), se_V_down_mu_alpha_u_k(i_mu,i_alpha,:,:));
                            se_V_down_mu_alpha_u_k_next(i_mu,i_alpha,:,:) = se_R_down_mu_alpha_u_k(i_mu,i_alpha,:,:) + einsum('ijklmn,ijmn->ijkl', se_P_down_mu_alpha_u_k_up_un_kn(i_mu,i_alpha,:,:,:,:), se_V_down_mu_alpha_u_k(i_mu,i_alpha,:,:)) - rel_V;
                        end
                    else
                        se_V_down_mu_alpha_u_k_next = se_V_down_mu_alpha_u_k;
                    end
                else
                    se_V_down_mu_alpha_u_k_next(:,i_alpha,:,:) = se_R_down_mu_alpha_u_k(:,i_alpha,:,:) + alpha * einsum('ijklmn,ijmn->ijkl', se_P_down_mu_alpha_u_k_up_un_kn(:,i_alpha,:,:,:,:), se_V_down_mu_alpha_u_k(:,i_alpha,:,:));
                end
                V_error = norm(reshape(se_V_down_mu_alpha_u_k(:,i_alpha,:,:) - se_V_down_mu_alpha_u_k_next(:,i_alpha,:,:), 1, []), inf);
                se_V_down_mu_alpha_u_k(:,i_alpha,:,:) = se_V_down_mu_alpha_u_k_next(:,i_alpha,:,:);
                V_iter = V_iter + 1;
            end
            if V_iter > se_param.V_max_iter
                fprintf('\nWARNING: Bellman recursion did not converge! Error: %f\n\n', V_error);
            end
        end

        %% Single-stage deviation rewards
        r_down_mu_alpha_u_k_a = einsum('ijk,lm->lmijk', reward_down_u_k_a, ones(param.n_mu, param.n_alpha));
        future_r_down_mu_alpha_u_k_a = einsum('ijklmn,iomn->iojkl', prob_down_mu_u_k_a_up_un_kn, se_V_down_mu_alpha_u_k);
        for i_alpha = 1 : param.n_alpha
            alpha = param.Alpha(max([i_alpha, i_alpha_comp]));
            se_Q_down_mu_alpha_u_k_a(:,i_alpha,:,:) = r_down_mu_alpha_u_k_a(:,i_alpha,:,:) + alpha * future_r_down_mu_alpha_u_k_a(:,i_alpha,:,:);
        end

        %% Perturbed best response policy
        lambda_Q_down_mu_alpha_u_k_a = se_param.lambda * se_Q_down_mu_alpha_u_k_a;
        for i_k = 1 : se_param.n_k
            if param.limited_actions    % Limited actions are always possible
                i_max_a = se_param.n_a;
            else
                i_max_a = i_k;
            end
            br_pi_down_mu_alpha_u_k_up_a(:,:,:,i_k,1:i_max_a) = exp(lambda_Q_down_mu_alpha_u_k_a(:,:,:,i_k,1:i_max_a) - max(lambda_Q_down_mu_alpha_u_k_a(:,:,:,i_k,1:i_max_a), [], 5)); % Subtract max for numerical stability
            assert(max(br_pi_down_mu_alpha_u_k_up_a(:)) < inf, 'Numerics exploding - decrease lambda');  % Just in case
        end
        br_pi_down_mu_alpha_u_k_up_a = br_pi_down_mu_alpha_u_k_up_a ./ sum(br_pi_down_mu_alpha_u_k_up_a, 5);
        
        % Plot
        if se_param.plot
            br_pi_plot_fg = 4;
            br_pi_plot_pos = [0, 0, screenwidth, screenheight];
            plot_br_pi(br_pi_plot_fg, br_pi_plot_pos, RedColormap, br_pi_down_mu_alpha_u_k_up_a, param, se_param, i_alpha_comp);
        end
        
        %% Next policy
        se_pi_down_mu_alpha_u_k_up_a_next = (1 - se_param.delta * se_param.dt) * se_pi_down_mu_alpha_u_k_up_a + se_param.delta * se_param.dt * br_pi_down_mu_alpha_u_k_up_a;
        se_pi_diff = se_pi_down_mu_alpha_u_k_up_a_next - se_pi_down_mu_alpha_u_k_up_a;
        se_pi_error = norm(reshape(se_pi_diff, 1, []), inf);
        
        %% Next type-state distribution
        se_d_up_mu_alpha_u_k_next = einsum('ijkl,ijklmn->ijmn', se_d_up_mu_alpha_u_k, se_P_down_mu_alpha_u_k_up_un_kn);
        se_d_up_mu_alpha_u_k_next = se_d_up_mu_alpha_u_k_next / sum(se_d_up_mu_alpha_u_k_next(:));
        se_d_up_mu_alpha_u_k_next = (1 - se_param.dt) * se_d_up_mu_alpha_u_k + se_param.dt * se_d_up_mu_alpha_u_k_next;
        se_d_error = norm(reshape(se_d_up_mu_alpha_u_k_next - se_d_up_mu_alpha_u_k, 1, []), inf);
        
        % Detect if karma is saturating at current k_max
        se_sigma_up_k = squeeze(sum(se_d_up_mu_alpha_u_k_next, [1 2 3]));
        if se_sigma_up_k(end) > se_param.max_sigma_k_max
            fprintf('\nWARNING: Too many agents saturating, consider increasing k_max. Prob[k_max] = %f\n\n', se_sigma_up_k(end));
        end
        

        %% Update SE policy & distribution candidates
        se_pi_down_mu_alpha_u_k_up_a = se_pi_down_mu_alpha_u_k_up_a_next;
        se_d_up_mu_alpha_u_k = se_d_up_mu_alpha_u_k_next;
        % Plot
        if se_param.plot
            plot_se_pi(se_pi_plot_fg, se_pi_plot_pos, RedColormap, se_pi_down_mu_alpha_u_k_up_a, param, se_param, i_alpha_comp);
            plot_se_d(se_d_plot_fg, se_d_plot_pos, se_d_up_mu_alpha_u_k, param, se_param, i_alpha_comp);
            drawnow;
        end
        
        %% Finish up iteration
        % Display status
        fprintf('Iteration %d policy error %f distribution error %f\n', iter, se_pi_error, se_d_error);
        
        % Update history
        se_pi_error_hist(iter) = se_pi_error;
        se_d_error_hist(iter) = se_d_error;
        if se_param.store_hist
            se_pi_hist(iter+1,:) = reshape(se_pi_down_mu_alpha_u_k_up_a, 1, []);
            se_d_hist(iter+1,:) = reshape(se_d_up_mu_alpha_u_k, 1, []);
        end
        
        % Increment iteration count
        iter = iter + 1;
    end
    
    %% Remove 'extra' history
    se_pi_error_hist(iter:end) = [];
    se_d_error_hist(iter:end) = [];
    if se_param.store_hist
        se_pi_hist(iter+1:end,:) = [];
        se_d_hist(iter+1:end,:) = [];
    end
    
    %% Some final results
    % Marginal urgency and karma distribution
    se_upsilon_up_u = squeeze(sum(se_d_up_mu_alpha_u_k, [1 2 4]));
    se_sigma_down_mu_alpha_up_k = zeros(param.n_mu, param.n_alpha, se_param.n_k);
    for i_mu = 1 : param.n_mu
        for i_alpha = 1 : param.n_alpha
            se_sigma_down_mu_alpha_up_k(i_mu,i_alpha,:) = squeeze(sum(se_d_up_mu_alpha_u_k(i_mu,i_alpha,:,:), 3));
            se_sigma_down_mu_alpha_up_k(i_mu,i_alpha,:) = se_sigma_down_mu_alpha_up_k(i_mu,i_alpha,:) / sum(se_sigma_down_mu_alpha_up_k(i_mu,i_alpha,:));
        end
    end
    
    %% Make sure value function is up-to-date for alpha = 1 (since it is updated periodically)
    % Also check that identical average stage cost assumption is satisfied
    % (see Bertsekas, DPOC Vol II, Chapter 4)
    for i_alpha = 1 : param.n_alpha
        alpha = param.Alpha(max([i_alpha, i_alpha_comp]));
        if alpha == 1
            V_iter = 1;
            V_error = inf;
            while V_error > se_param.V_tol && V_iter <= se_param.V_max_iter
                for i_mu = 1 : param.n_mu
                    rel_V = se_R_down_mu_alpha_u_k(i_mu,i_alpha,1,se_param.i_k_bar) + einsum('ijklmn,ijmn->ijkl', se_P_down_mu_alpha_u_k_up_un_kn(i_mu,i_alpha,1,se_param.i_k_bar,:,:), se_V_down_mu_alpha_u_k(i_mu,i_alpha,:,:));
                    se_V_down_mu_alpha_u_k_next(i_mu,i_alpha,:,:) = se_R_down_mu_alpha_u_k(i_mu,i_alpha,:,:) + einsum('ijklmn,ijmn->ijkl', se_P_down_mu_alpha_u_k_up_un_kn(i_mu,i_alpha,:,:,:,:), se_V_down_mu_alpha_u_k(i_mu,i_alpha,:,:)) - rel_V;
                end
                V_error = norm(reshape(se_V_down_mu_alpha_u_k(:,i_alpha,:,:) - se_V_down_mu_alpha_u_k_next(:,i_alpha,:,:), 1, []), inf);
                se_V_down_mu_alpha_u_k(:,i_alpha,:,:) = se_V_down_mu_alpha_u_k_next(:,i_alpha,:,:);
                V_iter = V_iter + 1;
            end
            if V_iter > se_param.V_max_iter
                fprintf('\nWARNING: Bellman recursion did not converge! Error: %f\n\n', V_error);
            end
            
            % Check if identical avearge stage cost assumption is satisfied
            se_V_check_down_mu_alpha_u_k = zeros(param.n_mu, param.n_alpha, param.n_u, se_param.n_k);
            for i_mu = 1 : param.n_mu
                se_V_check_down_mu_alpha_u_k(i_mu,i_alpha,:,:) = se_R_down_mu_alpha_u_k(i_mu,i_alpha,:,:) + einsum('ijklmn,ijmn->ijkl', se_P_down_mu_alpha_u_k_up_un_kn(i_mu,i_alpha,:,:,:,:), se_V_down_mu_alpha_u_k(i_mu,i_alpha,:,:)) - se_V_down_mu_alpha_u_k(i_mu,i_alpha,:,:);
                V_max_dev = max(se_V_check_down_mu_alpha_u_k(i_mu,i_alpha,:,:), [], 'all') - min(se_V_check_down_mu_alpha_u_k(i_mu,i_alpha,:,:), [], 'all');
                if V_max_dev > se_param.V_tol
                    fprintf('\nWARNING: Identical average stage cost assumption unsatisfied! Max deviation: %f\n\n', V_max_dev);
                end
            end
        end
    end
    
    %% Plot remaining statistics
    if se_param.plot
        % SE karma distribution plot
        se_sigma_plot_fg = 5;
        se_sigma_plot_pos = [0, 0, screenwidth, screenheight];
        plot_se_sigma(se_sigma_plot_fg, se_sigma_plot_pos, se_sigma_down_mu_alpha_up_k, param, se_param, i_alpha_comp);
        
        % SE value function plot
        se_V_plot_fg = 6;prob_down_o_up_kn_pre_tax(1,i_k+p_bar_high-i_b_high+1)
        se_V_plot_pos = [0, 0, screenwidth, screenheight];
        plot_se_V(se_V_plot_fg, se_V_plot_pos, se_V_down_mu_alpha_u_k, param, se_param, i_alpha_comp);

        % SE single-stage deviation rewards plot
        se_Q_plot_fg = 7;
        se_Q_plot_pos = [0, 0, screenwidth, screenheight];
        plot_se_Q(se_Q_plot_fg, se_Q_plot_pos, parula, se_Q_down_mu_alpha_u_k_a, param, se_param, i_alpha_comp);

        % SE state transitions plot
        se_P_plot_fg = 8;
        se_P_plot_pos = [0, 0, screenwidth, screenheight];
        plot_se_P(se_P_plot_fg, se_P_plot_pos, RedColormap, se_P_down_mu_alpha_u_k_up_un_kn, param, se_param, i_alpha_comp);

        % SE policy error plot
        se_pi_error_plot_fg = 9;
        se_pi_error_plot_pos = [0, 0, screenwidth, screenheight];
        plot_se_pi_error(se_pi_error_plot_fg, se_pi_error_plot_pos, se_pi_error_hist, param, i_alpha_comp);
    end
    
    % Store end results
    if se_param.save
        file_str = ['stationary_equilibrium/results/k_bar_', num2str(param.k_bar, '%02d')];
        if param.n_mu > 1
            file_str = [file_str, '_w'];
            for i_mu = 1 : param.n_mu
                file_str = [file_str, '_', num2str(param.w_up_mu(i_mu), '%.2f')];
            end
        elseif param.n_alpha > 1
            file_str = [file_str, '_z'];
            for i_alpha = 1 : param.n_alpha
                file_str = [file_str, '_', num2str(param.z_up_alpha(i_alpha), '%.2f')];
            end
        else
            if alpha > 0.99 && alpha < 1
                alpha_str = num2str(alpha, '%.3f');
            else
                alpha_str = num2str(alpha, '%.2f');
            end
            file_str = [file_str, '_alpha_', alpha_str];
        end
        file_str = [file_str, '.mat'];
        save(file_str);
    end
    
    i_alpha_comp = i_alpha_comp + 1;
end
i_alpha_comp = i_alpha_comp - 1;

%% If plotting is not active, plot everything at the end
if ~se_param.plot
    % SE policy plot
    se_pi_plot_fg = 1;
    se_pi_plot_pos = [0, 0, screenwidth, screenheight];
    plot_se_pi(se_pi_plot_fg, se_pi_plot_pos, RedColormap, se_pi_down_mu_alpha_u_k_up_a, param, se_param, i_alpha_comp);
    
    % SE stationary distribution plot
    se_d_plot_fg = 2;
    se_d_plot_pos = [0, 0, screenwidth, screenheight];
    plot_se_d(se_d_plot_fg, se_d_plot_pos, se_d_up_mu_alpha_u_k, param, se_param, i_alpha_comp);
    
    % SE distribution of bids plot
    se_nu_plot_fg = 3;
    se_nu_plot_pos = [0, 0, screenwidth, screenheight];
    plot_se_nu(se_nu_plot_fg, se_nu_plot_pos, se_nu_up_bj, param, se_param, i_alpha_comp);
    
    % Best response policy plot
    br_pi_plot_fg = 4;
    br_pi_plot_pos = [0, 0, screenwidth, screenheight];
    plot_br_pi(br_pi_plot_fg, br_pi_plot_pos, RedColormap, br_pi_down_mu_alpha_u_k_up_a, param, se_param, i_alpha_comp);

    % SE karma distribution plot
    se_sigma_plot_fg = 5;
    se_sigma_plot_pos = [0, 0, screenwidth, screenheight];
    plot_se_sigma(se_sigma_plot_fg, se_sigma_plot_pos, se_sigma_down_mu_alpha_up_k, param, se_param, i_alpha_comp);
    
    % SE payoffs plot
    se_V_plot_fg = 6;
    se_V_plot_pos = [0, 0, screenwidth, screenheight];
    plot_se_V(se_V_plot_fg, se_V_plot_pos, se_V_down_mu_alpha_u_k, param, se_param, i_alpha_comp);
    
    % SE payoffs per bid plot
    se_Q_plot_fg = 7;
    se_Q_plot_pos = [0, 0, screenwidth, screenheight];
    plot_se_Q(se_Q_plot_fg, se_Q_plot_pos, parula, se_Q_down_mu_alpha_u_k_a, param, se_param, i_alpha_comp);

    % SE state transitions plot
    se_P_plot_fg = 8;
    se_P_plot_pos = [0, 0, screenwidth, screenheight];
    plot_se_P(se_P_plot_fg, se_P_plot_pos, RedColormap, se_P_down_mu_alpha_u_k_up_un_kn, param, se_param, i_alpha_comp);

    % SE policy error plot
    se_pi_error_plot_fg = 9;
    se_pi_error_plot_pos = [0, 0, screenwidth, screenheight];
    plot_se_pi_error(se_pi_error_plot_fg, se_pi_error_plot_pos, se_pi_error_hist, param, i_alpha_comp);
end

%% Inform user when done
fprintf('DONE\n\n');