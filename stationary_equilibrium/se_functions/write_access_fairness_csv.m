% Write cost fairness to csv file
function write_access_fairness_csv(w, w_down_mu_alpha, param, fileprefix)
    % Header
    header = ["mu", "alpha", "w"];
    filename = [fileprefix, '-access-fairness.csv'];
    fout = fopen(filename, 'w');
    for i = 1 : length(header) - 1
        fprintf(fout, '%s,', header(i));
    end
    fprintf(fout, '%s\n', header(end));
    fclose(fout);

    % Data
    data = [-1, -1, w]; % Overall efficiency encoded as -1
    dlmwrite(filename, data, '-append');
    for i_mu = 1 : param.n_mu
        for i_alpha = 1 : param.n_alpha
            data = [i_mu, param.Alpha(i_alpha), w_down_mu_alpha(i_mu,i_alpha)];
            dlmwrite(filename, data, '-append');
        end
    end
end