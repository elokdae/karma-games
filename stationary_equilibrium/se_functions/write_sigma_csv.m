% Wrtie stationary karma distribution to csv file
function write_sigma_csv(sigma_down_mu_alpha_up_k, k_max, param, se_param, i_alpha_comp, fileprefix)
    % We put distribution of k >= k_max in k_max
    n_k = k_max + 1;
    sigma_down_mu_alpha_up_k(:,:,n_k) = sum(sigma_down_mu_alpha_up_k(:,:,n_k:end), 3);
    sigma_down_mu_alpha_up_k(:,:,n_k+1) = sigma_down_mu_alpha_up_k(:,:,n_k);
    
    % Header
    header = ["mu", "alpha", "k", "k2", "P(k)"];
    filename = [fileprefix, '-karma-dist.csv'];
    fout = fopen(filename, 'w');
    for i = 1 : length(header) - 1
        fprintf(fout, '%s,', header(i));
    end
    fprintf(fout, '%s\n', header(end));
    fclose(fout);

    % Data
    for i_mu = 1 : param.n_mu
        MU = i_mu * ones(n_k + 1, 1);
        for i_alpha = 1 : param.n_alpha
            if param.n_alpha == 1
                alpha = param.Alpha(i_alpha_comp);
            else
                alpha = param.Alpha(i_alpha);
            end
            data = [MU, alpha * ones(n_k + 1, 1), se_param.K(1:n_k+1), se_param.K(1:n_k+1) - 0.5, squeeze(sigma_down_mu_alpha_up_k(i_mu,i_alpha,1:n_k+1))];
            dlmwrite(filename, data, '-append');
        end
    end
end