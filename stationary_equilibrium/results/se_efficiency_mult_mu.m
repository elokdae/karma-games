clc;
screenwidth = 1920;
screenheight = 1080;

%% Some parameters
w1_eff_comp = 0 : 0.1 : 1;
n_w1_eff_comp = length(w1_eff_comp);

file_start = 'stationary_equilibrium/results/se_U_1_2_3_6_11_phi1_0.90_0.00_0.00_0.00_0.10_phi2_0.80_0.00_0.00_0.20_0.00_phi3_0.50_0.00_0.50_0.00_0.00_phi4_0.00_1.00_0.00_0.00_0.00_pay_1/k_bar_10_w_';

%% Efficiency arrays
for i_w1_eff_comp = 1 : n_w1_eff_comp
    w1 = w1_eff_comp(i_w1_eff_comp);
    w2 = 1 - w1;
    file = [file_start, num2str(w1, '%.2f'), '_', num2str(w2, '%.2f'), '.mat'];
    if exist(file, 'file')
        load(file);
        
        if i_w1_eff_comp == 1
            se_access_eff = zeros(param.n_mu + 1, n_w1_eff_comp);
            se_cost_eff = zeros(param.n_mu + 1, n_w1_eff_comp);
            
            se_cost_eff_mu1_truthful = zeros(param.n_mu + 1, n_w1_eff_comp);
            se_cost_eff_mu2_truthful = zeros(param.n_mu + 1, n_w1_eff_comp);
        end
        
        %% SE access efficiency
        % Probability of winning per mu type
        for i_mu = 1 : param.n_mu
            prob_up_b = 1 / param.g_up_mu_alpha(i_mu,1) * einsum('ijkl,ijklm->m', se_d_up_mu_alpha_u_k(i_mu,1,:,:), se_pi_down_mu_alpha_u_k_up_b(i_mu,1,:,:,:));
            se_access_eff(i_mu,i_w1_eff_comp) = dot(prob_up_b, squeeze(gamma_down_b_up_o(:,1)));
        end
        
        % Overall probability of winning
        se_access_eff(end,i_w1_eff_comp) = dot(se_nu_up_bj, squeeze(gamma_down_b_up_o(:,1)));
        
        %% SE cost efficiency
        % Cost efficiency per mu type
        for i_mu = 1 : param.n_mu
            se_cost_eff(i_mu,i_w1_eff_comp) = 1 / param.g_up_mu_alpha(i_mu,1) * dot(reshape(se_d_up_mu_alpha_u_k(i_mu,1,:,:), [], 1), reshape(se_R_down_mu_alpha_u_k(i_mu,1,:,:), [], 1));
            
            % Truthful efficiencies
            if i_mu == 1
                se_cost_eff_mu1_truthful(i_mu,i_w1_eff_comp) = se_cost_eff(i_mu,i_w1_eff_comp);
            else
                se_cost_eff_mu2_truthful(i_mu,i_w1_eff_comp) = se_cost_eff(i_mu,i_w1_eff_comp);
            end
            
            % Non-truthful efficiencies
            non_truthful_e = 0;
            for i_u = 1 : param.n_u
                ratio_true = param.prob_down_mu_up_u(3-i_mu,i_u) / param.prob_down_mu_up_u(i_mu,i_u);
                ratio_true = min([ratio_true, 1]);
                ratio_true = max([ratio_true, 0]);

                non_truthful_e = non_truthful_e + ratio_true / param.g_up_mu_alpha(i_mu,1) * dot(squeeze(se_d_up_mu_alpha_u_k(i_mu,1,i_u,:)), squeeze(se_R_down_mu_alpha_u_k(i_mu,1,i_u,:)));

                non_truthul_r_down_k = einsum('ijklm,km->l', se_pi_down_mu_alpha_u_k_up_b(i_mu,1,i_u,:,:), reward_down_u_b(3-i_u,:));
                non_truthful_e = non_truthful_e + (1 - ratio_true) / param.g_up_mu_alpha(i_mu,1) * dot(squeeze(se_d_up_mu_alpha_u_k(i_mu,1,i_u,:)), non_truthul_r_down_k);
            end
            if i_mu == 1
                se_cost_eff_mu2_truthful(i_mu,i_w1_eff_comp) = non_truthful_e;
            else
                se_cost_eff_mu1_truthful(i_mu,i_w1_eff_comp) = non_truthful_e;
            end
        end
        
        % Overall cost efficiency
        for i_mu = 1 : param.n_mu
            if param.g_up_mu_alpha(i_mu,1) ~= 0
                se_cost_eff(end,i_w1_eff_comp) = se_cost_eff(end,i_w1_eff_comp) + param.g_up_mu_alpha(i_mu,1) * se_cost_eff(i_mu,i_w1_eff_comp);
                se_cost_eff_mu1_truthful(end,i_w1_eff_comp) = se_cost_eff_mu1_truthful(end,i_w1_eff_comp) + param.g_up_mu_alpha(i_mu,1) * se_cost_eff_mu1_truthful(i_mu,i_w1_eff_comp);
                se_cost_eff_mu2_truthful(end,i_w1_eff_comp) = se_cost_eff_mu2_truthful(end,i_w1_eff_comp) + param.g_up_mu_alpha(i_mu,1) * se_cost_eff_mu2_truthful(i_mu,i_w1_eff_comp);
            end
        end
    end
end

%% Plot
figure(1);
fig = gcf;
fig.Position = [0, 0, screenwidth, screenheight];
subplot(2,2,1);
plot(1 - w1_eff_comp, se_access_eff(1:param.n_mu,:), '--o', 'LineWidth', 2, 'MarkerSize', 10);
hold on;
plot(1 - w1_eff_comp, se_access_eff(end,:), 'LineWidth', 2);
axes = gca;
axis_semi_tight(axes, 1.2);
axes.XAxis.FontSize = 15;
axes.XAxis.FontName = 'ubuntu';
axes.YAxis.FontSize = 15;
axes.YAxis.FontName = 'ubuntu';
axes.XLabel.FontName = 'ubuntu';
axes.XLabel.FontWeight = 'bold';
axes.XLabel.String = 'Proportion of \mu_2';
axes.XLabel.FontSize = 20;
axes.YLabel.FontName = 'ubuntu';
axes.YLabel.FontWeight = 'bold';
axes.YLabel.String = 'Access efficiency';
axes.YLabel.FontSize = 20;

subplot(2,2,2);
plot(1 - w1_eff_comp, se_cost_eff(1:param.n_mu,:), '--o', 'LineWidth', 2, 'MarkerSize', 10);
hold on;
plot(1 - w1_eff_comp, se_cost_eff(end,:), 'LineWidth', 2);
axes = gca;
axis_semi_tight(axes, 1.2);
axes.XAxis.FontSize = 15;
axes.XAxis.FontName = 'ubuntu';
axes.YAxis.FontSize = 15;
axes.YAxis.FontName = 'ubuntu';
axes.XLabel.FontName = 'ubuntu';
axes.XLabel.FontWeight = 'bold';
axes.XLabel.String = 'Proportion of \mu_2';
axes.XLabel.FontSize = 20;
axes.YLabel.FontName = 'ubuntu';
axes.YLabel.FontWeight = 'bold';
axes.YLabel.String = 'Cost efficiency - all truthfull';
axes.YLabel.FontSize = 20;

subplot(2,2,3);
plot(1 - w1_eff_comp, se_cost_eff_mu1_truthful(1:param.n_mu,:), '--o', 'LineWidth', 2, 'MarkerSize', 10);
hold on;
plot(1 - w1_eff_comp, se_cost_eff_mu1_truthful(end,:), 'LineWidth', 2);
axes = gca;
axis_semi_tight(axes, 1.2);
axes.XAxis.FontSize = 15;
axes.XAxis.FontName = 'ubuntu';
axes.YAxis.FontSize = 15;
axes.YAxis.FontName = 'ubuntu';
axes.XLabel.FontName = 'ubuntu';
axes.XLabel.FontWeight = 'bold';
axes.XLabel.String = 'Proportion of \mu_2';
axes.XLabel.FontSize = 20;
axes.YLabel.FontName = 'ubuntu';
axes.YLabel.FontWeight = 'bold';
axes.YLabel.String = 'Cost efficiency - \mu_2 non-truthfull';
axes.YLabel.FontSize = 20;

subplot(2,2,4);
plot(1 - w1_eff_comp, se_cost_eff_mu2_truthful(1:param.n_mu,:), '--o', 'LineWidth', 2, 'MarkerSize', 10);
hold on;
plot(1 - w1_eff_comp, se_cost_eff_mu2_truthful(end,:), 'LineWidth', 2);
axes = gca;
axis_semi_tight(axes, 1.2);
axes.XAxis.FontSize = 15;
axes.XAxis.FontName = 'ubuntu';
axes.YAxis.FontSize = 15;
axes.YAxis.FontName = 'ubuntu';
axes.XLabel.FontName = 'ubuntu';
axes.XLabel.FontWeight = 'bold';
axes.XLabel.String = 'Proportion of \mu_2';
axes.XLabel.FontSize = 20;
axes.YLabel.FontName = 'ubuntu';
axes.YLabel.FontWeight = 'bold';
axes.YLabel.String = 'Cost efficiency - \mu_1 non-truthfull';
axes.YLabel.FontSize = 20;