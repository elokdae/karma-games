% Accumulates the number of passes in the history up to specified fairness
% horizon. Fairness horizon = inf means all the history
function apass = accumulate_pass(pass, agents_id, t_i, fairness_horizon)
    apass = zeros(1, 2);
    for i_agent = 1 : 2
        id = agents_id(i_agent);
        if fairness_horizon == inf
            start_i = 1;
        else
            start_i = max([t_i(id) - fairness_horizon, 1]);
        end
        apass(i_agent) = sum(pass(start_i:t_i(id)-1,id));
        apass(i_agent) = apass(i_agent) / min([fairness_horizon, t_i(id)-1]);
    end
end