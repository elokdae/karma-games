\section{Introduction}
\label{sec:Intro}

The scarcity of resources is one of modern day society's most prominent challenges.
With a strong population growth and shift to urbanization, our finite natural and infrastructure resources are seeing unprecedented levels of stress.
The need to devise \emph{fair} and \emph{efficient} means of access to these resources is now more eminent than ever.

In this paper, we study a class of dynamic resource allocation problems in which an indivisible resource is repeatedly contested between two anonymous users who are randomly drawn from a large population.
Figure~\ref{fig:examples} demonstrates three motivating examples for this class of resource competitions.
\begin{enumerate}[label=(\alph*)]
    \item Due to excessive demand, only one of two trip requests from ride-hailing riders can be served by the closest ride-hailing driver. Which rider should be served? \label{ex:ridehailing}
    
    \item Two autonomous vehicles (AVs) meet at an unsignalled intersection. Which AV should go first? \label{ex:intersections}
    
    \item To improve quality of service for critical content, an internet service provider (ISP) splits its bandwidth into a high capacity fast channel and a low capacity slow channel, and dedicates the fast channel to half of the total traffic. Two internet content providers (ICPs) simultaneously request service. Which ICP should be granted to the fast channel? \label{ex:internet}
    
\end{enumerate}
\begin{figure}[bt!]
    \centering
    \subfloat[]{
        \includegraphics[height=0.3\textwidth, valign=m]{figures/karma-ride-hailing.pdf}
    }
    \hfill
    \subfloat[]{
        \includegraphics[height=0.3\textwidth, valign=m]{figures/karma-intersections.pdf}
    }
    \hfill
    \subfloat[]{
        \includegraphics[width=0.38\textwidth, valign=m]{figures/karma-net-neutrality.pdf}
        \vphantom{\includegraphics[height=0.3\textwidth, valign=m]{example-image-a}}
    }
    
    \caption{Three motivating examples for karma mechanisms.
    (a) When demand exceeds supply, which trip request should a ride-hailing platform assign to the next available driver?
    (b) In a future of autonomous vehicles (AVs), can the AVs self-coordinate who passes first in intersections in a fair and efficient manner?
    (c) Can an internet service provider (ISP) dedicate a fast channel for critical content in a net-neutral manner, i.e., without charging internet content providers (ICPs) differently?}
    \label{fig:examples}
\end{figure}

These examples share in common that the resource (the ride-hailing driver, the intersection, or the fast channel) is repeatedly contested over a long time horizon by anonymous users in a large population (the ride-hailing riders, the AVs, or the ICPs), who could have time-varying private needs for accessing the resource.
It is important to note that the reality of these examples is complex, and they each deserve a separate treatment.
% For example, it is possible that more than two users simultaneously request access to the resource; the ride-hailing driver might prefer a rider whose destination aligns better with an overall travel plan;
% which AV goes first in the intersection might have complex consequences for the overall traffic flow; and a 50-50 split of the fast and slow channels might be sub-optimal for the ISP.
Nonetheless, the level of abstraction chosen in this paper serves to highlight the fundamental trade-offs that arise in this class of problems, as well as to ease the presentation of the novel analysis tools developed to study them.
We adopt a level of generality in these tools that allows them to be readily specified to more complex settings.

% Considering the AVs example, we can take inspiration from how
% }humans often incorporate simple notions of fairness in resolving who goes first in an unsignalled intersection. 
% Using subtle driving maneuvers and possibly hand gestures, human drivers are able to signal their intent and are willing to yield occasionally when we feel that we have been treated generously a few times, even if we are likely intersecting with a different person now than before~\cite{nandavar2019understanding}.
% In some cases this `give and take' behavior is partly codified, e.g., in the all-way stop intersections and in the zipper lane merge.

One can draw inspiration from how small communities sometimes manage to self-organize the access to common resources~\cite{ostrom1990governing}.
Typically these communities devise systems that facilitate a form of fair `giving and taking', i.e., systems where the users take turns in accessing the resource (see, e.g., the system devised by local fishermen to manage the inshore fishery in Alanya, Turkey~\cite{berkes1986local,ostrom1990governing}).
This work is an attempt to systematize the `give and take' such that it can be applied unambiguously in a large-scale system, and builds upon the exploratory concept proposed in~\cite{censi2019today}. The main enabler is \emph{karma}, a counter that encodes the history of `giving and taking' of the users.
Loosely inspired by the notion of karma in Indian tradition~\cite{doniger1980karma}, each user is endowed with karma points which increase when the user yields the resource, and decrease when the user accesses the resource.
A user with a high level of karma was likely yielding in the past, and gets an advantage in receiving the resource now. In turn, the disfavored user that yields now gets compensated in karma which will give them an advantage in the future.

In its simplest form, such a \emph{karma mechanism} is an effective mean to facilitate \emph{turn-taking} on the large scale.
But a karma mechanism can do more than simple turn-taking.
If users are given the option to choose how much karma to use now, e.g., through an auction-like \emph{karma bidding} scheme, the karma becomes also a means to express \emph{private temporal preferences}.
For example, a user may decide to yield (and gain karma) when its urgency is low, in anticipation to the situation where accessing the resource is time-critical.
A karma mechanism can hence facilitate the allocation of the resource to whoever needs it most, i.e., the maximization of resource allocation \emph{efficiency}.

A classical device that is used to express preferences and facilitate access to resources is money.
The ride-hailing platform can raise the trip price until only one of the contesting riders is willing to pay for it -- a common practice referred to as \emph{surge pricing}~\cite{castillo2017surge,cachon2017role}.
This practice has seen some public criticism due to a lack of transparency and a tendency to raise prices in a manner that is deemed unfair~\cite{dholakia2015everyone,shontell2014uber}.
While in principle it is meant to allocate trips to the most needy riders, in practice the trips are allocated simply to those who can afford them.
In the transportation domain, decades of research on the use of monetary road pricing policies has been faced with little public enthusiasm due to concerns for equitable access to the roads~\cite{yang2011managing,xiao2013managing,brands2020tradable}.
A popular remedy is the use of \emph{tradable credits}, which are periodically issued road access tokens that are allowed to be traded in a monetary market.
While these schemes ensure that the yielding users can at least sell their credits and receive (monetary) compensation, they neglect the fact that wealthy users (i.e., those who have high `value of time'~\cite{borjesson2012income}) persist to have a systematic advantage in accessing the resource~\cite{xiao2013managing}.
Finally, the topic of \emph{net neutrality} has seen wide-spread public debate in recent years, with strong concerns that the internet will lose its integrity as an open and free resource if ISPs charge ICPs differently~\cite{obama2016net,bourreau2015net,pil2010net,hahn2006economics}.
In all of the above debates, the potential existence of a simple and efficient non-monetary solution seems to be overlooked.

Karma shares similarities with money in how it acts as a token of exchange, but has the distinguishing feature of being acquired from \emph{fair exchanges} that are relevant to the resource allocation problem at hand.
One need not worry about matters of wealth inequality, or rely on the assumption that money is a universal, objective measure of value, since karma is only acquired from the process of yielding the resource to another user.
Karma hence facilitates the design of a purpose-built, self-contained economy for the resource allocation task.
Like in monetary economies, the karma economy can be \emph{tuned} to achieve different fairness and efficiency objectives in a manner that is targeted to the specific resource allocation task, through the design of karma payment rules, the redistribution of karma, and other techniques that we explore here.

Karma mechanisms promise to be efficient and fair, but these unconventional mechanisms require a novel analysis.
A difficulty in the analysis arises due to the lack of reliance on an extrinsic measure of value. Karma does not have value a-priori, and is never used directly in the cost functions of the users.
The value of karma instead arises from how it facilitates access to the resource, and how users need to ration its use in order to cover their future resource access demands.
This makes the behavior of rational users under the karma mechanism, and the resulting social welfare, difficult to predict, and requires the formulation of non-trivial dynamic games played in large populations.
In this work, and in comparison to~\cite{censi2019today}, we develop a tractable and rigorous game-theoretic model to study karma mechanisms that is built on top of the class of \emph{dynamic population games}~\cite{elokda2021dynamic}, and prove the existence of a suitable notion of equilibrium, the \emph{stationary Nash equilibrium}.
% , and provide analytical tools to study social welfare measures of interest at the equilibrium.
We then utilize these technical tools to numerically investigate the strategic behaviors that emerge under the karma mechanisms, their consequences for the social welfare, as well as how the mechanisms can be tuned to achieve different resource allocation objectives.
% different karma mechanism designs which are tailored to case studies involving different assumptions on the users' private utilities and degree of strategic competence.

\subsection{Related works}

% \subsubsection{Governing common resources}

% The class of resource allocation problems we consider
% % , such as our running example of AVs competing over common road space,
% shares similarities to the \emph{common-pool resources} (CPRs) extensively studied in~\cite{ostrom1990governing}.
% % For example, the intersections disputed by the AVs are common-pool resources.
% There, multiple aspects to the challenge of governing common-pool resources are identified.
% These include the \emph{problem of appropriation}, that is, under what rules should resource units be allocated to the users; the \emph{problem of credible commitment}, that is, how to ensure that the users follow the appropriation rules; and the \emph{problem of mutual monitoring}, which concerns with whether the users have an incentive to monitor and punish others in the case of infringements~\cite[Chapter~2]{ostrom1990governing}.
% The karma mechanisms we propose target the problem of appropriation.
% %, while the problem of ensuring that users of the karma system actually follow it remains open. In the context of intersection management for AVs, some degree of central enforcement is likely required by a consortium of transport officials, AV manufacturers, and other stakeholders.
% %Nevertheless, 
% As Ostrom points out, it is fundamental to the success of the self-government of a CPR that the appropriation method is deemed as ``fair, orderly, and efficient'' by the users of the resource system, such that they have a motivation to participate in and contribute to the system.

% %In~\cite{ostrom1990governing}, a number of successful resource appropriation methods adopted in real CPR problems are presented.
% %A representative example is the system devised by local fishermen to manage the inshore fishery in Alanya, Turkey~\cite{berkes1986local}, where the fishing grounds are divided in locations for single use, and the fishermen are assigned a schedule to the fishing locations which rotates each day, such that all fishermen get to fish in all locations.
% Interestingly, a defining feature of many appropriation methods presented in~\cite{ostrom1990governing} (e.g., scheduling of inshore fishing in Alanya, Turkey~\cite{berkes1986local}) is that they facilitate turn-taking.
% Karma mechanisms also facilitate turn-taking, but in a setting where there is a large number of users who need not know each other, and need not interact with the same users again.
% Moreover, karma mechanisms enable the users to \emph{express preferences} through their bids, which is instrumental to the efficiency of the resource allocation whenever there is heterogeneity in the preferences.


\subsubsection{Repeated games}
\label{subsec:repeatedgames}
The celebrated folk theorem~\cite{friedman1971non,fudenberg1986folk} asserts that any individually rational outcome\footnote{In an individually rational outcome, the payoff of each player weakly dominates the player's security or minimax payoff.
The set of individually rational outcomes include socially efficient outcomes in many games, such as in the prisoner's dilemma.} of a finite-player single-stage game can be sustained in a Nash equilibrium of the infinitely repeated game, provided that the players are sufficiently future aware.
The constructive proofs of this classical result and many of its extensions rely on the notion of \emph{switch strategies}, in which the players initially agree on the socially desirable set of actions to play.
In case a player deviates from the agreed action, all other players effectively punish the deviator by switching to a set of actions that make the deviator worst off.
This requires the ability to both detect a deviation as well as identify the deviator.

Several extensions of the folk theorem consider when the actions of others are not perfectly observable, thereby posing a difficulty in identifying deviators.
These include when the players observe a common public outcome~\cite{fudenberg1994folk} or when they only observe private outcomes~\cite{fudenberg1991approximate}.
These works impose identifiability conditions
% \footnote{In~\cite{fudenberg1994folk}, the condition is \emph{pairwise identifiability} and in~\cite{fudenberg1991approximate} it is \emph{informational connectedness}.}}
on the stage game which essentially guarantee that each player can identify the history of others' actions from the observable outcomes.
Another extension of the folk theorem is to \emph{stochastic games}~\cite{dutta1995folk} where the stage games are time-varying and depend on the previous actions of the players.
The difficulty in this setting is that deviations are not only immediately beneficial, but could also take subsequent games into a regime that is profitable to the deviator on the long run.
The authors impose conditions on the game which essentially guarantee that the long run cost of punishment outweighs the long run benefit of deviation.

All of these works consider that every stage game is played by the same finite set of players.
A more related setting is when two players in a large population are randomly matched in each stage, for which a folk theorem is shown in~\cite{okuno1995social}.
Each player is associated with as social status state that is observable by others.
Deviators are punished by changing their status from good to bad, which all future matched players observe and punish for.
%Therefore, any individually rational outcome can be sustained in a so-called \emph{norm equilibrium}, where in addition to all players playing the desired action the distribution of status states in the population is stationary.
%We remark that the status state is similar to the karma state we consider, and the norm equilibrium is similar to our notion of the stationary Nash equilibrium.\textcolor{blue}{At this point of the paper we have not defined these concepts. I think this sentence can be removed or phrased more generally, saying that these concepts have similarities with the concepts that we will introduce later (but then it's so general that it doesn't help much).}

%Despite these similarities, o
Our setting differs fundamentally from the above cited works.
We consider that the players have time-varying private \emph{preferences}, namely their urgency to acquire a contested resource.
In contrast to folk theorem results with private information~\cite{fudenberg1991approximate}, the privacy is with respect to the payoffs of the opponents, rather than the actions they play.
In contrast to folk theorem results for stochastic games~\cite{dutta1995folk}, the time-varying nature is with respect to the private player preferences rather than a fully observable game state.
In the context of a folk theorem, the socially desirable set of actions in our setting is for the players to report their urgency truthfully such that the resource is allocated to the highest urgency player.
It is not obvious how deviation from truthfulness can be detected in the first place.
In principle, if the time-varying urgency \emph{process} is public and the game is played with the same finite set of players, non-truthfulness can be detected on the long run by correlating the history of each player's reports to the expected history\rev{~\cite{jackson2007overcoming}}.
% \footnote{To the extent of our knowledge, this setting has not been considered thus far.}
But in a large population setting where maintaining explicit histories is infeasible, we interpret karma as an extension of the social status state in~\cite{okuno1995social} to handle private preferences, essentially placing a budget on how often players can declare high urgency.

\subsubsection{Mechanism design without money}

The famous Gibbard-Satterthwaite impossibility theorem~\cite{gibbard1973manipulation,satterthwaite1975strategy} poses a fundamental challenge in the design of resource allocation mechanisms.
It asserts that when there are three or more alternative allocations, it is impossible to design a strategy-proof mechanism that is non-dictatorial when the domain of preferences is unrestricted.
One avenue to escape this impossibility is in the use of money, which imposes structure on the preferences of the users by measuring them against the objective monetary yardstick.
The problem of designing monetary mechanisms is well studied, with positive results including the Vickrey-Clarke-Groves (VCG) mechanism~\cite{vickrey1961counterspeculation,clarke1971multipart,groves1973incentives}, a general mechanism that is well known to be strategy-proof and lead to efficient allocations.
% However, the use of money is deemed as unfair or unethical in many application domains.
% Examples include the allocation of donated kidneys to transplant patients~\cite{roth2004kidney} and the allocation of college applicants to colleges~\cite{gale1962college}.
On the other hand, the design of \emph{mechanisms without money}~\cite{schummer2007mechanism} is in general more difficult due to the lack of a general instrument that can be used to align incentives.
Some successes include the cases when preferences are single-peaked~\cite{moulin1980strategy}, when each user has one item to trade~\cite{shapley1974cores}, and when matching users pairwise in a bipartite graph~\cite{gale1962college}, which all leverage specific structures in the preferences of the users that are difficult to generalize.
When users must express preferences over many alternatives, a general approach is the \emph{pseudo-market} pioneered by~\cite{hylland1979efficient} and famously adopted in the context of allocating course seats in business schools~\cite{sonmez2010course,budish2011combinatorial,budish2012multi}. In a pseudo-market, users are given a finite budget of tokens to distribute over the alternatives, whose prices (in tokens) are set/discovered to clear the market (i.e., allocate the correct amount of resources to the correct amount of users).
However, pseudo-markets only promise to be Pareto-efficient and are also not strategy-proof (although strategizing becomes difficult when there are many users and alternatives)~\cite{hylland1979efficient}.
It is noteworthy to mention that in our motivating examples, any allocation of the contested resource is Pareto-efficient.

The aforementioned difficulty stems from the fact that the classical mechanism design problem is concerned with a \emph{static} or \emph{one-shot} allocation of goods.
On the other hand, when the allocation is \emph{dynamic} or \emph{repeating over time}, new opportunities for the design of strategy-proof and efficient mechanisms present themselves.
On a conceptual level, just as how money can be used to incentivize truthful behavior (or punish non-truthfulness), a similar incentive can be achieved through a promise of future service (or denial thereof).
Despite of this intuitive notion, the role that repetition could play in mechanism design has only been recently noticed, and the literature on mechanism design for dynamic resource allocation is sparse.
A few recent works build upon the notion of ``promised utilities'', pioneered in the context of contract design in repeated relationships~\cite{spear1987repeated}.
These include~\cite{guo2020dynamic}, which develops an incentive compatible mechanism for the case when a single principal repeatedly allocates a single good to a single user, and~\cite{balseiro2019multiagent}, which extends this approach to the case when the single good is repeatedly allocated to one of the same contesting users.
The working principle of these works is to find a set of future utilities that the principal promises to the user(s) as a function of their reported immediate utility, in a manner that incentivizes truthful reporting, and while ensuring that the principal can recursively keep these future promises.
% In~\cite{guo2020dynamic}, the promised utilities are given the intuition of corresponding to the number of consecutive times the user can claim the good ``with no questions asked''. In~\cite{balseiro2019multiagent}, no intuition is given to how the principal can keep its promises.
This is based on the assumption that the principal has the power to commit to the promised utilities, without specifying the exact mechanism to do so.
Similarly, karma is a device that encodes future promises; the higher a user's karma the more favorable its future position will be.
But with karma, these promises need not be made explicit, and a single principal need not be held accountable for them.
Instead, the future value of karma arises in a decentralized and natural manner as the users strategically ration its use.
The promise of future utility is made by the population as a whole by attributing the right of access to future resources to karma.
% include~\cite{balseiro2019multiagent}, which proposes a mechanism based on ``promised utilities'' that are not intuitive to implement in practice.

In other related works, \cite{sonmez2020incentivized} leverage the high likelihood of kidney transplant failures to incentivize participation in the kidney exchange by providing a priority for re-transplant to participants.
\cite{kim2021organ} similarly incentivize participation in the kidney exchange by issuing participants a voucher for re-transplant that is also redeemable by their offspring.
We consider our karma mechanisms to be complimentary to these works, offering a simple and intuitive alternative that has the potential to scale to large systems and across multiple applications.

\subsubsection{Artificial currency mechanisms}

A special class of mechanisms without money, which are perhaps the most related to our karma mechanisms, are the so-called \emph{artificial currency} or \emph{scrip} mechanisms.
These mechanisms have been proposed in multiple isolated application instances since the early 2000's.
\cite{golle2001incentives} propose a ``point system'' to address the problem of \emph{free-riding} in peer-to-peer networks, where agents tend to download many more files than they upload.
%Uploads are incentivized by rewarding points which are needed to download files.
Similar works in the domain of peer-to-peer networks include~\cite{vishnumurthy2003karma}, who specifically call their point system ``karma'' and focus on its cryptographic implementation rather than the design of the mechanism itself; and~\cite{friedman2006efficiency}, who do incorporate elements of mechanism design but focus solely on the choice of a single parameter, which is the total amount of karma in their specific model.
In the domain of transportation, \cite{salazar2021urgency} recently demonstrated how an artificial currency (also called ``karma'') can be utilized instead of monetary tolls to achieve optimal routing in a two arc road network.
% In the domain of course allocation, the use of artificial currency has been employed in business schools as a means for students to bid over courses with limited seats and to allocate those seats~\cite{sonmez2010course,budish2011combinatorial,budish2012multi}. The course allocation problem is however fundamentally different from the considered class of problems we since it is essentially a one-shot allocation.
To the extent of our knowledge, the only concrete example of a real-life implementation of a karma-like concept is the ``choice system'' for the allocation of food donations to food banks in the United States~\cite{prendergast2022allocation}.
There, food banks are allocated ``shares'' which they use to bid on the food donations they need.
It is considered to be a major success as evidenced by the active participation of food banks in the system as well as the unprecedented fluidity of food donations it resulted in.

All of the above works share in common the need for a non-monetary medium of exchange to coordinate the use of shared resources.
However, there is an apparent lack of unity in the approaches taken, with most works proposing a problem-tailored, heuristic mechanism with little rigorous justification and scope for generalization.
In~\cite{prendergast2022allocation}, a model is presented that makes many simplifying assumptions on the strategic behavior of the users.
This model does not truly capture the dynamic nature of the optimization problem of the users, who must ration their use of shares now to secure their future needs.
In~\cite{salazar2021urgency}, a game-theoretic equilibrium is considered in which individual users solve a finite horizon dynamic optimization, but importantly, the amount of karma saved at the end of the horizon is treated as an exogenous parameter.
A few other works that attempt to systematically study artificial currency mechanisms include~\cite{johnson2014analyzing,gorokh2021monetary}.
\cite{johnson2014analyzing} studies a setting where a pool of users alternate between requesting and providing services to each other (e.g., a pool of parents exchanging baby-sitting services).
This differs from our work in the following fundamental aspects.
First, it does not give the users the flexibility to express intensity of preferences through a
bidding procedure.
Second, the equilibrium notion considered relies on remembering if certain users denied providing service before and punishing those users by never granting them service again.
As discussed in Section~\ref{subsec:repeatedgames}, 
retaliation schemes are crucially based on the capability of detecting defection, which in our setting cannot be done without knowing the private preference of the agents.
%This resembles the classical Folk theorem strategies for punishing deception~\cite{friedman1971non,fudenberg1986folk}, which defeats the purpose of introducing an artificial currency in the first place since it requires public knowledge of the histories and identities of the users and will be impractical to scale to large-scale systems.
\cite{gorokh2021monetary} provides a general method to convert truthful monetary to non-monetary mechanisms, which relies on a central planner estimating how much money each user would spend in a finitely repeated monetary auction and giving the user a similar amount of artificial currency at the beginning of the horizon.
This requires central knowledge of the players’ private
preferences, and does not capture the important dynamic feedback process of gaining currency through yielding.
In contrast to these works, our approach shows that a robust and ultimately efficient behavior emerges only from the dynamic strategic problem faced by the users of the karma mechanism, without additional rules or coordination mechanisms.
To the extent of our knowledge, there are no other works that study the strategic behavior in artificial currency mechanisms at this level of generality.
We believe that this is fundamental for the understanding of these mechanisms and serves as an important tool for the mechanism design.

% \subsubsection{Fair resource allocation}

% A plethora of definitions of what constitutes a fair resource allocation, as well as how to measure the fairness of an allocation, exists in the extensive literature on the subject.
% When the resource is divisible, a variety of fairness definitions and associated social welfare functions arise axiomatically, which primarily depend on the allowed degree of comparability of the utilities of different users~\cite{roberts1980interpersonal}.
% These include \emph{utilitarianism}~\cite{d1977equity}, \emph{proportional fairness} or \emph{Nash welfare}~\cite{nash1950bargaining,kaneko1979nash,kelly1997charging}, \emph{lexi-max-min fairness}~\cite{arrow1977extended}, and \emph{$\alpha$-fairness}~\cite{roberts1980interpersonal,mo2000fair}, where the latter forms a family of fairness definitions parametrized by a parameter $\alpha$ and includes the former three as special cases.
% Another notion of fairness that does not require any degree of interpersonal comparability is \emph{envy-freeness}~\cite{foley1966resource}, which defines a fair resource allocation as one where no user envies the resource share of another.
% When the resource is indivisible, a common approach to fairness is based on the notion of \emph{priority}~\cite{young1995equity}, where priority to access the resource is given to users based on a scoring scheme that is publicly deemed as fair.
% On the other hand, measures to quantify how fair a resource allocation is include the well-known Gini coefficient~\cite{gini1912variabilita}, Jain's index~\cite{jain1984quantitative}, Theil's index~\cite{theil1967economics}, the average utility of the least fortunate percentage of the population~\cite{chapman2018fair}, and many others (see~\cite[Chapter~2]{sen1997economic} for a survey).
% The defining feature of all of the above works is that they are, once again, concerned with static or one-shot allocations.
% While in principle a repeated resource allocation can be viewed as a static allocation of the resource duplicated over time (see, e.g., \cite{luss1999equitable}), this approach is impractical when the repetition is frequent, and when we must reason about the fairness of the resource allocation \emph{before} future instances of it occur.

% To this end, a tangential stream of literature studies the fairness of \emph{randomized resource allocations}, where the popular distinction between fairness \emph{ex-ante} and \emph{ex-post} is made~\cite{cappelen2013just}.
% A randomized resource allocation is ex-ante fair if the chances or odds of randomization are fair, and it is ex-post fair if in addition all possible realizations of the randomization are fair.
% In our context, a repeated coin toss is ex-ante fair, but could lead to vastly unfair allocations ex-post after multiple repetitions.
% It is clear from this example that a fairness definition is needed which incorporates memory from past allocations, a notion that, to the extent of our knowledge, has not been formalized before.
% We take a step in this direction with our definition of \emph{corrective fairness}, which quantifies the likelihood of receiving the resource given the number of consecutive previous denials.

% \subsubsection{Dynamic population games}

% \todo[inline]{The following copied from other paper, need to clean up (but has the intended references)}

% In a wide variety of fields, ranging from economics to biology and engineering, population games have become a standard model of strategic interplay in large societies of rational agents, see~\cite{sandholm2010population}. When the number of agents is large, the identity and behavior of the individual agent is not significant to the whole, and one can instead focus on groups of agents behaving in different ways. This enables the formulation of large-scale game-theoretic models that would otherwise be intractable.

% In the classical population game, this grouping is based on static \emph{types} of the agents which encode their incentives and behavior.
% It is natural, however, to expect that the agents alter their behavior based also on time-varying \emph{states}, which are in turn affected by their individual decisions and the decisions of others.
% For example, an agent behaves differently under the threat of an epidemic based on whether it is infected or not, and that very behavior affects its future infection state, see~\cite{elokda2021adynamic}.
% On the other hand, game-theoretic settings involving state dynamics have classically been formulated as the stochastic games of~\cite{shapley1953stochastic}, which are known to become intractable for more than a few agents and are not suitable for large populations.

% Extensions of stochastic games to large populations have been previously proposed under different taxonomies, including the \emph{mean field games} of~\cite{huang2006large} and~\cite{lasry2007mean}, the \emph{anonymous sequential games} of~\cite{jovanovic1988anonymous}, and variants thereof (see~\cite{elokda2021dynamic} for a comprehensive review).
% Most of these works give theoretical guarantees on the existence of an equilibrium notion, often referred to as the \emph{stationary equilibrium}, but give little clues on how they are applicable in practice.
% In sharp contrast to the literature, our treatment aims at offering the same kind of versatility that the static population games of~\cite{sandholm2010population} have seen.
% In our main contribution, Theorem~\ref{th:Reduction}, we show that dynamic population games can be reduced to a suitably defined static population game whose classical Nash equilibria coincide with the stationary equilibria in our dynamic setting.
% As a consequence, we are able to formulate a macroscopic model of the coupled evolution of both the agents' actions and states, by adapting classical evolutionary dynamics to dynamic population games.
% This evolutionary model can act both as an equilibrium seeking algorithm, as well as a model of the agents' behavior off the equilibrium.
% We demonstrate the utility of our approach on two application examples, the former being an intricate dynamic auction-like mechanism with finite budgets, called a \emph{karma mechanism}, and the latter being a model of strategic behavior under the threat of an epidemic.

\subsection{Organization of the paper}

Section~\ref{sec:karma} introduces the setting of dynamic resource allocation and the concept of karma mechanisms.
In Section~\ref{sec:Model} we model karma mechanisms as dynamic population games, and show that a stationary Nash equilibrium is guaranteed to exist.
Section~\ref{sec:KarmaTransferModel} focuses specifically on how different karma payment and redistribution rules can be incorporated in the model.
The model is utilized in a numerical investigation of karma mechanisms in Section~\ref{sec:NumericalAnalysis}, where we provide insights on the emerging strategic behavior as well as the consequences of the karma mechanism design on the achieved efficiency and fairness of the resource allocation.


\subsection{Notation}
Let $D$ be a discrete set and $C$ be a continuous set.
Let $a,d \in D$ and $c \in C$.
For a function $f : D \times C \rightarrow \Real$, we distinguish the discrete and continuous arguments through the notation $f[d](c)$.
Alternatively, we write $f : C \rightarrow \Real^{\lvert D \rvert}$ as the vector-valued function $f(c)$, with $f[d](c)$ denoting its $d^\textup{th}$ element.
Similarly, $g[a \mid d](c)$ denotes the conditional probability of $a$ given $d$ and $c$. 
Specifically, $g[d^+ \mid d](c)$ denotes one-step transition probabilities for $d$.
We denote by $\Delta(D):=\left\{\left. p \in \Real_+^{\lvert D \rvert} \right\rvert \sum_{d \in D} p[d] = 1 \right\}$ the set of probability distributions over the elements of $D$.
%, which has dimension equal to the cardinality of $D$.
For a probability distribution $p \in \Delta(D)$, $p[d]$ denotes the probability of element $d$.
When considering heterogeneous agent types, we denote by $x_\tau$ a quantity associated to type $\tau$.