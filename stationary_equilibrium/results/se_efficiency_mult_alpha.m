clc;
screenwidth = 1920;
screenheight = 1080;

%% Some parameters
z1_eff_comp = 0 : 0.1 : 1;
n_z1_eff_comp = length(z1_eff_comp);

file_start = 'stationary_equilibrium/results/se_U_1_1_10_phi1_0.95_0.05_0.00_0.00_0.50_0.50_0.95_0.05_0.00_alpha1_0.70_alpha2_0.99_pay_1_tax_0.005/k_bar_10_z_';

%% Efficiency arrays
for i_z1_eff_comp = 1 : n_z1_eff_comp
    z1 = z1_eff_comp(i_z1_eff_comp);
    z2 = 1 - z1;
    file = [file_start, num2str(z1, '%.2f'), '_', num2str(z2, '%.2f'), '.mat'];
    if exist(file, 'file')
        load(file);
        
        if i_z1_eff_comp == 1
            se_access_eff = zeros(param.n_alpha + 1, n_z1_eff_comp);
            se_cost_eff = zeros(param.n_alpha + 1, n_z1_eff_comp);
        end
        
        %% SE access efficiency
        % Probability of winning per alpha type
        for i_alpha = 1 : param.n_alpha
            prob_up_b = 1 / param.g_up_mu_alpha(1,i_alpha) * einsum('ijkl,ijklm->m', se_d_up_mu_alpha_u_k(1,i_alpha,:,:), se_pi_down_mu_alpha_u_k_up_b(1,i_alpha,:,:,:));
            se_access_eff(i_alpha,i_z1_eff_comp) = dot(prob_up_b, squeeze(gamma_down_b_up_o(:,1)));
        end
        
        % Overall probability of winning
        se_access_eff(end,i_z1_eff_comp) = dot(se_nu_up_bj, squeeze(gamma_down_b_up_o(:,1)));
        
        %% SE cost efficiency
        % Cost efficiency per alpha type
        for i_alpha = 1 : param.n_alpha
            se_cost_eff(i_alpha,i_z1_eff_comp) = 1 / param.g_up_mu_alpha(1,i_alpha) * dot(reshape(se_d_up_mu_alpha_u_k(1,i_alpha,:,:), [], 1), reshape(se_R_down_mu_alpha_u_k(1,i_alpha,:,:), [], 1));
        end
        
        % Overall cost efficiency
        se_cost_eff(end,i_z1_eff_comp) = dot(reshape(se_d_up_mu_alpha_u_k, [], 1), reshape(se_R_down_mu_alpha_u_k, [], 1));

        % Random cost efficiency
        rand_E = -0.5 * dot(se_upsilon_up_u, param.U);
        
        % Centralized urgency (best possible) cost efficiency
        u_E = 0;
        for i_u = 1 : param.n_u
            u_E = u_E - 0.5 * se_upsilon_up_u(i_u)^2 * param.U(i_u);
            for i_uj = i_u + 1 : param.n_u
                u_E = u_E - se_upsilon_up_u(i_u) * se_upsilon_up_u(i_uj) * param.U(i_u);
            end
        end
    end
end

%% Plot
figure(1);
fig = gcf;
fig.Position = [0, 0, screenwidth, screenheight];
subplot(1,2,1);
plot(1 - z1_eff_comp, se_access_eff(1:param.n_alpha,:), '--o', 'LineWidth', 2, 'MarkerSize', 10);
hold on;
plot(1 - z1_eff_comp, se_access_eff(end,:), 'LineWidth', 2);
axes = gca;
axis_semi_tight(axes, 1.2);
axes.XAxis.FontSize = 15;
axes.XAxis.FontName = 'ubuntu';
axes.YAxis.FontSize = 15;
axes.YAxis.FontName = 'ubuntu';
axes.XLabel.FontName = 'ubuntu';
axes.XLabel.FontWeight = 'bold';
axes.XLabel.String = 'Proportion of \alpha_2';
axes.XLabel.FontSize = 20;
axes.YLabel.FontName = 'ubuntu';
axes.YLabel.FontWeight = 'bold';
axes.YLabel.String = 'Access efficiency';
axes.YLabel.FontSize = 20;

subplot(1,2,2);
plot(1 - z1_eff_comp, rand_E * ones(1, n_z1_eff_comp), 'LineWidth', 2);
hold on;
plot(1 - z1_eff_comp, u_E * ones(1, n_z1_eff_comp), 'LineWidth', 2);
plot(1 - z1_eff_comp, se_cost_eff(1:param.n_alpha,:), '--o', 'LineWidth', 2, 'MarkerSize', 10);
plot(1 - z1_eff_comp, se_cost_eff(end,:), 'LineWidth', 2);
axes = gca;
axis_semi_tight(axes, 1.2);
axes.XAxis.FontSize = 15;
axes.XAxis.FontName = 'ubuntu';
axes.YAxis.FontSize = 15;
axes.YAxis.FontName = 'ubuntu';
axes.XLabel.FontName = 'ubuntu';
axes.XLabel.FontWeight = 'bold';
axes.XLabel.String = 'Proportion of \alpha_2';
axes.XLabel.FontSize = 20;
axes.YLabel.FontName = 'ubuntu';
axes.YLabel.FontWeight = 'bold';
axes.YLabel.String = 'Cost efficiency';
axes.YLabel.FontSize = 20;