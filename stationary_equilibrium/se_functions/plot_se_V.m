% Plot SE value function
function plot_se_V(fg, position, se_V_down_mu_alpha_u_k, param, se_param, i_alpha_comp)
    persistent se_V_plot
    if ~ishandle(fg)
        figure(fg);
        fig = gcf;
        fig.Position = position;
        se_V_plot = cell(param.n_mu, param.n_alpha, param.n_u);
        n_subplots = param.n_mu * param.n_alpha;
        n_cols = ceil(sqrt(n_subplots));
        n_rows = ceil(n_subplots / n_cols);
        lgd_text = cell(param.n_u, 1);
        for i_u = 1 : param.n_u
            lgd_text{i_u} = ['u = ', num2str(param.U(i_u))];
        end
        for i_mu = 1 : param.n_mu
            for i_alpha = 1 : param.n_alpha
                i_subplot = (i_mu - 1) * param.n_alpha + i_alpha;
                subplot(n_rows, n_cols, i_subplot);
                hold on;
                for i_u = 1 : param.n_u
                    se_V_plot{i_mu,i_alpha,i_u} = plot(se_param.K, squeeze(se_V_down_mu_alpha_u_k(i_mu,i_alpha,i_u,:)), '-x', 'LineWidth', 2);
                end
                axis tight;
                axes = gca;
                alpha = param.Alpha(max([i_alpha, i_alpha_comp]));
                axes.Title.FontName = 'ubuntu';
                if alpha > 0.99 && alpha < 1
                    alpha_str = num2str(alpha, '%.3f');
                else
                    alpha_str = num2str(alpha, '%.2f');
                end
                if alpha == 1
                    axes.Title.String = ['k_{bar} = ', num2str(param.k_bar, '%02d'), ' SE relative to average value function for \mu = ', num2str(i_mu), ', \alpha = ', alpha_str];
                else
                    axes.Title.String = ['k_{bar} = ', num2str(param.k_bar, '%02d'), ' SE value function for \mu = ', num2str(i_mu), ', \alpha = ', alpha_str];
                end
                axes.Title.FontSize = 12;
                axes.XAxis.FontSize = 10;
                axes.YAxis.FontSize = 10;
                axes.XLabel.FontName = 'ubuntu';
                axes.XLabel.String = 'Karma';
                axes.XLabel.FontSize = 12;
                axes.YLabel.FontName = 'ubuntu';
                axes.YLabel.String = 'Utility';
                axes.YLabel.FontSize = 12;
                lgd = legend(lgd_text);
                lgd.FontSize = 12;
                lgd.FontName = 'ubuntu';
                lgd.Location = 'bestoutside';
            end
        end
    else
        for i_mu = 1 : param.n_mu
            for i_alpha = 1 : param.n_alpha
                for i_u = 1 : param.n_u
                    se_V_plot{i_mu,i_alpha,i_u}.YData = squeeze(se_V_down_mu_alpha_u_k(i_mu,i_alpha,i_u,:));
                end
                alpha = param.Alpha(max([i_alpha, i_alpha_comp]));
                if alpha > 0.99 && alpha < 1
                    alpha_str = num2str(alpha, '%.3f');
                else
                    alpha_str = num2str(alpha, '%.2f');
                end
                if alpha == 1
                    se_V_plot{i_mu,i_alpha,1}.Parent.Title.String = ['k_{bar} = ', num2str(param.k_bar, '%02d'), ' SE relative to average value function for \mu = ', num2str(i_mu), ', \alpha = ', alpha_str];
                else
                    se_V_plot{i_mu,i_alpha,1}.Parent.Title.String = ['k_{bar} = ', num2str(param.k_bar, '%02d'), ' SE value function for \mu = ', num2str(i_mu), ', \alpha = ', alpha_str];
                end
            end
        end
    end
end